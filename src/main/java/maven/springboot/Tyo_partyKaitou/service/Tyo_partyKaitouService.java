/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Tyo_partyKaitou.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_Parties_UsersEntity;
import java.util.*;

import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouInDto;
import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouOutDto;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouupdateWebDto;




/**
 *
 * @author t4-yoshioka
 */

@Service
public class Tyo_partyKaitouService {
 
    @Autowired
    private T_Parties_UsersRepository repository;
    
    public Tyo_partyKaitouOutDto nomikaiHyoji(Tyo_partyKaitouInDto tyo_partyKaitouInDto){
        
        Tyo_partyKaitouOutDto tyo_partyKaitouOutDto = new Tyo_partyKaitouOutDto();
        
        // select文を発行し、要素数を代入
        int count = repository.findByUser_id(tyo_partyKaitouInDto.getUser_id()).size();
        
        System.out.println("*******************");
        System.out.println("きてる２");
        System.out.println("*******************");

        
        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
//            tyo_partyKaitouOutDto.setParty_name("nullだよ");
//            tyo_partyKaitouOutDto.setAttend_cd(00000);
        } else {
            List<T_Parties_UsersEntity> nomikaiList = repository.findByUser_id(tyo_partyKaitouInDto.getUser_id());
            
            for(int i=0; i < nomikaiList.size(); i++){
                
                String party_name = nomikaiList.get(i).getParty_name();
                int attend_cd = nomikaiList.get(i).getAttend_cd();

                //出力してみます①
                System.out.println("*******************");
                System.out.println(party_name);
                System.out.println(attend_cd);
                System.out.println("*******************");
                
                //ここから↓、Update機能を実装部分
                T_Parties_UsersEntity entity = nomikaiList.get(i);
                entity.setParty_name(party_name);
                entity.setAttend_cd(attend_cd);
                
                
                
//                //出力してみます②
//                System.out.println("*********Entity**********");
//                System.out.println(entity);
//                System.out.println("*********Entity**********");
//                
//                System.out.println("*********Indto**********");
//                System.out.println(tyo_partyKaitouInDto.getParty_name());
//                System.out.println(tyo_partyKaitouInDto.getAttend_cd());
//                System.out.println("*********Indto**********");
                
                
                this.repository.save(entity);


//                // DTOに値をセット
//                tyo_partyKaitouOutDto.setParty_name(party_name);
//                tyo_partyKaitouOutDto.setAttend_cd(attend_cd);
                tyo_partyKaitouOutDto.setList(nomikaiList);
            }
        }
        
        return tyo_partyKaitouOutDto;
        
    }
    
    
    public boolean nomikaiUpdate(Tyo_partyKaitouupdateWebDto tyo_partyKaitouupdateWebDto){
        
        boolean updateflag;
                
        List<T_Parties_UsersEntity> updateList = tyo_partyKaitouupdateWebDto.getList();

        //update文を記述
        for (T_Parties_UsersEntity t_Parties_UsersEntity : updateList) {
            this.repository.save(t_Parties_UsersEntity);
        }
  
        updateflag = true;
        
        return updateflag;
        
    }

}
