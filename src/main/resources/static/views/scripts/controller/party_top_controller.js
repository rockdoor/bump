/* global $routeScope */

angular.module('MyApp')
        .controller('Party_TopController', ['$scope', '$http', '$location', '$rootScope' ,'$routeParams',
            function ($scope, $http, $location, $rootScope, $routeParams) {

                //header表示フラグをfalseに変更(初期値)
                $rootScope.header = false;
                $rootScope.id = $routeParams.id;

                $scope.dopartyLogin = function () {
//                    $rootScope.email = $scope.email;
                    $http({
                        method: 'POST',
                        data: {email: $scope.user.mail,
                        password: $scope.user.password},
                        url: '/login'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.flag === true) {
                            console.log('ログイン成功');
                            $rootScope.user_id = data.user_id;
                            //header表示フラグをtrueに変更
                            $rootScope.header = false;
                            $rootScope.loginflag = true;
                            var url = '/party/' + $rootScope.id;
                            console.log(url);
                            $location.path(url);
                            
                        } else {
                            alert("登録されていないか、パスワードが間違っています");
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
            }]);