package maven.springboot.KininaruToroku.service.dto;

import java.util.List;
import maven.springboot.Entity.M_LikesEntity;

public class KininaruSettingWebOutDto {

    private List<M_LikesEntity> KininaruSettingWebOutDto;

    /**
     * @return the KininaruSettingWebOutDto
     */
    public List<M_LikesEntity> getKininaruSettingWebOutDto() {
        return KininaruSettingWebOutDto;
    }

    /**
     * @param KininaruSettingWebOutDto the KininaruSettingWebOutDto to set
     */
    public void setKininaruSettingWebOutDto(List<M_LikesEntity> KininaruSettingWebOutDto) {
        this.KininaruSettingWebOutDto = KininaruSettingWebOutDto;
    }
    
}