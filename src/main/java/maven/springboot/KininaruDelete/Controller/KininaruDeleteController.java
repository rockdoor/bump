package maven.springboot.KininaruDelete.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.KininaruDelete.service.KininaruDeleteService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.KininaruDelete.service.dto.KininaruDeleteInDto;
import maven.springboot.KininaruDelete.service.dto.KininaruDeleteWebInDto;
import maven.springboot.KininaruDelete.service.dto.KininaruDeleteWebOutDto;
import org.springframework.web.bind.annotation.RequestBody;
import maven.springboot.Repository.M_LikesRepository;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KininaruDeleteController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruDeleteService kininarudeleteservice;
//登録ボタン押下後の処理⇒個人用のt_users_likesにuser_idと紐付けてインサートする。

    @RequestMapping(value = "/kininaruDelete", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruDeleteWebOutDto requestKininaruDelete(@RequestBody KininaruDeleteWebInDto kdwid) {
        int requser_id = kdwid.getUser_id();
        HashMap<String,String> delete_map = kdwid.getDelete_map();
        
        KininaruDeleteInDto kdid = new KininaruDeleteInDto();
        
        //indtoに受け渡し
        kdid.setUser_id(requser_id);
        kdid.setDelete_map(delete_map);
        
        KininaruDeleteWebOutDto kdwod = kininarudeleteservice.kininaruDelete(kdid);

        //try {
            //確認
          //  System.out.println(new ObjectMapper().writeValueAsString(kswod));
            
        //} catch (JsonProcessingException ex) {
          //  Logger.getLogger(KininaruDeleteController.class.getName()).log(Level.SEVERE, null, ex);
        //}
        return kdwod;
    }
};