package maven.springboot.MatchingToroku.service;

import maven.springboot.MatchingToroku.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_UsersEntity;
import java.util.*;

import maven.springboot.MatchingToroku.service.dto.MatchingTorokuInDto;
import maven.springboot.MatchingToroku.service.dto.MatchingTorokuWebDto;
import maven.springboot.Repository.T_Users_MatchingRepository;

import maven.springboot.Entity.T_Users_MatchingEntity;

@Service  // DIする対象の目印
public class MatchingTorokuService {

    @Autowired
    private T_Users_MatchingRepository t_Users_MatchingRepository;

    public MatchingTorokuWebDto MatchingHyoji(MatchingTorokuInDto matchingtorokuindto) {

        MatchingTorokuWebDto matchingtorokuwebdto = new MatchingTorokuWebDto();

        // select文を発行し、要素数を代入
        int count = t_Users_MatchingRepository.findByUser_id(matchingtorokuindto.getUser_id()).size();
        int user_id = matchingtorokuindto.getUser_id();

        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
        } else {
            List<T_Users_MatchingEntity> MatchingList = t_Users_MatchingRepository.findByUser_id(user_id);

            // DTOに値をセット
            matchingtorokuwebdto.setUser_id(MatchingList.get(0).getUser_id());
            matchingtorokuwebdto.setT_ok_ng(MatchingList.get(0).getT_ok_ng());
            matchingtorokuwebdto.setT_interact(MatchingList.get(0).getT_interact());
            matchingtorokuwebdto.setT_area1(MatchingList.get(0).getT_area1());
            matchingtorokuwebdto.setT_area2(MatchingList.get(0).getT_area2());
            matchingtorokuwebdto.setT_area3(MatchingList.get(0).getT_area3());
            matchingtorokuwebdto.setT_time(MatchingList.get(0).getT_time());
            matchingtorokuwebdto.setR_ok_ng(MatchingList.get(0).getR_ok_ng());
            matchingtorokuwebdto.setR_interact(MatchingList.get(0).getR_interact());
            matchingtorokuwebdto.setR_area1(MatchingList.get(0).getR_area1());
            matchingtorokuwebdto.setR_area2(MatchingList.get(0).getR_area2());
            matchingtorokuwebdto.setR_area3(MatchingList.get(0).getR_area3());
            matchingtorokuwebdto.setR_time(MatchingList.get(0).getR_time());

        }
        return matchingtorokuwebdto;
    }

    public MatchingTorokuWebDto kousin(MatchingTorokuInDto matchingtorokuindto) {
        MatchingTorokuWebDto mtwd = new MatchingTorokuWebDto();

        int user_id = matchingtorokuindto.getUser_id();
        int t_ok_ng = matchingtorokuindto.getT_ok_ng();
        int t_interact = matchingtorokuindto.getT_interact();
        int t_time = matchingtorokuindto.getT_time();
        int t_area1 = matchingtorokuindto.getT_area1();
        int t_area2 = matchingtorokuindto.getT_area2();
        int t_area3 = matchingtorokuindto.getT_area3();
        //出欠区分が出席のものだけ持ってくる（リポジトリで実装）
        List<T_Users_MatchingEntity> t_Users_MatchingEntity_List = t_Users_MatchingRepository.findByUser_id(user_id);
        int count = t_Users_MatchingEntity_List.size();

        if (count == 0) {
            System.out.println("★★★★★更新対象はありません★★★★");
            mtwd.setT_flag(false);
        } else {
            mtwd.setT_flag(true);
            System.out.println("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★");
            T_Users_MatchingEntity t_Users_MatchingEntity = t_Users_MatchingEntity_List.get(0);
            t_Users_MatchingEntity.setUser_id(user_id);
            t_Users_MatchingEntity.setT_ok_ng(t_ok_ng);
            t_Users_MatchingEntity.setT_interact(t_interact);
            t_Users_MatchingEntity.setT_time(t_time);
            t_Users_MatchingEntity.setT_area1(t_area1);
            t_Users_MatchingEntity.setT_area2(t_area2);
            t_Users_MatchingEntity.setT_area3(t_area3);

            //更新処理
            t_Users_MatchingRepository.save(t_Users_MatchingEntity_List);
            // DTOに値をセット
            List<T_Users_MatchingEntity> MatchingList = t_Users_MatchingRepository.findByUser_id(user_id);
            mtwd.setUser_id(MatchingList.get(0).getUser_id());
            mtwd.setT_ok_ng(MatchingList.get(0).getT_ok_ng());
            mtwd.setT_interact(MatchingList.get(0).getT_interact());
            mtwd.setT_area1(MatchingList.get(0).getT_area1());
            mtwd.setT_area2(MatchingList.get(0).getT_area2());
            mtwd.setT_area3(MatchingList.get(0).getT_area3());
            mtwd.setT_time(MatchingList.get(0).getT_time());
            mtwd.setR_ok_ng(MatchingList.get(0).getR_ok_ng());
            mtwd.setR_interact(MatchingList.get(0).getR_interact());
            mtwd.setR_area1(MatchingList.get(0).getR_area1());
            mtwd.setR_area2(MatchingList.get(0).getR_area2());
            mtwd.setR_area3(MatchingList.get(0).getR_area3());
            mtwd.setR_time(MatchingList.get(0).getR_time());
        }
        System.out.println(mtwd);
        return mtwd;
    }
}
