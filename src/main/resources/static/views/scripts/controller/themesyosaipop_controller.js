/* global $routeScope */

angular.module('MyApp')
        .controller('themesyosaipopController', ['$scope', '$rootScope', '$location', '$http', 'params',
            function ($scope, $rootScope, $location, $http, params) {

                console.log(params);

                $scope.themename = params.themename;
                $scope.theme = params.theme;
                $scope.themedate = params.themedate;
                $scope.themetime = params.themetime;
                $scope.themearea = params.themearea;
                $scope.themeGenre = params.themeGenre;
                $scope.themeKaisaimax = params.themeKaisaimax;
                $scope.themeBosyumax = params.themeBosyumax;
                $scope.themedeaddate = params.themedeaddate;
                $scope.themedeadtime = params.themedeadtime;

//                console.log($scope.themedeadtime);

                //設定していないときの為に変数に別途格納しておく必要がある
                //初期値設定
                var theme_area_id = null;
                var theme_genre_id = null;
                var holding_max_person = null;
                var recruit_max_person = null;
                var deaddate = null;
                var deadtime = null;

                if ($scope.themearea !== undefined) {
                    theme_area_id = Number($scope.themearea.value);
                }
                if ($scope.themeGenre !== undefined) {
                    theme_genre_id = Number($scope.themeGenre.value);
                }
                if ($scope.themeKaisaimax !== undefined) {
                    holding_max_person = Number($scope.themeKaisaimax.value);
                }
                if ($scope.themedeaddate !== undefined) {
                    deaddate = Number($scope.themedeaddate.key);
                }
                if ($scope.themedeadtime !== undefined) {
                    deadtime = Number($scope.themedeadtime.key);
                }

                $scope.themenomikaisai = function () {
                    $http({
                        method: 'POST',
                        data: {
                            party_name: $scope.themename,
                            user_id: Number($rootScope.user_id),
                            date: Number($scope.themedate.key),
                            time: Number($scope.themetime.key),
                            artifact_id: Number($scope.theme.artifact_id),
                            theme_area_id: theme_area_id,
                            theme_genre_id: theme_genre_id,
                            holding_max_person: holding_max_person,
                            recruit_max_person: recruit_max_person,
                            deaddate: deaddate,
                            deadtime: deadtime
                        },
                        url: '/theme_Partytoroku'
                    }).success(function (data, status, headers, config) {
//                        console.log(status);
//                        console.dir(data);
                        $rootScope.syotaisum = data.syotaisum;
                        $rootScope.errormessage = data.errormessage;
                        $scope.url_id = data.url_id;

//                        console.log("dataに格納されている時点：" + data.url_id);
//                        console.log("rootScope時点：" + $scope.url_id);

                        //閉じたい
                        $rootScope.themenomitorokuflag = true;
                        $rootScope.modalInstance.close($scope.url_id);

                    }).error(function (data, status, headers, config) {
//                        console.log(status);
                    });

                };
                $scope.themenomiyametoku = function () {

                    //閉じたい
                    $rootScope.themenomitorokuflag = false;
                    $rootScope.modalInstance.close();
                };

            }]);
