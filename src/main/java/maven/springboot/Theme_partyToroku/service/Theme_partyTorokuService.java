package maven.springboot.Theme_partyToroku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_Users_LikesRepository;

import java.util.*;
import maven.springboot.Entity.M_LikesEntity;
import maven.springboot.Entity.T_Theme_PartiesEntity;
import maven.springboot.Entity.T_Users_LikesEntity;
import maven.springboot.Entity.T_Users_MatchingEntity;
import maven.springboot.Logic.Random8;
import maven.springboot.Logic.ThemeMailsend;
import maven.springboot.Repository.M_LikesRepository;
import maven.springboot.Repository.T_FriendsRepository;
import maven.springboot.Repository.T_Theme_PartiesRepository;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Repository.T_Users_MatchingRepository;
import maven.springboot.Theme_partyToroku.service.dto.Theme_partyTorokuInDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_partyTorokuWebOutDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_sansyoWebInDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_sansyoWebOutDto;

@Service  // DIする対象の目印
public class Theme_partyTorokuService {

    //DI層へ接続
    @Autowired
    private T_Theme_PartiesRepository t_Theme_PartiesRepository;

    @Autowired
    private T_Parties_UsersRepository t_Parties_UsersRepository;

    @Autowired
    private T_PartiesRepository t_PartiesRepository;

    @Autowired
    private T_Users_LikesRepository t_Users_LikesRepository;

    @Autowired
    private T_UsersRepository t_UsersRepository;

    @Autowired
    private T_Users_MatchingRepository t_Users_MatchingRepository;

    @Autowired
    private T_FriendsRepository t_FriendsRepository;

    @Autowired
    private ThemeMailsend themeMailsend;

    @Autowired
    private Random8 random;

    @Autowired
    private M_LikesRepository m_LikesRepository;

    public Theme_sansyoWebOutDto Themesansyo(Theme_sansyoWebInDto theme_sansyoWebInDto) {

        Theme_sansyoWebOutDto theme_sansyoWebOutDto = new Theme_sansyoWebOutDto();

        //Useridを元にartifact_idを持ってくる
        List<T_Users_LikesEntity> t_Users_LikesEntity_list = t_Users_LikesRepository.findByUser_id(theme_sansyoWebInDto.getUser_id());

        //格納用リスト
        List<M_LikesEntity> M_LikesEntity_list = new ArrayList();

        for (T_Users_LikesEntity t_Users_LikesEntity : t_Users_LikesEntity_list) {
            M_LikesEntity_list.add(m_LikesRepository.findByArtifact_id(t_Users_LikesEntity.getArtifact_id()));
        }

        theme_sansyoWebOutDto.setM_LikesEntity_list(M_LikesEntity_list);

        return theme_sansyoWebOutDto;
    }

    public Theme_partyTorokuWebOutDto PartyToroku(Theme_partyTorokuInDto theme_partyInDto) throws Exception {

        //エラーメッセージ
        String errormessage = "";
        //登録成功フラグ
        boolean themekaisaiflg;

        //コントローラに返すDTOを定義
        Theme_partyTorokuWebOutDto theme_partyTorokuWebOutDto = new Theme_partyTorokuWebOutDto();

        //T_partiesテーブルに格納-------------------------------------------------------------------------------
        int Party_id;
        int party_cd = 2;
        int syotaininzu = 0;

        //T_PartiesEntityをインスタンス化し、値を入れる
        T_PartiesEntity entity = new T_PartiesEntity();
        entity.setParty_cd(party_cd);
        entity.setParty_name(theme_partyInDto.getParty_name());
        entity.setUser_id(theme_partyInDto.getUser_id());
        entity.setDate(theme_partyInDto.getDate());
        entity.setTime(theme_partyInDto.getTime());
        entity.setPayer_id(theme_partyInDto.getUser_id());

        //URL設定用のurl_idを発行
        int url_id = random.makeramdom();

        //url_idもテーブルに格納
        entity.setUrl_id(url_id);

        // insert文の発行
        t_PartiesRepository.save(entity);

        //T_theme_partiesテーブルに格納------------------------------------------------------------------------
        //T_PartiesEntityをもう一度検索、更新行を取り出し、Party_idの取得
        List<T_PartiesEntity> list = t_PartiesRepository.findByAddress(
                theme_partyInDto.getUser_id(), theme_partyInDto.getTime(), theme_partyInDto.getDate());
        int i = list.size();
        Party_id = list.get(i - 1).getParty_id();

        //T_theme_entityに値を格納
        T_Theme_PartiesEntity t_Theme_PartiesEntity = new T_Theme_PartiesEntity();

        t_Theme_PartiesEntity.setParty_id(Party_id);
        t_Theme_PartiesEntity.setArtifact_id(theme_partyInDto.getArtifact_id());
        t_Theme_PartiesEntity.setTheme_area_id(theme_partyInDto.getTheme_area_id());
        t_Theme_PartiesEntity.setTheme_genre_id(theme_partyInDto.getTheme_genre_id());
        t_Theme_PartiesEntity.setHolding_max_person(theme_partyInDto.getHolding_max_person());
        t_Theme_PartiesEntity.setRecruit_max_person(theme_partyInDto.getRecruit_max_person());
        t_Theme_PartiesEntity.setDeaddate(theme_partyInDto.getDeaddate());
        t_Theme_PartiesEntity.setDeadtime(theme_partyInDto.getDeadtime());

        // insert文の発行
        t_Theme_PartiesRepository.save(t_Theme_PartiesEntity);

        //T_parties_usersテーブルに格納------------------------------------------------------------------------
        //①テーマidを気になることに登録しているUserのリストを取得する（後に七澤作成機能に対応する必要がある）------------------------------------
        List<T_Users_LikesEntity> t_users_likesentity_list = t_Users_LikesRepository.findByArtifact_id(theme_partyInDto.getArtifact_id());

        //処理前に飲み会主催者の情報を取ってきておく
        List<T_UsersEntity> syusai_list = t_UsersRepository.findByUser_id(theme_partyInDto.getUser_id());
        T_UsersEntity syusaiUsersEntity;        //主催者の情報格納用のEntity

        //一応エラー処理(飲み会主催者が登録されているか)
        if (syusai_list.size() > 0) {
            syusaiUsersEntity = syusai_list.get(0);

            //エラー処理（気になることが同じUserが存在するか）
            if (t_users_likesentity_list.size() > 0) {
                //②t_users_matchingから対象となるUserのマッチング条件リストを取得する-----------------------------------------------------------------
                List<T_Users_MatchingEntity> t_Users_MatchingEntity_list = new ArrayList();
                for (T_Users_LikesEntity t_Users_LikesEntity : t_users_likesentity_list) {
                    t_Users_MatchingEntity_list.addAll(t_Users_MatchingRepository.findByUser_idT_ok_ng(t_Users_LikesEntity.getUser_id(), 0));
                }

                //③さらにマッチング条件に応じて対象ユーザを絞ってゆく---------------------------------------------------------------------------------
                List<Integer> user_list = new ArrayList();
                boolean matchingflg;  //マッチング判定フラグ
                boolean busyoflg;  //部署が同じかの判定フラグ
                boolean tsunagariflg;  //つながり登録しているかの判定フラグ
                List<T_UsersEntity> userinfo_list;
                T_UsersEntity userinfo;

                for (T_Users_MatchingEntity t_Users_MatchingEntity : t_Users_MatchingEntity_list) {
                    matchingflg = true;    //値の初期化
                    busyoflg = false;    //値の初期化
                    tsunagariflg = false;    //値の初期化

                    //開催時間条件---------------------------------------------------
                    if (t_Users_MatchingEntity.getT_time() == 0 || t_Users_MatchingEntity.getT_time() <= theme_partyInDto.getTime()) {
//                        matchingflg = true;
                    } else {
                        matchingflg = false;
                    }

                    //エリア条件-----------------------------------------------------
                    if (t_Users_MatchingEntity.getT_area1() == 0 && matchingflg) {
//                        matchingflg = true;
                    } else if ((t_Users_MatchingEntity.getT_area1() == theme_partyInDto.getTheme_area_id() || t_Users_MatchingEntity.getT_area2() == theme_partyInDto.getTheme_area_id() || t_Users_MatchingEntity.getT_area3() == theme_partyInDto.getTheme_area_id()) && matchingflg) {
//                        matchingflg = true;
                    } else {
                        matchingflg = false;
                    }

                    //テーマのみマッチング範囲条件------------------------------------
                    //部署が同じかみる----------------
                    //まずUserの情報を持ってくる
                    userinfo_list = t_UsersRepository.findByUser_id(t_Users_MatchingEntity.getUser_id());
                    if (userinfo_list.size() > 0) {
                        userinfo = userinfo_list.get(0);
                        //部署を比べて判定
                        if (userinfo.getDepartment_id() == syusaiUsersEntity.getDepartment_id()) {
                            busyoflg = true;
                        }
                    } else {
                        errormessage = "気になること登録済みのユーザの情報がありません";
                        themekaisaiflg = false;
                    }
                    //つながり登録しているかを見る
                    if (t_FriendsRepository.findTsunagari(syusaiUsersEntity.getUser_id(), t_Users_MatchingEntity.getUser_id()).size() > 0) {
                        tsunagariflg = true;
                    }
                    //条件によって判定
                    if (t_Users_MatchingEntity.getT_interact() == 0 && matchingflg) {
//                        matchingflg = true;
                    } else if (t_Users_MatchingEntity.getT_interact() == 1 && tsunagariflg && matchingflg) {
//                        matchingflg = true;
                    } else if (t_Users_MatchingEntity.getT_interact() == 2 && busyoflg && matchingflg) {
//                        matchingflg = true;
                    } else if (t_Users_MatchingEntity.getT_interact() == 3 && (tsunagariflg || busyoflg) && matchingflg) {
//                        matchingflg = true;
                    } else {
                        matchingflg = false;
                    }

                    //useridの追加処理を行う
                    if (matchingflg) {
                        user_list.add(t_Users_MatchingEntity.getUser_id());
                    }
                }

                //募集上限に合わせて募集人数を制限する
                if (user_list.size() > theme_partyInDto.getRecruit_max_person()) {
                    for (int j = user_list.size(); j > 30; j--) {
                        user_list.remove(j);
                    }
                }

                //user_idを元に名前等をt_usersから抽出
                List<T_UsersEntity> t_UsersEntity_list = new ArrayList();

                for (Integer x : user_list) {
                    t_UsersEntity_list.addAll(t_UsersRepository.findByUser_id(x));
                }

                //抽出したUseridと名前を元にt_parties_usersテーブルに値を格納
                T_Parties_UsersEntity t_parties_usersEntity = new T_Parties_UsersEntity();

                //paymentrateの初期値
                int rate = 5;
                //showed_flagの初期値
                boolean showed_flag = false;

                t_parties_usersEntity.setParty_id(Party_id);
                t_parties_usersEntity.setParty_name(theme_partyInDto.getParty_name());
                t_parties_usersEntity.setPayment_rate(rate);
                t_parties_usersEntity.setShowed_flag(showed_flag);

                for (T_UsersEntity t_UsersEntity : t_UsersEntity_list) {
                    t_parties_usersEntity.setUser_id(t_UsersEntity.getUser_id());
                    t_parties_usersEntity.setName(t_UsersEntity.getName());

                    t_Parties_UsersRepository.save(t_parties_usersEntity);

                    //メールもここで送信
                    themeMailsend.Mailsend(t_UsersEntity.getUser_id(), theme_partyInDto.getUser_id(), url_id, theme_partyInDto.getParty_name(), theme_partyInDto.getDate(), theme_partyInDto.getTime(), theme_partyInDto.getDeaddate(), theme_partyInDto.getDeadtime());
                }

                themekaisaiflg = true;
                syotaininzu = t_UsersEntity_list.size();

            } else {
                errormessage = "気になることが同じUserが該当しませんでした";
                themekaisaiflg = false;
            }
        } else {
            errormessage = "主催者の情報が登録されていません";
            themekaisaiflg = false;
        }

        theme_partyTorokuWebOutDto.setThe_partyTorokuFlag(themekaisaiflg);
        theme_partyTorokuWebOutDto.setErrormessage(errormessage);
        theme_partyTorokuWebOutDto.setSyotaisum(syotaininzu);   
        theme_partyTorokuWebOutDto.setUrl_id(url_id);

        return theme_partyTorokuWebOutDto;
    }
}
