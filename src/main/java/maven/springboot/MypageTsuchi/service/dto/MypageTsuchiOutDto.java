package maven.springboot.MypageTsuchi.service.dto;

public class MypageTsuchiOutDto {

    private int count;

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }


}
