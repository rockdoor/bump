package maven.springboot.FriendsSansyo.service.dto;

import java.util.ArrayList;
import java.util.List;


public class FriendsSansyoOutDto {

    
    
            //private ArrayList friends_list;
            private String friend_mail;
            private String friend_name;
            private int flag; 
    
    /**
     * @return the friend_mail
     */
    public String getFriend_mail() {
        return friend_mail;
    }

    /**
     * @param friend_mail the friend_mail to set
     */
    public void setFriend_mail(String friend_mail) {
        this.friend_mail = friend_mail;
    }

    /**
     * @return the friend_name
     */
    public String getFriend_name() {
        return friend_name;
    }

    /**
     * @param friend_name the friend_name to set
     */
    public void setFriend_name(String friend_name) {
        this.friend_name = friend_name;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

}