/* global $routeScope */

angular.module('MyApp')
        .controller('DialogUserTorokuController', ['$scope', '$http', '$location', '$rootScope', '$uibModal',
            function ($scope, $http, $location, $rootScope, $uibModal) {

                $scope.doDialogUserToroku = function () {
                    var uibModalInstance = $uibModal.open({
                        templateUrl: '/views/UserToroku.html',
                        controller: 'UserTorokuController',
                        backdrop: true,
                        scope: $scope
                    });
                };
            }
        ])

        .controller('UserTorokuController', ['$scope', '$http', '$location', '$rootScope', '$uibModal',
            function ($scope, $http, $location, $rootScope, $uibModal) {
                $scope.doUserToroku = function () {
                    $http({
                        method: 'POST',
                        data: {email: $scope.email_cfm},
                        url: '/userToroku'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.flag == false) {
                            alert('すでに登録済みです！');
                            console.log("登録失敗");
                        } else {
                            $location.path('/tempentry');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                }
            }
        ]);
