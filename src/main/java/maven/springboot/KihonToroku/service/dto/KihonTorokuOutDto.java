package maven.springboot.KihonToroku.service.dto;

public class KihonTorokuOutDto {

    //OutDto内には、処理結果（true,false）のみ
    private boolean flag;
    private int user_id;
    private int eflag;

    /**
     * @return the flag
     */
    public boolean getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the eflag
     */
    public int getEflag() {
        return eflag;
    }

    /**
     * @param eflag the eflag to set
     */
    public void setEflag(int eflag) {
        this.eflag = eflag;
    }
}