package maven.springboot.RakurakuSeisan.service.dto;

import java.util.ArrayList;
import java.util.List;

public class RakurakuSeisanWebDto {

    private int user_id;
    private int party_id;
    private int payment;
    private int count; 
    private List paylist = new ArrayList();

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the payment
     */
    public int getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(int payment) {
        this.payment = payment;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the paylist
     */
    public List getPaylist() {
        return paylist;
    }

    /**
     * @param paylist the paylist to set
     */
    public void setPaylist(List paylist) {
        this.paylist = paylist;
    }
}