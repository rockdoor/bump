package maven.springboot.FriendsKensaku.service;

import maven.springboot.FriendsKensaku.Controller.FriendsKensakuController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Repository.T_FriendsRepository;
import maven.springboot.Repository.T_UsersRepository;
import java.util.*;
import maven.springboot.Entity.T_FriendsEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuInDto;
import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuOutDto;
import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuWebDto;


@Service
public class FriendsKensakuService {
    
    
        //DI層へ接続
        @Autowired
        private T_FriendsRepository t_friendsRepository;
        @Autowired
        private T_UsersRepository t_UsersRepository;
    
    public FriendsKensakuWebDto friendskensaku(FriendsKensakuInDto fkid) throws Exception {

        
        FriendsKensakuWebDto fkwd = new FriendsKensakuWebDto();
        //共通
        int eflag= 0;//エラーフラグ
        int count = 0;
        //検索条件分岐処理
        int user_id = fkid.getUser_id();
        int department_id = fkid.getDepartment_id();
        String mail = fkid.getMail();
        
        
        List<T_UsersEntity> search_user = new ArrayList();                
        
        //検索条件分岐
        if(mail != null && department_id != 0){ //両方が選択されてる場合
            search_user = t_UsersRepository.findByEmail_AND_Did("%"+mail+"%", department_id);
            System.out.println("アドレスと部署情報で検索します");
            System.out.println(search_user.size());
            count = search_user.size();
        
        }else if(mail != null && department_id == 0){ //メールアドレス検索しか行われていない場合
            search_user = t_UsersRepository.findByLikeemail("%"+mail+"%");
            System.out.println("アドレス情報のみで検索します");
            System.out.println(search_user.size());
            count = search_user.size();
        
        }else if(mail == null && department_id != 0){ //部署選択しか行われていない場合
            search_user = t_UsersRepository.findByDepartment_id(department_id);
            System.out.println("部署情報のみで検索します");
            System.out.println(search_user.size());
            count = search_user.size();
        
        }else if(mail == null && department_id == 0){ //両方が選択されていない場合
            eflag = 1;//エラーフラグON
            System.out.println("検索情報が入力されていません");

        }else{//その他の例外
            eflag = 1;//エラーフラグON
            System.out.println("不明な情報が入力されています");
            
        }
        
        //検索結果確認(既にフレンド登録しているものは除外する)
        if(count == 0){
            eflag=1; //エラー:対象のデータが一件もない
            System.out.println("対象のデータがありません");    
        }else{
            
            List<FriendsKensakuOutDto> search_list = new ArrayList<>();
            
            
            for (T_UsersEntity t_UsersEntity : search_user) {

                List<T_FriendsEntity> check_myfriends = t_friendsRepository.findByMyfriend_id(user_id,t_UsersEntity.getUser_id());
                FriendsKensakuOutDto fkod = new FriendsKensakuOutDto();
                
                if(t_UsersEntity.getUser_id() != user_id){  //検索データがユーザーのデータと一致しない場合
                    if(check_myfriends.size() == 0){        //登録対象ユーザーが未申請のユーザーだった場合
                        fkod.setName(t_UsersEntity.getName());
                        fkod.setRmail(t_UsersEntity.getEmail());
                        fkod.setUser_id(t_UsersEntity.getUser_id());
                        fkod.setSflag(check_myfriends.size());//trueが入る
                        search_list.add(fkod);            
                    }else{                                 //登録対象ユーザーが申請済みのユーザーだった場合
                        fkod.setName(t_UsersEntity.getName());
                        fkod.setRmail(t_UsersEntity.getEmail());
                        fkod.setUser_id(t_UsersEntity.getUser_id());
                        fkod.setSflag(check_myfriends.size());//falseが入る
                        search_list.add(fkod);            
                    }
                    fkwd.setFriends(search_list);
                }else{
                    eflag=1; //エラー:検索データがユーザーのデータと一致する場合
                    System.out.println("検索データがユーザーと一致しています");   
                }
                fkwd.setEflag(eflag);
            }
 
        }
        
        return fkwd;
    }
}

