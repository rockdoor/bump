package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "m_likes")
public class M_LikesEntity {

    @Id
    @GeneratedValue
    private Integer artifact_id;

    //likesの大区別(スポーツ・政治・IT・・・とか)
    @Column(name = "like_first_cd")
    private Integer like_first_cd;

    //likesの中区別(サッカー・バスケ・AI・・・とか)
    @Column(name = "like_second_cd")
    private Integer like_second_cd;

    //それぞれの区分の中での一意にする番号
    @Column(name = "like_no")
    private Integer like_no;

    //気になる項目の名前
    @Column(name = "like_name")
    private String like_name;
    
//    //登録時にチェックされたかどうかを判断する。
//    @Column(name = "checkFlag")
//    private boolean checkFlag;

    /**
     * @return the artifact_id
     */
    public Integer getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(Integer artifact_id) {
        this.artifact_id = artifact_id;
    }

    /**
     * @return the like_first_cd
     */
    public Integer getLike_first_cd() {
        return like_first_cd;
    }

    /**
     * @param like_first_cd the like_first_cd to set
     */
    public void setLike_first_cd(Integer like_first_cd) {
        this.like_first_cd = like_first_cd;
    }

    /**
     * @return the like_second_cd
     */
    public Integer getLike_second_cd() {
        return like_second_cd;
    }

    /**
     * @param like_second_cd the like_second_cd to set
     */
    public void setLike_second_cd(Integer like_second_cd) {
        this.like_second_cd = like_second_cd;
    }

    /**
     * @return the like_no
     */
    public Integer getLike_no() {
        return like_no;
    }

    /**
     * @param like_no the like_no to set
     */
    public void setLike_no(Integer like_no) {
        this.like_no = like_no;
    }

    /**
     * @return the like_name
     */
    public String getLike_name() {
        return like_name;
    }

    /**
     * @param like_name the like_name to set
     */
    public void setLike_name(String like_name) {
        this.like_name = like_name;
    }

    /**
//     * @return the checkFlag
//     */
//    public boolean isCheckFlag() {
//        return checkFlag;
//    }
//
//    /**
//     * @param checkFlag the checkFlag to set
//     */
//    public void setCheckFlag(boolean checkFlag) {
//        this.checkFlag = checkFlag;
//    }

}
