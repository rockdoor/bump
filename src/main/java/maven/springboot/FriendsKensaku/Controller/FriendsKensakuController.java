package maven.springboot.FriendsKensaku.Controller;

import java.util.HashMap;
import maven.springboot.FriendsKensaku.service.FriendsKensakuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuInDto;
import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuWebDto;
import maven.springboot.FriendsKensaku.service.dto.FriendsKensakuOutDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FriendsKensakuController {

    @Autowired
    private FriendsKensakuService friendskensakuservice;

    //初期登録画面とMapping
    @RequestMapping(value = "/friendsKensaku", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンスとして返す
    
    public FriendsKensakuWebDto requestFriendsKensaku(@RequestBody FriendsKensakuWebDto fkwd) throws Exception{
        
    //入力値をゲット    
    int user_id = fkwd.getUser_id();
    int department_id = fkwd.getDepartment_id();
    String mail = fkwd.getMail();
    
    FriendsKensakuInDto fkid = new FriendsKensakuInDto(); //Indtoを宣言
    
    fkid.setUser_id(user_id);
    fkid.setMail(mail);
    fkid.setDepartment_id(department_id);
    
    
    fkwd = friendskensakuservice.friendskensaku(fkid); //引数(InDto)をサービスに渡し、返り値(OutDto)を取得
            
        //確認
        System.out.println("***********************************");
        System.out.println("eflag=" + fkwd.getEflag());
        System.out.println("***********************************");
    
        return fkwd; //登録判定をリターン

    }

}
