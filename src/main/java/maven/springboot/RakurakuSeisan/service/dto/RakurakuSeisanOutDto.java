package maven.springboot.RakurakuSeisan.service.dto;

import java.util.List;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;

public class RakurakuSeisanOutDto {

    //OutDto内フィールド
    private int eflag;
    private int party_id;
    private int warikan_result;
    private List<T_PartiesEntity> t_partieslist;
    private List<T_Parties_UsersEntity> t_parties_userslist;
    private List<T_UsersEntity> t_userslist;
    private int user_name;

    /**
     * @return the eflag
     */
    public int getEflag() {
        return eflag;
    }

    /**
     * @param eflag the eflag to set
     */
    public void setEflag(int eflag) {
        this.eflag = eflag;
    }

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the warikan_result
     */
    public int getWarikan_result() {
        return warikan_result;
    }

    /**
     * @param warikan_result the warikan_result to set
     */
    public void setWarikan_result(int warikan_result) {
        this.warikan_result = warikan_result;
    }

    /**
     * @return the t_partieslist
     */
    public List<T_PartiesEntity> getT_partieslist() {
        return t_partieslist;
    }

    /**
     * @param t_partieslist the t_partieslist to set
     */
    public void setT_partieslist(List<T_PartiesEntity> t_partieslist) {
        this.t_partieslist = t_partieslist;
    }

    /**
     * @return the t_parties_userslist
     */
    public List<T_Parties_UsersEntity> getT_parties_userslist() {
        return t_parties_userslist;
    }

    /**
     * @param t_parties_userslist the t_parties_userslist to set
     */
    public void setT_parties_userslist(List<T_Parties_UsersEntity> t_parties_userslist) {
        this.t_parties_userslist = t_parties_userslist;
    }

    /**
     * @return the t_userslist
     */
    public List<T_UsersEntity> getT_userslist() {
        return t_userslist;
    }

    /**
     * @param t_userslist the t_userslist to set
     */
    public void setT_userslist(List<T_UsersEntity> t_userslist) {
        this.t_userslist = t_userslist;
    }

    /**
     * @return the user_name
     */
    public int getUser_name() {
        return user_name;
    }

    /**
     * @param user_name the user_name to set
     */
    public void setUser_name(int user_name) {
        this.user_name = user_name;
    }

}
