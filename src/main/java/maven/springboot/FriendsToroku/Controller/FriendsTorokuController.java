package maven.springboot.FriendsToroku.Controller;

import java.util.HashMap;
import maven.springboot.FriendsToroku.service.FriendsTorokuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.FriendsToroku.service.dto.FriendsTorokuInDto;
import maven.springboot.FriendsToroku.service.dto.FriendsTorokuWebInDto;
import maven.springboot.FriendsToroku.service.dto.FriendsTorokuWebOutDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FriendsTorokuController {

    @Autowired
    private FriendsTorokuService friendstorokuservice;

    //初期登録画面とMapping
    @RequestMapping(value = "/friendsToroku", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンスとして返す
    
    public FriendsTorokuWebOutDto requestFriendsToroku(@RequestBody FriendsTorokuWebInDto ftwid){
        
    HashMap<String,String> friend_hash = ftwid.getFriend_hash();//JSONがHashMapで着ているため
    int reuser_id = ftwid.getUser_id();
    
    FriendsTorokuInDto ftid = new FriendsTorokuInDto(); //Indtoを宣言
    ftid.setFriend_hash(friend_hash);  //Indtoにデータをセット
    ftid.setUser_id(reuser_id);
    
    FriendsTorokuWebOutDto ftwod = friendstorokuservice.friendstoroku(ftid); //引数(InDto)をサービスに渡し、返り値(OutDto)を取得
            
        //確認
        System.out.println("***********************************");
        System.out.println("eflag=" + ftwod.getFriendsTorokuFlag());
        System.out.println("***********************************");
    
        return ftwod; //登録判定をリターン

    }

}
