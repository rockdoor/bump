package maven.springboot.Mypage_nomikaiyotei.service.dto;

import java.util.List;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;

public class Mypage_nomikaiyoteiWebOutDto {

    private List<T_PartiesEntity> t_partiesEntity_list;
    private List<T_Parties_UsersEntity> t_partiesUsersEntity_list;
    private boolean syotaiflag;

    /**
     * @return the t_partiesEntity_list
     */
    public List<T_PartiesEntity> getT_partiesEntity_list() {
        return t_partiesEntity_list;
    }

    /**
     * @param t_partiesEntity_list the t_partiesEntity_list to set
     */
    public void setT_partiesEntity_list(List<T_PartiesEntity> t_partiesEntity_list) {
        this.t_partiesEntity_list = t_partiesEntity_list;
    }

    /**
     * @return the t_partiesUsersEntity_list
     */
    public List<T_Parties_UsersEntity> getT_partiesUsersEntity_list() {
        return t_partiesUsersEntity_list;
    }

    /**
     * @param t_partiesUsersEntity_list the t_partiesUsersEntity_list to set
     */
    public void setT_partiesUsersEntity_list(List<T_Parties_UsersEntity> t_partiesUsersEntity_list) {
        this.t_partiesUsersEntity_list = t_partiesUsersEntity_list;
    }

    /**
     * @return the syotaiflag
     */
    public boolean isSyotaiflag() {
        return syotaiflag;
    }

    /**
     * @param syotaiflag the syotaiflag to set
     */
    public void setSyotaiflag(boolean syotaiflag) {
        this.syotaiflag = syotaiflag;
    }



}
