package maven.springboot.MypageTsuchi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Repository.T_Parties_UsersRepository;
import java.util.*;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.MypageTsuchi.service.dto.MypageTsuchiInDto;
import maven.springboot.MypageTsuchi.service.dto.MypageTsuchiOutDto;

@Service  // DIする対象の目印
public class MypageTsuchiService {

    @Autowired
    private T_Parties_UsersRepository t_Parties_UsersRepository;
    
    public MypageTsuchiOutDto tsuchicheck(MypageTsuchiInDto mypageTsuchiInDto) {

        MypageTsuchiOutDto mypageTsuchiOutDto = new MypageTsuchiOutDto();

        // select文を発行し、要素数を代入
        List<T_Parties_UsersEntity> list = t_Parties_UsersRepository.findByUser_id(mypageTsuchiInDto.getUser_id());

        //countの初期化
        int count = 0;
        
        //出席区分が0（未回答）の数をカウント
        for (T_Parties_UsersEntity t_Parties_UsersEntity : list) {
            if(t_Parties_UsersEntity.getAttend_cd() == 0){
                count++;
            }
        }
        
        System.out.println(count);
                
        mypageTsuchiOutDto.setCount(count);
        
        return mypageTsuchiOutDto;

    }
}
