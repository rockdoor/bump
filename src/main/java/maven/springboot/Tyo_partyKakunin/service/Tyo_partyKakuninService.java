package maven.springboot.Tyo_partyKakunin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninInDto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninOutDto;

import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_parties_users_Dto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninWebOutDto;

@Service  // DIする対象の目印
public class Tyo_partyKakuninService {

    @Autowired
    private T_UsersRepository t_usersRepository;

    @Autowired
    private T_PartiesRepository t_partiesRepository;

    @Autowired
    private T_Parties_UsersRepository t_parties_usersRepository;

    public Tyo_partyKakuninWebOutDto tyoPartyHyouji(Tyo_partyKakuninInDto tyo_partykakuninindto) {
        Tyo_partyKakuninWebOutDto tkwod = new Tyo_partyKakuninWebOutDto();
        List<T_PartiesEntity> tyo_party_List = t_partiesRepository.findByUserid_Partycd(tyo_partykakuninindto.getUser_id());

        int count = tyo_party_List.size();

        if (count == 0) {
            System.out.println("★★★★★t_partiesのparty_id_Listがnullです★★★★");
        } else {
            List<Tyo_partyKakuninOutDto> tkod_List = new ArrayList();

            for (T_PartiesEntity tyo_partiesEntity : tyo_party_List) {
                System.out.println("★★★★★★" + tyo_partiesEntity.getParty_id() + "★★★★★★");
                Tyo_partyKakuninOutDto tkod = new Tyo_partyKakuninOutDto();
                //t_patiesからDateとParty_nameをWebOutDtoにいれておく。
                tkod.setDate(tyo_partiesEntity.getDate());
                tkod.setParty_name(tyo_partiesEntity.getParty_name());

                List<T_Parties_UsersEntity> t_parties_users_List
                        = t_parties_usersRepository.findByParty_id(tyo_partiesEntity.getParty_id());
                List<Tyo_parties_users_Dto> tpud_List = new ArrayList();
                for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
                    //WeboutDtoに保存する　＋　T_Usersから名前を持ってくるために変数に一度保管

                    int attend_user_id = t_Parties_UsersEntity.getUser_id();
                    Tyo_parties_users_Dto tpud = new Tyo_parties_users_Dto();
                    //t_paties_usersからAttend_cdを、t_usersからNameをWebOutDtoにいれる。
                    tpud.setAttend_cd(t_Parties_UsersEntity.getAttend_cd());
                    tpud.setName(t_usersRepository.findByUser_id(attend_user_id).get(0).getName());
                    //対象者一覧のListに上記のAttend_cdとNameをaddする。
                    tpud_List.add(tpud);
                }
                //OutDtoに上記の対象者ListをSet
                tkod.setTyo_parties_users_Dto(tpud_List);
                //上記のtkodをtkod_Listにセット
                tkod_List.add(tkod);
                //WebOutDtoにOutDtoをセット
                tkwod.setTyo_partyKakuninOutDto(tkod_List);

                System.out.println("----------はじまり--------");

                for (Tyo_parties_users_Dto tyo_parties_users_Dto : tpud_List) {
                    System.out.println("★★★★★はじまり２★★★★");
                    System.out.println(tyo_parties_users_Dto.getName());
                    System.out.println(tyo_parties_users_Dto.getAttend_cd());
                    System.out.println("★★★★★おわり２★★★★");
                }
                System.out.println("--------------おわり---------");

            }

        }
        return tkwod;
    }
}

//        Tyo_partyKakuninOutDto tkod = new Tyo_partyKakuninOutDto();
//        
//        // select文を発行し、t_partiesからuser_id、party_cd＝1(ちょい飲み)でレコードを持ってくる。
//        List<T_PartiesEntity> tyo_party_id_List = t_partiesRepository.findByUserid_Partycd(tyo_partykakuninindto.getUser_id());
//        int count = tyo_party_id_List.size();
//        if (count == 0) {
//            System.out.println("★★★★★t_partiesのparty_id_Listがnullです★★★★");
//        } else {
//            List<T_Parties_UsersEntity> t_parties_users_List = new ArrayList();
//            for (T_PartiesEntity t_PartiesEntity : tyo_party_id_List) {
//                t_parties_users_List.addAll(t_parties_usersRepository.findByParty_id(t_PartiesEntity.getParty_id())); 
//            }
//            tkod.setT_Parties_Users_List(t_parties_users_List);
//        }
//        return tkod;
//    }

