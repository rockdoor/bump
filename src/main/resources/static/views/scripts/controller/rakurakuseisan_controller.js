/* global $routeScope */

angular.module('MyApp')
        .controller('RakurakuSeisanController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {
                $scope.user_id = $rootScope.user_id;
                $scope.party_id = $rootScope.party_id;
//                $scope.user_id = $rootScope.user_id;
                
                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                $scope.flag = false;
                $scope.flag1 = false;
                $scope.flag2 = false;
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id
                    },
                    url: 'settingrakuraku'
                }).success(function (data, status, headers, config) {
                    $scope.setting = data;
                    console.log(status);
                    console.log(data);
                    angular.forEach($scope.setting.t_parties_userslist, function (user) {
                        user.count = true;
                    });
                });
                $scope.doWarikan = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id,
                            payment: Number($scope.payment),
                            //jsonに数値型で入れたいため＋を変数の前につける
                            party_id: Number($scope.party_id),
                            count: Number($scope.list.length),
                            paylist: $scope.list},
                        url: '/warikan'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.eflag == 1) {
                            alert('なにがしかのエラーが！');
                            console.log("なにがしかのエラーが！！！");
                        } else if (data.eflag == 2) {
                            alert('すでに利用されているニックネームです');
                            console.log("ニックネーム使用済み");
                        } else {
                            console.log('登録成功');
                            alert('登録完了！');
                            $location.path('/mypage');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
                $scope.doCount = function () {
                    $scope.list = [];
                    angular.forEach($scope.setting.t_parties_userslist, function (user) {
                        if (user.party_id == $scope.party_id && user.count && (user.attend_cd == 0 || user.attend_cd == 2))
                            $scope.list.push(user.user_id);
                    });
                    console.log($scope.list);
                };
                $scope.doKeisan = function () {
                    var warikanresult = (Math.ceil($scope.payment / $scope.list.length / 100) * 100);
                    return warikanresult;
                };
                $scope.doSetname = function (usersan) {
                    var username;
                    angular.forEach($scope.setting.t_userslist, function (user) {
                        if (user.user_id == usersan) {
                            username = user.name;
                        }
                    });
                    return username;
                };
                $scope.doSetname2 = function (usersama) {
                    var username2;
                    angular.forEach($scope.setting.t_userslist, function (user2) {
                        if (user2.user_id == usersama) {
                            username2 = user2.name;
                        }
                    });
                    return username2;
                };
                $scope.doZangaku = function () {
                    var zangaku = $scope.payment;
                    angular.forEach($scope.setting.t_parties_userslist, function (list) {
                        if (list.party_id == $scope.party_id && (list.attend_cd == 0 || list.attend_cd == 2)) {
                            zangaku = zangaku - list.payment;
                        }
                    });
                    return zangaku;
                };
                $scope.doKobetsu = function () {
                    console.log('登録成功');
                    alert('登録完了！');
                };
                $scope.doByyear = function () {
                    console.log('登録成功');
                    alert('登録完了！');
                };
//                $scope.doCountup = function(){
//                    angular.forEach($scope.setting.t_parties_userslist, function (countlist){
//                        if (list.party_id == $scope.party_id && list.user_id == $scope.user_id && list.payment > 0) {
//                            zangaku = zangaku - list.payment;
//                        }
//                        return ;
//                    });
//                    };
            }
        ]);
//        .controller('RakurakuSeisanController', ['$scope', function ($scope) {
//                        $scope.flag = false;
//                        $scope.flag1 = false;
//                        $scope.flag2 = false;
//                    }])