package maven.springboot.Entity;

import java.io.Serializable;
import java.util.Objects;
/**
 *
 * @author t4-yoshioka
 */
public class T_Users_LikesIdEntity implements Serializable{
    
    private Integer user_id;
    private Integer artifact_id;
    
    //主キーの一意性を保証するためにequalsとhashCodeを必ず定義しなければならない
    @Override
    public int hashCode(){

        final int prime = 31;
            int result = 1;
            result = prime * result + ((artifact_id == null) ? 0 : artifact_id.hashCode());
            result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
            return result;
         
    }
    
    @Override
    public boolean equals(Object obj){
        //何か書く
        //主キーにNullは入らないかな
//        if (this == obj)
//            return true;
//        if (obj == null)
//            return false;
//        if (getClass() != obj.getClass())
//            return false;
//        PresenceAbsenceAnswerEntityId other = (PresenceAbsenceAnswerEntityId) obj;
//        if (artifact_id == null) {
//            if (other.artifact_id != null)
//                return false;
//        } else if (!artifact_id.equals(other.artifact_id))
//            return false;
//        if (user_id == null) {
//            if (other.user_id != null)
//                return false;
//        } else if (!user_id.equals(other.user_id))
//            return false;
//        return true;

            T_Users_LikesIdEntity other = (T_Users_LikesIdEntity) obj;

            if  (artifact_id == other.artifact_id && user_id == other.user_id ){
                return true;
            }
            else{
                return false;
            }

    }
    
}