///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package maven.springboot.PresenceAbsenceAnswer.service;
//
//import maven.springboot.PresenceAbsenceAnswer.Controller.PresenceAbsenceAnswerController;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import maven.springboot.PresenceAbsenceAnswer.entity.PresenceAbsenceAnswerEntity;
//import maven.springboot.PresenceAbsenceAnswer.Repository.PresenceAbsenceAnswerRepository;
//import java.util.*;
//
//import maven.springboot.PresenceAbsenceAnswer.service.dto.PresenceAbsenceAnswerInDto;
//import maven.springboot.PresenceAbsenceAnswer.service.dto.PresenceAbsenceAnswerOutDto;
//
///**
// *
// * @author t4-yoshioka
// */
//@Service
//public class PresenceAbsenceAnswerService {
//    
//    //DI層へ接続
//    @Autowired
//    private PresenceAbsenceAnswerRepository presenceAbsenceAnswerRepository;
//   
//    //method1
//    public PresenceAbsenceAnswerOutDto answer(PresenceAbsenceAnswerInDto presenceAbsenceAnswerInDto){
//        
//        //OutDtoをインスタンス化
//        PresenceAbsenceAnswerOutDto presenceAbsenceAnswerOutDto = new PresenceAbsenceAnswerOutDto();
//        
//        //Entityクラスのインスタンス化
//        PresenceAbsenceAnswerEntity presenceAbsenceAnswerEntity = new PresenceAbsenceAnswerEntity();
//        
//        //Entityクラスに値をsetする（後々、「飲み会名」も必要となる。。）
//        presenceAbsenceAnswerEntity.setParty_id(presenceAbsenceAnswerInDto.getParty_id());
//        presenceAbsenceAnswerEntity.setUser_id(presenceAbsenceAnswerInDto.getUser_id());
//        presenceAbsenceAnswerEntity.setAttend_cd(presenceAbsenceAnswerInDto.getAttend_cd());
//        
//        //Repositoryクラスのsaveメソッドに、Entityクラスのインスタンスを渡す
//        presenceAbsenceAnswerRepository.save(presenceAbsenceAnswerEntity);
//        
//        //OutDtoに値をセット
//        presenceAbsenceAnswerOutDto.setAttend_cd(presenceAbsenceAnswerInDto.getAttend_cd());
//        
//        return presenceAbsenceAnswerOutDto;
//        
//    }
//    
//}
