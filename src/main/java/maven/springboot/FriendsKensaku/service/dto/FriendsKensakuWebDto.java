package maven.springboot.FriendsKensaku.service.dto;

import java.util.HashMap;
import java.util.List;

public class FriendsKensakuWebDto {

    //in
    private int user_id;
    private String mail;
    private int department_id; 
    //out
    private List<FriendsKensakuOutDto> friends;
    private int eflag;
    
    /**
     * @return the friends
     */
    public List<FriendsKensakuOutDto> getFriends() {
        return friends;
    }

    /**
     * @param friends the friends to set
     */
    public void setFriends(List<FriendsKensakuOutDto> friends) {
        this.friends = friends;
    }
    
    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the department_id
     */
    public int getDepartment_id() {
        return department_id;
    }

    /**
     * @param department_id the department_id to set
     */
    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    /**
     * @return the eflag
     */
    public int getEflag() {
        return eflag;
    }

    /**
     * @param eflag the eflag to set
     */
    public void setEflag(int eflag) {
        this.eflag = eflag;
    }

   
    
}