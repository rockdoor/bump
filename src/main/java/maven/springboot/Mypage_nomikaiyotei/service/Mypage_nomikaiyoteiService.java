package maven.springboot.Mypage_nomikaiyotei.service;

import java.text.SimpleDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Mypage_nomikaiyotei.service.dto.Mypage_nomikaiyoteiInDto;
import maven.springboot.Mypage_nomikaiyotei.service.dto.Mypage_nomikaiyoteiWebOutDto;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_Parties_UsersRepository;

@Service  // DIする対象の目印
public class Mypage_nomikaiyoteiService {

    @Autowired
    private T_PartiesRepository t_partiesRepository;

    @Autowired
    private T_Parties_UsersRepository t_parties_usersRepository;

    public Mypage_nomikaiyoteiWebOutDto nomikaiyotei(Mypage_nomikaiyoteiInDto mypage_nomikaiyoteiindto) {

        Mypage_nomikaiyoteiWebOutDto mypage_nomikaiyoteiWebOutDto = new Mypage_nomikaiyoteiWebOutDto();

        //出欠区分が出席のものだけ持ってくる（リポジトリで実装）
        List<T_Parties_UsersEntity> t_Parties_UsersEntity_List = t_parties_usersRepository.findByUser_idAttend_cd(mypage_nomikaiyoteiindto.getUser_id());

        int count = t_Parties_UsersEntity_List.size();

        // SimpleDateFormatクラスを使用し、現在日付を持ってくる
        Date d = new Date();

        SimpleDateFormat d1 = new SimpleDateFormat("yyyyMMdd");
        String q1 = d1.format(d);
        int nowdate = Integer.parseInt(q1);
        SimpleDateFormat d2 = new SimpleDateFormat("HHmm");
        String q2 = d2.format(d);
        int nowtime = Integer.parseInt(q2);
        System.out.println("現在日付、時刻" + q1 + q2);
        System.out.println("時刻" + nowtime);

        if (count == 0) {
            System.out.println("★★★★★招待されている飲み会はありましぇーん★★★★");
            mypage_nomikaiyoteiWebOutDto.setSyotaiflag(false);
        } else {
            mypage_nomikaiyoteiWebOutDto.setSyotaiflag(true);

            //ユーザが参加する飲み会の詳細情報格納用のリスト
            List<T_PartiesEntity> t_PartiesEntity_list = new ArrayList();

            //あとからいれてく（条件にあったもののみ）
            List<T_Parties_UsersEntity> t_Parties_UsersEntity_List2 = new ArrayList();

            int party_id;

            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_Parties_UsersEntity_List) {
                //ユーザが参加する飲み会の飲み会IDを持ってくる
                party_id = t_Parties_UsersEntity.getParty_id();

                //予め用意したユーザ参加飲み会の詳細格納用のリストに検索結果を追加(本日よりも先の予定のみ)
                t_PartiesEntity_list.addAll(t_partiesRepository.findByParty_idDate(party_id, nowdate));
            }
            //outdtoに結果を格納
            mypage_nomikaiyoteiWebOutDto.setT_partiesEntity_list(t_PartiesEntity_list);

            //t_partioesentity_listをソートしたい           

            for (T_PartiesEntity t_PartiesEntity : t_PartiesEntity_list) {
                //ユーザが参加する飲み会の飲み会IDを持ってくる
                party_id = t_PartiesEntity.getParty_id();

                //テーブル間の抽出物をあわせる為に、あえてもう一度選択しなおす（日付抽出のため）
                for (T_Parties_UsersEntity t_Parties_UsersEntity : t_Parties_UsersEntity_List) {
                    if (party_id == t_Parties_UsersEntity.getParty_id()) {
                        t_Parties_UsersEntity_List2.add(t_Parties_UsersEntity);
                    }
                }
            }
            mypage_nomikaiyoteiWebOutDto.setT_partiesUsersEntity_list(t_Parties_UsersEntity_List2);
        }
        return mypage_nomikaiyoteiWebOutDto;
    }
}
