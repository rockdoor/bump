package maven.springboot.RakurakuSansyo.Service.dto;

//import maven.springboot.RakurakuSansyo.Service.dto.*;

public class RakurakuSansyoOutDto {

    //private String w_list;
    private String name;
    //private int payment;

    /**
     * @return the w_list
     */
    //public String getW_list() {
    //    return w_list;
    //}

    /**
     * @param w_list the w_list to set
     */
    //public void setW_list(String w_list) {
    //    this.w_list = w_list;
    //}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the payment
     */
   /* public int getPayment() {
        return payment;
    }*/

    /**
     * @param payment the payment to set
     */
    /*public void setPayment(int payment) {
        this.payment = payment;
    }*/
}