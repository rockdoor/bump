package maven.springboot.Tyo_partyToroku.Controller;

import maven.springboot.Tyo_partyToroku.service.Tyo_partyTorokuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.Tyo_partyToroku.service.dto.Tyo_partyTorokuInDto;
import maven.springboot.Tyo_partyToroku.service.dto.Tyo_partyTorokuOutDto;
import maven.springboot.Tyo_partyToroku.service.dto.Tyo_partyTorokuWebDto;

import org.springframework.web.bind.annotation.RequestBody;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
public class Tyo_partyTorokuController {

    //サービスロジック層へ接続
    @Autowired   // DIコンテナからインスタンスを取得する
    private Tyo_partyTorokuService tyo_partytorokuservice;

    @RequestMapping(value = "/tyo_Partytoroku", method = RequestMethod.POST)
    @ResponseBody

    public Tyo_partyTorokuOutDto Requestparty(@RequestBody Tyo_partyTorokuWebDto tptwd) throws Exception {

        //jsonで受け取った値をKihonTorokuWebDtoに渡して、以下で各値に戻す。
        String reqparty_name = tptwd.getParty_name();
        int requser_id = tptwd.getUser_id();
        int reqstarttime = tptwd.getStarttime();
        int reqdeadline = tptwd.getDeadline();

        //パーティー区分の格納処理
        int party_cd = 1;
        int reqparty_cd = party_cd;

        ///開催日を取得する処理  
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String strDate = sdf.format(cal.getTime());
        int date = Integer.parseInt(strDate);

        ///InDTOにパーティー区分、パーティーの名前、開催日、開催時間を入れる処理
        Tyo_partyTorokuInDto tyo_partyInDto = new Tyo_partyTorokuInDto();
        tyo_partyInDto.setParty_cd(reqparty_cd);
        tyo_partyInDto.setParty_name(reqparty_name);
        tyo_partyInDto.setUser_id(requser_id);
        tyo_partyInDto.setStarttime(reqstarttime);
        tyo_partyInDto.setDeadline(reqdeadline);
        tyo_partyInDto.setDate(date);

        //Tyo_partyTorokuServiceにDtoを渡し、OutDtoにparty_idを入れる処理
        Tyo_partyTorokuOutDto tyo_partyOutDto = tyo_partytorokuservice.PartyToroku(tyo_partyInDto);

        //確認
        System.out.println("***********************************");
        System.out.println("party_cd=" + tyo_partyInDto.getParty_cd());
        System.out.println("party_name=" + tyo_partyInDto.getParty_name());
        System.out.println("user_id=" + tyo_partyInDto.getUser_id());
        System.out.println("date=" + tyo_partyInDto.getDate());
        System.out.println("starttime=" + tyo_partyInDto.getStarttime());
        System.out.println("deadline=" + tyo_partyInDto.getDeadline());
        System.out.println("party_id=" + tyo_partyOutDto.getParty_id());
        System.out.println("***********************************");

        return tyo_partyOutDto;
    }
}
