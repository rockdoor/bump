package maven.springboot.Logic;

// AWS内でメールを送るメソッドを持つクラス（シェル）ローカルでは不可
public class Mailsend{
    
    //　お試し用
//    public static void main (String[] args ) throws Exception{
//        String message = "hello";
//        String sendto = "k-anzai@nri.co.jp";
//        String subject = "testmail";
//        Mailsend mailsend = new Mailsend();
//        mailsend.send(message, sendto, subject);
//    }

    // 引数として、本文、送り先、件名を渡す
    public void send(String message, String sendto, String subject) throws Exception {
        Runtime r = Runtime.getRuntime();
        Process process = r.exec("mail -s \"" + subject + "\" " + sendto );
        byte[] b;
        b=message.getBytes();
        process.getOutputStream().write(b);
        process.getOutputStream().close();

    }

}
