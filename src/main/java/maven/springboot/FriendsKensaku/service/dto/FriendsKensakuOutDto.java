
package maven.springboot.FriendsKensaku.service.dto;

/**
 *
 * @author y-katakami
 */
public class FriendsKensakuOutDto {

    
     private String rmail;
     private String name;
     private int user_id;
     private int sflag;
     
    /**
     * @return the sflag
     */
    public int getSflag() {
        return sflag;
    }

    /**
     * @param sflag the sflag to set
     */
    public void setSflag(int sflag) {
        this.sflag = sflag;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the rmail
     */
    public String getRmail() {
        return rmail;
    }

    /**
     * @param rmail the rmail to set
     */
    public void setRmail(String rmail) {
        this.rmail = rmail;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
   
}
