
angular.module('MyApp', ['ngRoute','ui.bootstrap','xeditable', 'ngTouch','ngAnimate'])

//angular.module('MyApp', ['ngRoute','ui.bootstrap'])
        // 1configメソッドに$routeProviderプロバイダーを注入
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider
                        .when('/', {
                            templateUrl: '/views/Top.html',
                            controller: 'TopController'
                        })
//                        .when('/login', {
//                            templateUrl: '/views/mypage.html',
//                            controller: 'LoginController'
//                        })
                        .when('/usertoroku', {
                            templateUrl: '/views/UserToroku.html',
                            controller: 'UserTorokuController'
                        })
                        .when('/mypage', {
                            templateUrl: '/views/Mypage.html',
                            controller: 'MypageController'
                        })
                        .when('/kihontoroku', {
                            templateUrl: '/views/KihonToroku.html'
//                            controller: 'KihonTorokuController'
                        })
                        .when('/jyohosyosai', {
                            templateUrl: '/views/JyohoSyosai.html',
                            controller: 'JyohoSyosaiController'
                        })
                        .when('/attendancevertification', {
                            templateUrl: '/views/SyukketuKakunin.html'
                        })
                        .when('/tyoinomi_attendanceverification', {
                            templateUrl: '/views/Tyo_partyKakunin.html',
                            controller: 'Tyo_partyKakuninController'
                        })
                        .when('/tyo_partyKaitou', {
                            templateUrl: '/views/Tyo_partyKaitou.html',
                            controller: 'tyo_partyKaitouController'
                        })
                        .when('/tyo_partytoroku', {
                            templateUrl: '/views/Tyo_partyToroku.html',
                            controller: 'Tyo_partyTorokuController'
                        })
                        .when('/rakurakuseisan', {
                            templateUrl: '/views/RakurakuSeisan.html',
                            controller: 'RakurakuSeisanController'
                        })
                        .when('/rakuraku/sansyo', {
                            templateUrl: '/views/RakurakuSansyo.html',
                            controller: 'RakurakuController'
                        })
                        .when('/party/:id', {
                            templateUrl: 'views/Party.html',
                            controller: 'PartyController'
                        })

                        .when('/friendstoroku', {
                            templateUrl: '/views/FriendsToroku.html',
                            controller: 'FriendsTorokuController'
                        })
                        
                         .when('/friendskensaku', {
                            templateUrl: '/views/FriendsKensaku.html',
                            controller: 'FriendsTorokuController'
                        })
                        
                        .when('/party_top/:id', {
                            templateUrl: 'views/Party_top.html',
                            controller: 'Party_TopController'
                        })
                        
                        .when('/friendstorokucheck', {

                            templateUrl: '/views/FriendsTorokuCheck.html'
                        })
                        
                        .when('/friendssettings', {

                            templateUrl: '/views/FriendsSettings.html'
                        })

                        .when('/friendssansyo', {

                            templateUrl: '/views/FriendsSansyo.html',
                            controller: 'FriendsSansyoController'
                        })
                        .when('/tempentry', {
                            templateUrl: 'views/TempEntry.html'
                        })
                        .when('/entry/:id', {
                            templateUrl: 'views/Entry.html',
                            controller: 'EntryController'
                        })
                        .when('/likes', {
                            templateUrl: '/views/KininaruToroku.html',
                            controller: 'KininaruSettingController'
                        })
                        .when('/likescfm', {
                            templateUrl: '/views/KininaruSansyo.html',
                            controller: 'KininaruSansyoController'
                        })
                        .when('/mypagenomiyotei', {
                            templateUrl: '/views/Mypage_nomikaiyotei.html',
                            controller: 'mypagenomikaiyoteiController'
                        })
                        .when('/createparty', {
                            templateUrl: '/views/Createparty.html',
                            controller: 'createpartyController'
                        })
                        .when('/delete', {
                            templateUrl: '/views/KininaruDelete.html',
                            controller: 'KininaruDeleteController'
                        })
                        .when('/syukkestupop', {
                            templateUrl: '/views/Syukkestupop.html',
                            controller: 'SyukkestupopController'
                        })
                        .when('/themesyosaipop', {
                            templateUrl: '/views/Themesyosaipop.html',
                            controller: 'themesyosaipopController'
                        })
                        
//                        .when('/tyosyosaipop', {
//                            templateUrl: '/views/Tyosyosaipop.html',
//                            controller: 'tyosyosaipopController'
//                        })

                        .when('/personalsettings', {
                            templateUrl: '/views/PersonalSettings.html'
                        })
                        .when('/matching', {
                            templateUrl: '/views/Matching.html',
                            controller: 'MatchingController'
                        })
                        .otherwise({
                            redirectTo: '/'
                        });
            }]);
