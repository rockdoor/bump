/*  メールアドレスの上下マッチング処理 */
angular.module("MyApp").directive("match", ["$parse", function ($parse) {
        return {
            require: 'ngModel',

            link: function (scope, elem, attrs, ctrl)

            {
                scope.$watch(function () {
                    var target = $parse(attrs.match)(scope);

                    return !ctrl.$modelValue || target === ctrl.$modelValue;
                },
                        function (currentValue) {
                            ctrl.$setValidity('mismatch', currentValue);
                        }
                );
            }
        };
    }]);
/*ボタンを押したときにmyformの内容をコントローラに送る処理*/
$(document).ready(function () {
    $('#btn').click(function () {
        $('#myForm').submit();
    });
});

/*　入力box内で自動的に全角から半角へ変換する -->*/
$(function () {
    $(".js-characters-change").blur(function () {
        charactersChange($(this));
    });
    charactersChange = function (ele) {
        var val = ele.val();
        var han = val.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function (s) {
            return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
        });
        if (val.match(/[Ａ-Ｚａ-ｚ０-９]/g)) {
            $(ele).val(han);
        }
    };
});

/* ボタンクリックで検索を実行(メールアドレス登録時)*/
//$(function () {
//    $('#button_toroku').click(function (e) {
//
//        $.getJSON('/userToroku',
//                {
//                    email_cfm: $('#email').val()
//                }
//        )
//                .done(function (data) {
//                    //アラートとコンソールで確認してるだけ。消してもよし
//                    console.log(data);
//                    //条件文で遷移先を変更
//                    //結果がfalseのとき
//                    if (data.flag === false) {
//                        alert('すでに登録済みです。');
//                        console.log("false");
//                    }
//                    //結果がtrueのとき
//                    else {
//                        console.log("true");
//                        //mail_noの取得
//                        var mail_no = data.mail_no;
//
//                        //ダイアログを表示する
//                        $("#kekka-dialog").dialog({
//                            modal: true,
//                            buttons: {
//                                "Mypage": function () {
//                                    window.location.href = "/mypage?mail_no=" + mail_no;
//                                }
//                            }
//                        });
//                    }
//                });
//    });
//});
//
///*ニックネームが被っていたらはじく(基本情報登録)*/ 
//// ボタンクリックで検索を実行
//$(function () {
//    $('#button_kihon').click(function (e) {
//        console.log("bbbbbbbbbbbbbbbbbbbbbbbb");
//        // もらったURL内のmail_noを取得する
//        var mail = window.location.search.substring(1);
//        var mail_no = mail.split("=")[1];
//        $.getJSON('/kihontoroku',
//                {
//                    nickname: $('#name').val(),
//                    busho: $('#Department').val(),
//                    nyushayear: $('#joined').val(),
//                    mail_no: mail_no
//                }
//        )
//                .done(function (data) {
//                    //アラートとコンソールで確認してるだけ。消してもよし
//                    console.log(data);
//                    //条件文で遷移先を変更
//                    //結果がfalseのとき
//                    if (data.flag === false) {
//                        if (data.eflag === 1) {
//                            alert('すでに基本情報を登録しています');
//                        } else if (data.eflag === 2) {
//                            alert('すでに利用されているニックネームです');
//                        }
//                    }
//                    //結果がtrueのとき
//                    else {
//                        console.log("結果としてtrueがかえってきたよーん");
//
//                        var mail_no = data.mail_no;
//                        //ダイアログを表示する
//                        $("#kekka-dialog").dialog({
//                            modal: true,
//                            buttons: {
//                                "戻る": function () {
//                                    window.location.href = "/mypage?mail_no=" + mail_no;
//                                }
//                            }
//                        });
//                    }
//                });
//    });
//});

/*プルダウンの処理*/
angular.module('staticSelect', [])
        .controller('ExampleController', ['$scope', function ($scope) {
                $scope.data = {
                    Department: null
                };
            }]);

/*検索(基本情報参照)*/
// ボタンクリックで検索を実行 
//$(function () {
//    $('#button_kensaku').click(function (e) {
//        console.log("aaaaaaaaaaaaaaaaaaaaaa");
//        // 最初の1文字 (?記号) を除いた文字列を取得する
//        var mail = window.location.search.substring(1);
//        var mail_no = mail.split("=")[1];
//        $.getJSON('/jyohosyosai',
//                {
//                    mail_no: mail_no
//                }
//        )
//                .done(function (data) {
//                    //アラートとコンソールで確認してるだけ。消してもよし
//                    console.log(data);
//                });
//    });
//});
