package maven.springboot.Party_SyukketsuKaitou.Controller;

import maven.springboot.Party_SyukketsuKaitou.service.Party_SyukketsuKaitouService;
import maven.springboot.Party_SyukketsuKaitou.service.dto.Party_SyukketsuKaitouInDto;
import maven.springboot.Party_SyukketsuKaitou.service.dto.Party_SyukketsuKaitouWebInDto;
import maven.springboot.Party_SyukketsuKaitou.service.dto.Party_SyukketsuKaitouWebOutDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class Party_SyukketsuKaitouController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private Party_SyukketsuKaitouService party_SyukketsuKaitouservice;

    @RequestMapping(value = "/attend_update", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Party_SyukketsuKaitouWebOutDto mypage_nomikaiyotei(@RequestBody Party_SyukketsuKaitouWebInDto party_SyukketsuKaitouWebInDto) {
        int user_id = party_SyukketsuKaitouWebInDto.getUser_id();
        int party_id = party_SyukketsuKaitouWebInDto.getParty_id();
        int attend_cd = party_SyukketsuKaitouWebInDto.getAttend_cd();

        //Tyo_partyKakuninInDTO.classにReqparamに入れる処理
        Party_SyukketsuKaitouInDto party_SyukketsuKaitouInDto = new Party_SyukketsuKaitouInDto();
        party_SyukketsuKaitouInDto.setUser_id(user_id);
        party_SyukketsuKaitouInDto.setParty_id(party_id);
        party_SyukketsuKaitouInDto.setAttend_cd(attend_cd);

        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
        Party_SyukketsuKaitouWebOutDto party_SyukketsuKaitouWebOutDto = party_SyukketsuKaitouservice.syukketsukosin(party_SyukketsuKaitouInDto);

        return party_SyukketsuKaitouWebOutDto;
    }
}
