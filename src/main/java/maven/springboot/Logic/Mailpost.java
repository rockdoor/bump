package maven.springboot.Logic;

import java.util.List;
import javax.annotation.Resource;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Logic.JavaMail;
import maven.springboot.Repository.T_UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mailpost {

    @Autowired
    private T_UsersRepository t_UsersRepository;

    public void Mailsend(int user_id, int url_id, String party_name, int date, int time, int deadline) throws Exception {

        //開催日を○○○○年○○月○○日に
        StringBuilder hizuke = new StringBuilder();
        hizuke.append(date);
        hizuke.insert(4, "年");
        hizuke.insert(7, "月");
        hizuke.insert(10, "日");

        //開催日を○○時○○分に        
        StringBuilder jikan = new StringBuilder();
        jikan.append(time);
        jikan.insert(2, "時");
        jikan.insert(5, "分");
        
        //登録締め切り時間を○○時○○分に
        StringBuilder shimekiri = new StringBuilder();
        shimekiri.append(deadline);
        shimekiri.insert(2, "時");
        shimekiri.insert(5, "分");

        //User_idをもとに部署idを取り出す為、検索をおこなう
        List<T_UsersEntity> list1 = t_UsersRepository.findByUser_id(user_id);

        //ありえないが一応あるか確認
        int count = list1.size();

        //メール本文の改行
        String crlf = System.getProperty("line.separator");

        //一応エラーメッセージを格納しておく
        String errormessage;

        if (count > 0) {
            //部署idを取り出す
            int department_id = list1.get(0).getDepartment_id();

            //メール本文に記載する為、名前を格納しておく
            String name = list1.get(0).getName();

            //部署idが等しいUserを検索
            List<T_UsersEntity> list2 = t_UsersRepository.findByDepartment_id(department_id);

            int count1 = list2.size();

            if (count1 > 0) {
                //メール送信クラスをインスタンス化しておく
                //Mailsend mailsend = new Mailsend();
                String subject = "飲み会招待通知";

                //検索されたuserのアドレスに通知メールを送信
                for (T_UsersEntity entity : list2) {
                    //メール送信に用いる値を定義
                    String sendto = entity.getEmail();
                    String message = entity.getName() + "さん。" + crlf + crlf
                            + "Bump!からのお知らせです。" + crlf
                            + name + "さんに飲み会に招待されました！！" + crlf + crlf
                            + "--------------------------------------------" + crlf
                            + "飲み会名：" + party_name + crlf
                            + "開催時間：" + hizuke + "    " + jikan + crlf
                            + "出欠締切：" + hizuke + "    " + shimekiri + crlf
                            + "--------------------------------------------" + crlf + crlf
                            + "下記サイトにて出欠の回答を行ってください！！"+ crlf
                            + "http://localhost:8080/#/party_top/" + url_id;

                    //mail送信メソッドでメール送信(シェルなのでAWSのみ起動)
                    //mailsend.send(message, sendto, subject);
                    //ローカルでのテスト用
                    JavaMail javamail = new JavaMail();
                    //nriドメインだったら送れる設定になってる
                    String from = "a-nanasawa@nri.co.jp";
                    javamail.send(sendto, subject, message, from);

                }
            } else {
                errormessage = "同じ部署の登録者がいませんでした。";
            }
        } else {
            errormessage = "ユーザーが存在しません。";
        }
    }

}
