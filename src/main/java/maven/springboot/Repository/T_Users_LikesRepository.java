package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;
import maven.springboot.Entity.T_Users_LikesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_Users_LikesRepository extends JpaRepository<T_Users_LikesEntity, Integer> {

    @Query(value = "select t from T_Users_LikesEntity t where t.user_id = :user_id")
    public List<T_Users_LikesEntity> findByUser_id(@Param("user_id") int user_id);

    @Query(value = "select t from T_Users_LikesEntity t where t.artifact_id = :artifact_id")
    public List<T_Users_LikesEntity> findByArtifact_id(@Param("artifact_id") int artifact_id);
}
