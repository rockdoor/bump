/* global $routeScope */

angular.module('MyApp')
        .controller('MypageController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {

                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/mypage_nomikaiyotei'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    $scope.results2 = data;
                    console.dir(data);
                    console.log(status);

                    var date;
                    var time;

                    //今日の日付データを変数hidukeに格納
                    var hiduke = new Date();

                    //年・月・日・曜日を取得する
                    var year = hiduke.getFullYear();
                    var month = hiduke.getMonth() + 1;
                    var day = hiduke.getDate();

                    if (month < 10 && day < 10) {
                        var today = "" + year + "0" + month + "0" + day;
                    } else if (month >= 10 && day < 10) {
                        var today = "" + year + "" + month + "0" + day;
                    } else if (month < 10 && day >= 10) {
                        var today = "" + year + "0" + month + "" + day;
                    } else {
                        var today = "" + year + "" + month + "" + day;
                    }

                    console.log("本日日付" + today);
                    var savedate = "";

                    //招待されている飲み会がある場合
                    if ($scope.results.syotaiflag === true) {

                        //リストの長さ
                        var l = Object.keys($scope.results.t_partiesEntity_list).length;

                        $scope.syotaiflag = true;

                        //今日日付のものと、そうでないもので分ける。
                        for (k = 0; k < l; k++) {
                            savedate = "" + $scope.results.t_partiesEntity_list[k].date;
                            console.log(savedate);
                            if (savedate === today) {
                                $scope.results.t_partiesEntity_list[k].user_id = true;
                                $scope.results.t_partiesEntity_list[k].party_cd = false;
                            } else {
                                $scope.results.t_partiesEntity_list[k].user_id = false;
                                $scope.results.t_partiesEntity_list[k].party_cd = true;
                            }
                        }

                        //年月日を表示できるように直す。
                        for (k = 0; k < l; k++) {
                            date = $scope.results.t_partiesEntity_list[k].date;
                            date = date + "";
                            date = date.slice(4);
                            date = date.slice(0, 2) + '/' + date.slice(2, date.length);
                            $scope.results.t_partiesEntity_list[k].date = date;

                            time = $scope.results.t_partiesEntity_list[k].time;
                            time = time + "";
                            time = time.slice(0, 2) + ':' + time.slice(2, time.length);
                            $scope.results.t_partiesEntity_list[k].time = time;
                        }

                        var party_id;
                        var showed_flag;
                        var tsuchi;
                        //リストの長さ
                        var l2 = Object.keys($scope.results.t_partiesUsersEntity_list).length;

                        //既読フラグを表示させたい為、t_partiesのpayer_idのカラム部分を代用
                        //ついでにURLを作成
                        for (k = 0; k < l; k++) {
                            party_id = $scope.results.t_partiesEntity_list[k].party_id;
                            url_id = $scope.results.t_partiesEntity_list[k].url_id;
                            $scope.results.t_partiesEntity_list[k].url_id = '#/party/' + url_id;
                            for (j = 0; j < l2; j++) {
                                if (party_id === $scope.results.t_partiesUsersEntity_list[k].party_id) {
                                    showed_flag = $scope.results.t_partiesUsersEntity_list[k].showed_flag;
                                    if (showed_flag === false) {
                                        tsuchi = "New!";

//                                    //DBの通知フラグを更新する際に利用する
//                                    $scope.results2.t_partiesUsersEntity_list[k].showed_flag = true;
                                    } else {
                                        tsuchi = null;
                                    }
                                } else {
                                    tsuchi = null;
                                }
                            }
                            $scope.results.t_partiesEntity_list[k].payer_id = tsuchi;
                        }

                        console.dir(data);
                        //DBの通知フラグを更新する処置を記述
                    }
                    //招待されている飲み会がない場合
                    else {
                        $scope.syotaiflag = false;
                    }


                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

            }]);
