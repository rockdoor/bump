package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.T_UsersEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_UsersRepository extends JpaRepository<T_UsersEntity, Integer> {

    @Query(value = "select t from T_UsersEntity t where t.email = :email")
    public List<T_UsersEntity> findByEmail(@Param("email") String email);
    
    @Query(value = "select t from T_UsersEntity t where t.email like :email")
    public List<T_UsersEntity> findByLikeemail(@Param("email") String email);

    @Query(value = "select t from T_UsersEntity t where t.user_id = :user_id")
    public List<T_UsersEntity> findByUser_id(@Param("user_id") int user_id);

    @Query(value = "select t from T_UsersEntity t where t.department_id = :department_id")
    public List<T_UsersEntity> findByDepartment_id(@Param("department_id") int department_id);

    @Query(value = "select t from T_UsersEntity t where t.name = :name")
    public List<T_UsersEntity> findByName(@Param("name") String name);
        
    @Query(value = "select t from T_UsersEntity t where t.email like :email and t.department_id = :department_id")
    public List<T_UsersEntity> findByEmail_AND_Did(@Param("email") String email, @Param("department_id") int department_id);

//        @Modifying
//        @Query("update T_UsersEntity t set t.name = :name where t.user_id = :user_id")
//        int setNameByUser_id(@Param("name") String name, @Param("user_id") int user_id);
//
//        @Modifying
//        @Query("update T_UsersEntity t set t.department_id = :department_id where t.user_id = :user_id")
//        int setDepartment_idByUser_id(@Param("department_id") int department_id, @Param("user_id") int user_id);
//
//        @Modifying
//        @Query("update T_UsersEntity t set t.joinedyear = :joinedyear where t.user_id = :user_id")
//        int setJoinedyearByUser_id(@Param("joinedyear") int joinedyear, @Param("user_id") int user_id);        
//        @Modifying
//        @Query("update T_UsersEntity t set t.name = :name, t.department_id = :department_id, t.joinedyear = :joinedyear where t.user_id = :user_id")
//        int setByUser_id(@Param("name")String name, @Param("department_id")int department_id, @Param("joinedyear")int joinedyear, @Param("user_id")int user_id);
    
}
