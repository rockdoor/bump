package maven.springboot.Logic;

import java.util.List;
import maven.springboot.Entity.T_PasswordsEntity;
import maven.springboot.Repository.T_PasswordsRepository;
import static org.eclipse.jdt.internal.compiler.parser.Parser.name;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntryMail {

    @Autowired
    private T_PasswordsRepository t_PasswordsRepository;

    public void sendMail(String email) throws Exception {
        //メール本文に記載する為の情報を格納しておく(個人名、飲み会名、飲み会日)
        List<T_PasswordsEntity> passlist = t_PasswordsRepository.findByEmail(email);
        int url_id = passlist.get(0).getUrl_id();
        String crlf = System.getProperty("line.separator");
        //AWS環境用のメール送信クラスをインスタンス化しておく
        //Mailsend mailsend = new Mailsend();

        String subject = "Bump! 新規登録のご案内";

        //検索されたuserのアドレスに通知メールを送信
        //メール送信に用いる値を定義
        String sendto = email;
        String message
                = "━  [Bump! 新規登録] ━━━━━━━━━━━━━━━━━━━━" + crlf + crlf
                + "　この度は Bump! にお申し込みいただき、" + crlf
                + "　　　　　　　誠にありがとうございます！☆" + crlf
                + "　下記の案内にしたがって、ユーザー登録を進めてください" + crlf + crlf
                + "━━━━━━━━━━━━━━━━━━━ Bump!運営事務局 ━━━" + crlf + crlf
                + "お客様の新規登録申込を確かに承りました。" + crlf
                + "専用の新規登録URLから、ユーザー登録を行ってください。" + crlf + crlf
                + "* 新規登録URL ： http://localhost:8080/#/entry/" + url_id + crlf + crlf
                + "[お願い]" + crlf
                + "上記の無料メールのお申し込み手続きをした覚えがない場合、誤登録の可能性があります。" + crlf
                + "このような場合は、お手数ですが下記削除担当までご連絡いただきますようお願い申し上げます。" + crlf
                + "* 削除担当 : y-katakami@nri.co.jp" + crlf + crlf
                + "それでは、Bump! を使って素晴らしい飲み会Lifeをお楽しみください。";

        //mail送信メソッドでメール送信(シェルなのでAWSのみ起動)
        //mailsend.send(message, sendto, subject);
        //ローカルでのテスト用
        JavaMail javamail = new JavaMail();
        //nriドメインだったら送れる設定になってる
        String from = "d-tagomori@nri.co.jp";
        javamail.send(sendto, subject, message, from);
    }
}
