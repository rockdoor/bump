/* global $routeScope */

angular.module('MyApp')
        .controller('KihonTorokuContoroller', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {

                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                $scope.doKihonToroku = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id,
                            name: $scope.name,
                            //jsonに数値型で入れたいため＋を変数の前につける
                            department_id: Number($scope.department_id),
                            joinedyear: Number($scope.joinedyear),
                            birth: Number($scope.birth)},
                        url: '/kihontoroku'
                    }).success(function (data, status) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.eflag == "1") {
                            alert('すでに基本情報を登録しています');
                            console.log("登録済み");
                        } else if (data.eflag == "2") {
                            alert('すでに利用されているニックネームです');
                            console.log("ニックネーム使用済み");
                        } else {
                            console.log('登録成功');
                            alert('登録完了！');
                            $location.path('/mypage');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
                $scope.setTrends = function (n) {
                    var array = [];
                    for (var i = 0; i < n; ++i)
                        array.push($scope.trendlist[i]);
                    return array;
                };
            }]);