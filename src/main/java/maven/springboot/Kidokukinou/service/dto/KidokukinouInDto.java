package maven.springboot.Kidokukinou.service.dto;

public class KidokukinouInDto {

    private int party_id;
    private int user_id;

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

}
