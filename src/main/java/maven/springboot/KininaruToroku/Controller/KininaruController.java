package maven.springboot.KininaruToroku.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.KininaruToroku.service.KininaruSettingService;
import maven.springboot.KininaruToroku.service.KininaruTagService;
import maven.springboot.KininaruToroku.service.KininaruTorokuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.KininaruToroku.service.dto.KininaruSettingWebOutDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTagInDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTagWebInDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTorokuInDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTorokuWebInDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTorokuWebOutDto;
import maven.springboot.Tyo_partyKakunin.Controller.Tyo_partyKakuninController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KininaruController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruSettingService kininarusettingservice;
//気になること登録ページ生成用のJava⇒単純にlikesのマスタを出力するだけ。

    @RequestMapping(value = "/kininarusetting")
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruSettingWebOutDto requestKininaruSetting() throws JsonProcessingException {
        //kininaruSetting.serviceには渡す値は無い(マスタから持ってくるだけ)
        KininaruSettingWebOutDto kswod = kininarusettingservice.kininaruSetting();
        //確認
        System.out.println(new ObjectMapper().writeValueAsString(kswod));
        return kswod;
    }

    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruTorokuService kininarutorokuservice;
//登録ボタン押下後の処理⇒個人用のt_users_likesにuser_idと紐付けてインサートする。

    @RequestMapping(value = "/kininarutoroku", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruTorokuWebOutDto requestKininaruToroku(@RequestBody KininaruTorokuWebInDto ktwid) {
        int requser_id = ktwid.getUser_id();
        System.out.println("■■■■■■"+ktwid.getArtifact_list());
        List<String>reqartifact_list = ktwid.getArtifact_list();

        KininaruTorokuInDto ktid = new KininaruTorokuInDto();
        ktid.setArtifact_list(reqartifact_list);
        ktid.setUser_id(requser_id);

        KininaruTorokuWebOutDto ktwod = kininarutorokuservice.kininaruToroku(ktid);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(ktwod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Tyo_partyKakuninController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ktwod;
    }

//気になることのタグを追加する。（戻り値はSettingと同じなので、KininaruSettingWebOutDtoを使いまわす）
    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruTagService kininarutagservice;
    //登録ボタン押下後の処理⇒個人用のt_users_likesにuser_idと紐付けてインサートする。

    @RequestMapping(value = "/kininarutag", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruSettingWebOutDto requestKininaruTag(@RequestBody KininaruTagWebInDto tagwid) {
        
        int reqadd_like_first_cd = tagwid.getAdd_like_first_cd();
        int reqadd_like_second_cd = tagwid.getAdd_like_second_cd();
        String reqadd_like_name = tagwid.getAdd_like_name();
        
        KininaruTagInDto tagid = new KininaruTagInDto();
        tagid.setAdd_like_first_cd(reqadd_like_first_cd);
        tagid.setAdd_like_second_cd(reqadd_like_second_cd);
        tagid.setAdd_like_name(reqadd_like_name);
        
        KininaruSettingWebOutDto kswod = kininarutagservice.addTag(tagid);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(kswod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Tyo_partyKakuninController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kswod;
    }
}
