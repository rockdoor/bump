package maven.springboot.Logic;

import java.util.List;
import javax.annotation.Resource;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_PasswordsEntity;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_PasswordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Random8 {

    @Autowired
    private T_PartiesRepository t_partiesRepository;
    
    @Autowired
    private T_PasswordsRepository t_passwordsRepository;
    

    public int makeramdom() {
        int ramdomnum = 0;
        boolean flag = false;   //url_idが一意であるか
        int count;              //url_idからの検索ヒット数

        while (flag == false) {
            //randomメソッドで生成した実数を変数に代入
            ramdomnum = (int) (Math.random() * 100000000);

            System.out.println("ランダムに生成した数" + ramdomnum);
            
            //テーブルに同じ実数が存在しないか確認
            List<T_PartiesEntity> list = t_partiesRepository.findByUrl_id(ramdomnum);
            count = list.size();

            //Url_idが重複していなかったらループを抜ける
            if (count == 0) {
                flag = true;
            }

        }
        return ramdomnum;
    }

        public int makeramdomforEntry() {
        int ramdomnum = 0;
        boolean flag = false;   //url_idが一意であるか
        int count;              //url_idからの検索ヒット数

        while (flag == false) {
            //randomメソッドで生成した実数を変数に代入
            ramdomnum = (int) (Math.random() * 100000000);

            System.out.println("ランダムに生成した数" + ramdomnum);
            
            //テーブルに同じ実数が存在しないか確認
            List<T_PasswordsEntity> list = t_passwordsRepository.findByUrl_id(ramdomnum);
            count = list.size();

            //Url_idが重複していなかったらループを抜ける
            if (count == 0) {
                flag = true;
            }

        }
        return ramdomnum;
    }
}
