package maven.springboot.KininaruToroku.service.dto;


public class KininaruTorokuWebOutDto {
    private boolean KininaruTorokuFlag;

    /**
     * @return the KininaruTorokuFlag
     */
    public boolean isKininaruTorokuFlag() {
        return KininaruTorokuFlag;
    }

    /**
     * @param KininaruTorokuFlag the KininaruTorokuFlag to set
     */
    public void setKininaruTorokuFlag(boolean KininaruTorokuFlag) {
        this.KininaruTorokuFlag = KininaruTorokuFlag;
    }
    
}