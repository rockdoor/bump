///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
///**
// *
// * @author t4-yoshioka
// */
//package maven.springboot.PresenceAbsenceAnswer.Controller;
//
//import maven.springboot.PresenceAbsenceAnswer.service.PresenceAbsenceAnswerService;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import maven.springboot.PresenceAbsenceAnswer.service.dto.PresenceAbsenceAnswerInDto;
//import maven.springboot.PresenceAbsenceAnswer.service.dto.PresenceAbsenceAnswerOutDto;
//import maven.springboot.UserToroku.service.UserTorokuService;
//import maven.springboot.UserToroku.service.dto.UserTorokuInDto;
//import maven.springboot.UserToroku.service.dto.UserTorokuOutDto;
//
//
//@Controller
//public class PresenceAbsenceAnswerController {
//    
//    //1つのコントローラにまとめるならいらないかも。。
////    @RequestMapping("/partieslist")
////    public String partiesList()
////    {
////        return "YoshiokaSample1";
////    } 
//    
//    @RequestMapping("/yoshiokatop")
//    public String yoshiokaTop()
//    {
//        return "TopYoshioka";
//    }
//    
//    @Autowired
//    private PresenceAbsenceAnswerService presenceAbsenceAnswerService;
//    
//    
//    //吉岡：出欠回答画面表示の為のコントローラ作成（開始）
//    @RequestMapping("/partieslist")
//    @ResponseBody //戻り値をＪＳＯＮ形式で返す
//    public PresenceAbsenceAnswerOutDto requestAnswer12345(@RequestParam("user_id") int reqUser_id)//これはDBから持ってくる
//    {
//        //InDTO.classにRequestParamを入れる処理
//        PresenceAbsenceAnswerInDto presenceAbsenceAnswerInDto = new PresenceAbsenceAnswerInDto();
//        presenceAbsenceAnswerInDto.setUser_id(reqUser_id);
//        
//        
//        //PresenceAbsenceAnswerServiceにInDtoを渡し、OutDtoとして結果をもらう;
//        PresenceAbsenceAnswerOutDto presenceAbsenceAnswerOutDto = presenceAbsenceAnswerService.answer(presenceAbsenceAnswerInDto);
//        //presenceAbsenceAnswerOutDto.setAttend_cd("DBからとってきた「飲み会のID」（条件：DB上のuser_id = :user_id）");
//        presenceAbsenceAnswerOutDto.setAttend_cd("DBからとってきた「飲み会のID」（条件：DB上のuser_id = :user_id）");
//        //↓後々必要
//        //presenceAbsenceAnswerOutDto.set????("DBからとってきた「飲み会の名前」（条件：DB上のuser_id = :user_id）");
//        //presenceAbsenceAnswerOutDto.setParty_id("DBからとってきた「参加/不参加」（条件：DB上のuser_id = :user_id）");
//        presenceAbsenceAnswerOutDto.setParty_id("DBからとってきた「参加/不参加」（条件：DB上のuser_id = :user_id）");
//        
//        //来てるか確認します
//        System.out.println("***********************:");
//        System.out.println(presenceAbsenceAnswerOutDto);
//        System.out.println("***********************:");
//        
//        
//        //OutDtoを返す
//        return presenceAbsenceAnswerOutDto;      
//
//        
//    }
//    
//    
//    //吉岡：出欠回答画面表示の為のコントローラ作成（終了）
//    
//    @RequestMapping(value = "/presenceabsenceanswer", method = RequestMethod.GET)
//    @ResponseBody //戻り値をＪＳＯＮ形式で返す
//    public PresenceAbsenceAnswerOutDto requestAnswer(@RequestParam("party_id") int reqParty_id,
//                                                       @RequestParam("user_id") int reqUser_id,
//                                                       @RequestParam("attend_cd") int reqAttend_cd){
//        
//        //InDTO.classにRequestParamを入れる処理
//        PresenceAbsenceAnswerInDto presenceAbsenceAnswerInDto = new PresenceAbsenceAnswerInDto();
//        presenceAbsenceAnswerInDto.setParty_id(reqParty_id);
//        presenceAbsenceAnswerInDto.setUser_id(reqUser_id);
//        presenceAbsenceAnswerInDto.setAttend_cd(reqAttend_cd);
//        
//        //PresenceAbsenceAnswerServiceにInDtoを渡し、OutDtoとして結果をもらう;
//        PresenceAbsenceAnswerOutDto presenceAbsenceAnswerOutDto = presenceAbsenceAnswerService.answer(presenceAbsenceAnswerInDto);
//        presenceAbsenceAnswerOutDto.setAttend_cd(reqAttend_cd);
//        
//        //出力してみます
////        System.out.println("***********************:");
////        System.out.println(presenceAbsenceAnswerOutDto);
////        System.out.println("***********************:");
//        
//        //OutDtoを返す
//        return presenceAbsenceAnswerOutDto;       
//        
//        
//    }
//    
//    
//    //吉岡が↓を追加
//    /*★★★★AddressHanteiLogic.javaに渡して戻り値（OK、NG）に応じたJSONを返す。*/
////    @Autowired   // DIコンテナからインスタンスを取得する
////    private UserTorokuService usertorokuservice;
////    
////    @RequestMapping(value = "/mypageyoshioka", method = RequestMethod.GET)
////    @ResponseBody //戻り値をレスポンス(JSON)として返す。
////    public UserTorokuOutDto requestLogin(@RequestParam("email") String reqaddress) {
////        //InDTO.classにReqparamに入れる処理
////        UserTorokuInDto usertorokuindto = new UserTorokuInDto();
////        usertorokuindto.setAddress(reqaddress);
////        
////        System.out.println(reqaddress);
////        
////        //UserTorokuServiceにDtoを渡し、結果をもらう;
////        UserTorokuOutDto utod = usertorokuservice.login(usertorokuindto);
////        //確認
////        System.out.println(utod.getFlag());
////
////        return utod;
////    }
//
//
//    
//    
//    //飲み会id、出欠id、？？？の入力項目表示画面を返す（疎通確認用）
////    @RequestMapping("/partieslist")
////    public String yoshiokaSample1() {
////        return "YoshiokaSample1";
////    }
//    
// 
//    
//    
//    @RequestMapping("/mypageyoshioka")
//    public String mypageYoshioka() {
//        return "MypageYoshioka";
//    }
//    
//    
//    //仮に作ります
//    @RequestMapping("/partieslist1")
//    public String yoshiokaSample11111() {
//        return "YoshiokaSample1";
//    }
//    
//    
//
//
//    
//    
//}
