/* global $routeScope */

angular.module('MyApp')
        .controller('JyohoSyosaiController', ['$scope', '$http', '$location', '$rootScope', '$filter',
            function ($scope, $http, $location, $rootScope, $filter) {
//                $scope.js_onload = function () {

                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);
                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/jyohoSyosai'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    $scope.reloaddata = data;
                    console.log(status);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
                $scope.statuses = [
                    {value: 1, text: '証券システムプロジェクト部'},
                    {value: 2, text: '証券リテールフロントシステム一部'},
                    {value: 3, text: '証券リテールフロントシステム二部'},
                    {value: 4, text: '証券リテールフロントシステム三部'},
                    {value: 5, text: '証券コアシステム一部'},
                    {value: 6, text: '証券コアシステム二部'},
                    {value: 7, text: '証券コアシステム三部'},
                    {value: 8, text: '証券グローバルシステム一部'},
                    {value: 9, text: '証券グローバルシステム二部'},
                    {value: 10, text: '証券グローバルシステム三部'}
                ];
                $scope.showStatus = function () {
                    var selected = $filter('filter')($scope.statuses, {value: $scope.results.department_id});
                    return ($scope.results.department_id && selected.length) ? selected[0].text : 'Not set';
                };
                $scope.updateOn = function () {
                    $scope.$watch('results.email', function (newValue, oldValue, scope) {
                        if (!angular.equals(newValue, oldValue)) {
                            scope.updateflag = true;
                        }
                    });
                    $scope.$watch('results.name', function (newValue, oldValue, scope) {
                        if (!angular.equals(newValue, oldValue)) {
                            scope.updateflag = true;
                        }
                    });
                    $scope.$watch('results.department_id', function (newValue, oldValue, scope) {
                        if (!angular.equals(newValue, oldValue)) {
                            scope.updateflag = true;
                        }
                    });
                    $scope.$watch('results.joinedyear', function (newValue, oldValue, scope) {
                        if (!angular.equals(newValue, oldValue)) {
                            scope.updateflag = true;
                        }
                    });
                };
                //再度非同期通信が必要であるのか。甚だ疑問である。
                $scope.reload = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id},
                        url: '/jyohoSyosai'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        console.log($scope.results.department_name);
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
                $scope.doKihonUpdate = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id,
                            name: $scope.results.name,
                            //jsonに数値型で入れたいため＋を変数の前につける
                            department_id: Number($scope.results.department_id),
                            joinedyear: Number($scope.results.joinedyear),
                            email: $scope.results.email},
                        url: '/kihonupdate'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        alert('更新完了！');
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    $scope.updateflag = false;
                };

                //$watchGroupを使うと読み込み時にも動いてしまうようである。
//                $scope.$watchGroup([
//                    'results.email', 'results.name', 'results.department_name'
//                ], function (newValue, oldValue, scope) {
//                    console.log($scope.updateflag);
//                    scope.updateflag = true;
//                    console.log($scope.updateflag);
//                });
            }]);
