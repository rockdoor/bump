package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "t_parties")
public class T_PartiesEntity {

    @Id
    @GeneratedValue
    private Integer party_id;

    @Column(name = "party_name")
    private String party_name;

    @Column(name = "party_cd")
    private int party_cd;

    @Column(name = "user_id")
    private int user_id;

    @Column(name = "payer_id")
    private int payer_id;

    @Column(name = "date")
    private int date;

    @Column(name = "time")
    private int time;
   
    @Column(name = "url_id")
    private int url_id;

    /**
     * @return the party_id
     */
    public Integer getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(Integer party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the party_name
     */
    public String getParty_name() {
        return party_name;
    }

    /**
     * @param party_name the party_name to set
     */
    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    /**
     * @return the party_cd
     */
    public int getParty_cd() {
        return party_cd;
    }

    /**
     * @param party_cd the party_cd to set
     */
    public void setParty_cd(int party_cd) {
        this.party_cd = party_cd;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the payer_id
     */
    public int getPayer_id() {
        return payer_id;
    }

    /**
     * @param payer_id the payer_id to set
     */
    public void setPayer_id(int payer_id) {
        this.payer_id = payer_id;
    }

    /**
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(int date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public int getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the url_id
     */
    public int getUrl_id() {
        return url_id;
    }

    /**
     * @param url_id the url_id to set
     */
    public void setUrl_id(int url_id) {
        this.url_id = url_id;
    }

    
}