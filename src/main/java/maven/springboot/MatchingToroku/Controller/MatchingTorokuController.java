package maven.springboot.MatchingToroku.Controller;

import maven.springboot.MatchingToroku.Controller.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.MatchingToroku.service.MatchingTorokuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.MatchingToroku.service.dto.MatchingTorokuInDto;
import maven.springboot.MatchingToroku.service.dto.MatchingTorokuWebDto;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class MatchingTorokuController {

    /*★★★★mail_no(個人識別番号)に応じた各情報のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private MatchingTorokuService matchingtorokuservice;

    @RequestMapping(value = "/matching", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public MatchingTorokuWebDto requestMatching(@RequestBody MatchingTorokuInDto mtid) {
        int requser_id = mtid.getUser_id();
        //MatchingTorokuInDto.classにReqparamに入れる処理
        MatchingTorokuInDto matchingtorokuindto = new MatchingTorokuInDto();
        matchingtorokuindto.setUser_id(requser_id);

        //MatchingTorokuServiceにDtoを渡し、結果をもらう;
        MatchingTorokuWebDto tkwod = matchingtorokuservice.MatchingHyoji(matchingtorokuindto);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(tkwod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(MatchingTorokuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tkwod;
    }

    @RequestMapping(value = "/MatchingToroku", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public MatchingTorokuWebDto mypage_nomikaiyotei(@RequestBody MatchingTorokuInDto mtid) {
        int user_id = mtid.getUser_id();

        //MatchingTorokuInDt.classにReqparamに入れる処理
        MatchingTorokuInDto matchingtorokuindto = new MatchingTorokuInDto();
        matchingtorokuindto.setUser_id(user_id);
        matchingtorokuindto.setT_ok_ng(mtid.getT_ok_ng());
        matchingtorokuindto.setT_interact(mtid.getT_interact());
        matchingtorokuindto.setT_time(mtid.getT_time());
        matchingtorokuindto.setT_area1(mtid.getT_area1());
        matchingtorokuindto.setT_area2(mtid.getT_area2());
        matchingtorokuindto.setT_area3(mtid.getT_area3());

        //MatchingTorokuServiceにDtoを渡し、結果をもらう;
        MatchingTorokuWebDto matchingtorokuwebdto = matchingtorokuservice.kousin(matchingtorokuindto);

        return matchingtorokuwebdto;
    }
}
