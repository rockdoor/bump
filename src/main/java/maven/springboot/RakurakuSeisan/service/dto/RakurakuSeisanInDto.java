package maven.springboot.RakurakuSeisan.service.dto;

import java.util.ArrayList;
import java.util.List;

public class RakurakuSeisanInDto {

    //InDto内フィールド
    private int user_id;
    private int party_id;
    private int count;
    private int payment;
    private List paylist = new ArrayList();

    //nicknameのsetter, getter
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    public int getParty_id() {
        return this.party_id;
    }

    //bushoのsetter, getter
    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return this.count;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the payment
     */
    public int getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(int payment) {
        this.payment = payment;
    }

    /**
     * @return the paylist
     */
    public List getPaylist() {
        return paylist;
    }

    /**
     * @param paylist the paylist to set
     */
    public void setPaylist(List paylist) {
        this.paylist = paylist;
    }

   
}
