package maven.springboot.FriendsSansyo.service.dto;

import java.util.ArrayList;
import java.util.List;

public class FriendsSansyoWebDto {

     
    private int user_id;
    private List<FriendsSansyoOutDto> friends_list;
    private int eflag;
    
    /**
     * @return the eflag
     */
    public int getEflag() {
        return eflag;
    }

    /**
     * @param eflag the eflag to set
     */
    public void setEflag(int eflag) {
        this.eflag = eflag;
    }
    
    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the friends_list
     */
    public List<FriendsSansyoOutDto> getFriends_list() {
        return friends_list;
    }

    /**
     * @param friends_list the friends_list to set
     */
    public void setFriends_list(List<FriendsSansyoOutDto> friends_list) {
        this.friends_list = friends_list;
    }
 
}