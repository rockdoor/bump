package maven.springboot.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "t_users_likes")
@IdClass(T_Users_LikesIdEntity.class)
public class T_Users_LikesEntity {

    //t_usersテーブルのusers_idと紐付けたい！！
    @Id
    private Integer user_id;

    //m_likesテーブルのartifact_idと紐付けたい！！
    @Id
    private Integer artifact_id;

    /**
     * @return the user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the artifact_id
     */
    public Integer getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(Integer artifact_id) {
        this.artifact_id = artifact_id;
    }

}
