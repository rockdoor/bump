package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.M_LikesEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface M_LikesRepository extends JpaRepository<M_LikesEntity, Integer> {

    @Query(value = "select m from M_LikesEntity m")
    @Override
    public List<M_LikesEntity> findAll();

    @Query(value = "select m from M_LikesEntity m where m.like_name = :like_name")
    public List<M_LikesEntity> findByLike_name(@Param("like_name") String add_like_name);

    @Query(value = "select m from M_LikesEntity m where m.like_first_cd = :like_first_cd AND m.like_second_cd = :like_second_cd")
    public List<M_LikesEntity> findByCd(@Param("like_first_cd")int add_like_first_cd,
                                         @Param("like_second_cd")int add_like_second_cd);
    
    @Query(value = "select m from M_LikesEntity m where m.artifact_id = :artifact_id")
    public M_LikesEntity findByArtifact_id(@Param("artifact_id")int artifact_id);
    
    @Query(value = "select m from M_LikesEntity m order by m.like_first_cd, m.like_second_cd, m.like_no, m.artifact_id")
    public List<M_LikesEntity> findAllOrder();
}
