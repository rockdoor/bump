package maven.springboot.Entity;

import java.io.Serializable;


/**
 *
 * @author t4-yoshioka
 */
public class T_Parties_UsersIdEntity implements Serializable{
    
    private Integer party_id;
    
    private Integer user_id;
    
    
    //プライマリキークラスは，publicで引数のないコンストラクタを持たなければいけない。
//    public PresenceAbsenceAnswerEntityId(){
//    }
    
    //主キーの一意性を保証するためにequalsとhashCodeを必ず定義しなければならない
    @Override
    public int hashCode(){

        final int prime = 31;
            int result = 1;
            result = prime * result + ((party_id == null) ? 0 : party_id.hashCode());
            result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
            return result;
         
    }
    
    @Override
    public boolean equals(Object obj){
        //何か書く
        //主キーにNullは入らないかな
//        if (this == obj)
//            return true;
//        if (obj == null)
//            return false;
//        if (getClass() != obj.getClass())
//            return false;
//        PresenceAbsenceAnswerEntityId other = (PresenceAbsenceAnswerEntityId) obj;
//        if (party_id == null) {
//            if (other.party_id != null)
//                return false;
//        } else if (!party_id.equals(other.party_id))
//            return false;
//        if (user_id == null) {
//            if (other.user_id != null)
//                return false;
//        } else if (!user_id.equals(other.user_id))
//            return false;
//        return true;

            T_Parties_UsersIdEntity other = (T_Parties_UsersIdEntity) obj;

            if  (party_id == other.party_id && user_id == other.user_id ){
                return true;
            }
            else{
                return false;
            }

    }
    
}
