package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "t_tyo_parties")
public class T_Tyo_PartiesEntity {
    
    //パーティID（主キー）
    @Id
    private Integer party_id;

    //登録締め切り時間
    @Column(name = "deadline")
    private int deadline;

    /**
     * @return the party_id
     */
    public Integer getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(Integer party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the deadline
     */
    public int getDeadline() {
        return deadline;
    }

    /**
     * @param deadline the deadline to set
     */
    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }
    
    
    
}
