package maven.springboot.Party_SyukketsuKaitou.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Party_SyukketsuKaitou.service.dto.Party_SyukketsuKaitouInDto;
import maven.springboot.Party_SyukketsuKaitou.service.dto.Party_SyukketsuKaitouWebOutDto;
import maven.springboot.Repository.T_Parties_UsersRepository;

@Service  // DIする対象の目印
public class Party_SyukketsuKaitouService {

    @Autowired
    private T_Parties_UsersRepository t_parties_usersRepository;

    public Party_SyukketsuKaitouWebOutDto syukketsukosin(Party_SyukketsuKaitouInDto party_SyukketsuKaitouindto) {

        Party_SyukketsuKaitouWebOutDto party_SyukketsuKaitouWebOutDto = new Party_SyukketsuKaitouWebOutDto();

        int user_id = party_SyukketsuKaitouindto.getUser_id();
        int party_id = party_SyukketsuKaitouindto.getParty_id();

        //出欠区分が出席のものだけ持ってくる（リポジトリで実装）
        List<T_Parties_UsersEntity> t_Parties_UsersEntity_List = t_parties_usersRepository.findByUser_idAndParty_id(user_id, party_id);
        int count = t_Parties_UsersEntity_List.size();

        if (count == 0) {
            System.out.println("★★★★★更新対象はありません★★★★");
            party_SyukketsuKaitouWebOutDto.setFlag(false);
        } else {
            party_SyukketsuKaitouWebOutDto.setFlag(true);

            T_Parties_UsersEntity t_Parties_UsersEntity = t_Parties_UsersEntity_List.get(0);
            t_Parties_UsersEntity.setAttend_cd(party_SyukketsuKaitouindto.getAttend_cd());
            
            //更新処理
            t_parties_usersRepository.save(t_Parties_UsersEntity_List);
        }
        return party_SyukketsuKaitouWebOutDto;
    }
}
