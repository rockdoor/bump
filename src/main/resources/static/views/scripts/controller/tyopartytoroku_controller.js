/* global $routeScope */

angular.module('MyApp')
        .controller('Tyo_partyTorokuController', ['$scope', '$http', '$location', '$rootScope', '$uibModal',
            function ($scope, $http, $location, $rootScope, $uibModal) {

                //開始時刻
                $scope.starttime_groups = [
                    {"starttime": "17:30", "number": "1730"}, {"starttime": "17:45", "number": "1745"}, {"starttime": "18:00", "number": "1800"}, {"starttime": "18:15", "number": "1815"},
                    {"starttime": "18:30", "number": "1830"}, {"starttime": "18:45", "number": "1845"}, {"starttime": "19:00", "number": "1900"}, {"starttime": "19:15", "number": "1915"},
                    {"starttime": "19:30", "number": "1930"}, {"starttime": "19:45", "number": "1945"}, {"starttime": "20:00", "number": "2000"}, {"starttime": "20:15", "number": "2015"},
                    {"starttime": "20:30", "number": "2030"}, {"starttime": "20:45", "number": "2045"}, {"starttime": "21:00", "number": "2100"}, {"starttime": "21:15", "number": "2115"},
                    {"starttime": "21:30", "number": "2130"}, {"starttime": "21:45", "number": "2145"}, {"starttime": "22:00", "number": "2200"}, {"starttime": "22:15", "number": "2215"},
                    {"starttime": "22:30", "number": "2230"}, {"starttime": "22:45", "number": "2245"}, {"starttime": "23:00", "number": "2300"}, {"starttime": "23:15", "number": "2315"},
                    {"starttime": "23:30", "number": "2330"}, {"starttime": "23:45", "number": "2345"}, {"starttime": "24:00", "number": "2400"}
                ];

                //締め切り時刻
                $scope.deadline_groups = [
                    {"deadline": "12:00", "number": "1200"}, {"deadline": "13:00", "number": "1300"},
                    {"deadline": "14:00", "number": "1400"}, {"deadline": "15:00", "number": "1500"}, {"deadline": "16:00", "number": "1600"}, {"deadline": "17:00", "number": "1700"},
                    {"deadline": "18:00", "number": "1800"}, {"deadline": "19:00", "number": "1900"}, {"deadline": "20:00", "number": "2000"}, {"deadline": "21:00", "number": "2100"},
                    {"deadline": "22:00", "number": "2200"}, {"deadline": "23:00", "number": "2300"}, {"deadline": "24:00", "number": "2400"}
                ];

                console.log('0番目(deadline)：' + $scope.deadline_groups[0].deadline);
                console.log('0番目(number)：' + $scope.deadline_groups[0].number);
                console.log('長さ（deadline_groups）' + $scope.deadline_groups.length);

                //登録締め切り時刻を開始時刻までとして表示させる
                var cnt = $scope.deadline_groups.length;
                for (var i = 0; i < cnt; i++) {
                    //console.log($scope.deadline_groups[i].number);
                    if ($scope.starttime_groups.number === $scope.deadline_groups[i].number) {
                        break;
                    }
                }


                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
//                    console.log(status);
//                    console.log(data.count);

                    // 現在時刻を生成･取得
                    DD = new Date();
                    $scope.Hours = DD.getHours();
                    $scope.Minutes = DD.getMinutes();
                    //$scope.Seconds = DD.getSeconds();
                    console.log('現在の時刻：', $scope.Hours, '時', $scope.Minutes, '分');

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }


                }).error(function (data, status, headers, config) {
                    console.log(status);
                });


                //createボタンを押したときの処理
                $scope.doTyo_partyToroku = function () {

//                    console.log('starttime:' + $scope.starttime.starttime);
//                    console.dir($scope.starttime);
//                    console.log('starttime:' + $scope.starttime_groups[0].number);


                    $rootScope.modalInstance = $uibModal.open({
                        templateUrl: '/views/Tyosyosaipop.html',
                        controller: 'tyosyosaipopController',
                        backdrop: true,
                        scope: $scope,
                        resolve: {
                            params: function () {
                                return {
                                    tyoname: $scope.party_name,
                                    tyostarttime: $scope.starttime,
                                    tyodeadline: $scope.deadline
                                };
                            }
                        }
                    });

                    console.log($scope.party_name);
                    console.log($scope.starttime);
                    console.log($scope.deadline);



                            
                            
                };
                
              

//                   登録締め切り時刻（開始時刻より早い時間のみ）を表示する処理
                $scope.deadline_time_change = function () {
                    console.log('開始時刻：' + $scope.starttime.number);
                    $scope.deadlineList = [];
                    for (var i = 0; i < $scope.deadline_groups.length; i++) {

                        //「開始時刻 < 登録締め切り時刻」となったらBreakさせる
                        if ($scope.starttime.number < $scope.deadline_groups[i].number) {
                            break;
                        }

                        //console.log($scope.deadline_groups[i]);
                        $scope.deadlineList.push($scope.deadline_groups[i]);

                    }
                    //console.dir($scope.deadlineList);
                };
            }]);




