/* global $routeScope */

angular.module('MyApp')
        //吉岡追加部分①（開始）
        .controller('tyo_partyKaitouController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {

                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/tyo_PartyKaitou'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data);
                    console.log($scope.results.list[0].party_name);
                    //dataの長さを格納したい （JSONの長さ）
                    var l = Object.keys(data.list).length;
                    console.log("JSONの長さ（data.listが引数）;" + l);

                    //もらったデータをもとにselectedの位置を決定する
//                    for (k = 0; k < l; k++) {
//                        var options = document.getElementById('attend_' + k).options;
//                        if ($scope.results.list[k].attend_cd === 0) {
//                            options[0].selected = true;
//                        } else if ($scope.results.list[k].attend_cd === 1) {
//                            options[1].selected = true;
//
//                        } else {
//                            options[2].selected = true;
//                        }
//                    }

                }
                ).error(function (data, status, headers, config) {
                    console.log(status);
                });



                $scope.doTyo_partyKaitou = function () {

                    var attend_cd = [];
                    var l = Object.keys($scope.results.list).length;
                    var id;

                    // 未回答の部分のみ表示、Updateするためfor文の中に条件文を指定
                    for (k = 0; k < l; k++) {
                        if ($scope.results.list[k].attend_cd === 0) {
                            id = document.getElementById('attend_' + k);
                            console.log(id.selectedIndex);
                            attend_cd.push(id.selectedIndex);
                            $scope.results.list[k].attend_cd = id.selectedIndex - 1;
                        }
                    }
                    console.log(attend_cd);
                    console.log($scope.results);

                    $http({
                        method: 'POST',
                        data: $scope.results,
                        url: '/nomikaiupdate'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        console.log($scope.results.list[0].party_name);
                        //dataの長さを格納したい （JSONの長さ）
                        var l = Object.keys(data.list).length;
                        console.log("JSONの長さ（data.listが引数）;" + l);

                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });



                };
            }
        ]);





