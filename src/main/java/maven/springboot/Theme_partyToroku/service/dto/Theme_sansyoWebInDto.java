package maven.springboot.Theme_partyToroku.service.dto;

public class Theme_sansyoWebInDto {

    private int user_id; //個人ID

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

}
