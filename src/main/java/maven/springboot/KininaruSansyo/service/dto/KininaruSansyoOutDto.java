package maven.springboot.KininaruSansyo.service.dto;

public class KininaruSansyoOutDto {
    
    private String like_name;
    private int artifact_id;
    /**
     * @return the artifact_id
     */
    public int getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(int artifact_id) {
        this.artifact_id = artifact_id;
    }

    /**
     * @return the like_name
     */   
    public String getLike_name() {
        return like_name;
    }

    /**
     * @param like_name the like_name to set
     */
    public void setLike_name(String like_name) {
        this.like_name = like_name;
    }
    
    
}
