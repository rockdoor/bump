/* global $routeScope */

angular.module('MyApp')
        .controller('SyukkestupopController', ['$scope', '$rootScope', '$http',
            function ($scope, $rootScope, $http) {

                console.log("おはざえる");
                console.log($rootScope.user_id);
                console.log($rootScope.party_id);
                console.log($rootScope.marubatsu);
                $scope.domarubatsukosin = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: Number($rootScope.user_id),
                            party_id: Number($rootScope.party_id),
                            attend_cd: Number($rootScope.marubatsu)},
                        url: '/attend_update'
                    }).success(function (data, status, headers, config) {
                        console.log(status);
                        console.log(data.flag);
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    //更新フラグ
                    $rootScope.marubatsuflag = true;
                    
                    //閉じたい
                    $rootScope.modalInstance.close();
                };
                $scope.domarubatsunokosin = function () {
                    //更新フラグ
                    $rootScope.marubatsuflag = false;

                    //閉じたい
                    $rootScope.modalInstance.close();
                };
            }]);




