package maven.springboot.KininaruSansyo.service.dto;

import java.util.List;


public class KininaruSansyoWebOutDto {

    private List<KininaruSansyoOutDto> KininaruSansyoWebOutDto;

    /**
     * @return the KininaruSansyoWebOutDto
     */
    public List<KininaruSansyoOutDto> getKininaruSansyoWebOutDto() {
        return KininaruSansyoWebOutDto;
    }

    /**
     * @param KininaruSansyoWebOutDto the KininaruSansyoWebOutDto to set
     */
    public void setKininaruSansyoWebOutDto(List<KininaruSansyoOutDto> KininaruSansyoWebOutDto) {
        this.KininaruSansyoWebOutDto = KininaruSansyoWebOutDto;
    }

    
}
