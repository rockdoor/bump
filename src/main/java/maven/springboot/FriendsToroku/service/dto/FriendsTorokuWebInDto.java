package maven.springboot.FriendsToroku.service.dto;

import java.util.HashMap;

public class FriendsTorokuWebInDto {

    //OutDto内には、処理結果（true,false）のみ
       private HashMap<String, String> friend_hash;
       private int user_id;
    
    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the friend_hash
     */
    public HashMap<String, String> getFriend_hash() {
        return friend_hash;
    }

    /**
     * @param friend_hash the friend_hash to set
     */
    public void setFriend_hash(HashMap<String, String> friend_hash) {
        this.friend_hash = friend_hash;
    }
  
}