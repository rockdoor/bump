package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.T_PasswordsEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_PasswordsRepository extends JpaRepository<T_PasswordsEntity, Integer> {

    @Query(value = "select t from T_PasswordsEntity t where t.email = :email")
    public List<T_PasswordsEntity> findByEmail(@Param("email") String email);
    
    @Query(value = "select t from T_PasswordsEntity t where t.url_id = :url_id")
    public List<T_PasswordsEntity> findByUrl_id(@Param("url_id") int url_id);

}
