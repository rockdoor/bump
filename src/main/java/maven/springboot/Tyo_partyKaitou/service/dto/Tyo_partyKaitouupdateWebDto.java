/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Tyo_partyKaitou.service.dto;

import java.util.List;
import maven.springboot.Entity.T_Parties_UsersEntity;

public class Tyo_partyKaitouupdateWebDto {

    //LIST型変数を追加します
    private List<T_Parties_UsersEntity> list;

    /**
     * @return the list
     */
    public List<T_Parties_UsersEntity> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<T_Parties_UsersEntity> list) {
        this.list = list;
    }
    
}
