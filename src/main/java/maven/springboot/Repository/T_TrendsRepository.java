package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.T_TrendsEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_TrendsRepository extends JpaRepository<T_TrendsEntity, Integer> {

    @Query(value = "select t from T_TrendsEntity t where t.like_date between :currentDate and :lastweek group by t.artifact_id order by count(t.artifact_id) desc")
    public List<Object> getTrend(@Param("currentDate") Date currentDate, @Param("lastweek") Date lastweek);

}
