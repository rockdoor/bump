/* global $routeScope */

angular.module('MyApp')
        .controller('tyosyosaipopController', ['$scope', '$rootScope', '$location', '$http', 'params',
            function ($scope, $rootScope, $location, $http, params) {

                console.log(params);

                $scope.tyoname = params.tyoname;
                $scope.tyostarttime = params.tyostarttime;
                $scope.tyodeadline = params.tyodeadline;


                $scope.tyonomikaisai = function () {
                    
                    $http({
                        method: 'POST',
                        data: {
                            party_name: $scope.party_name,
                            user_id: $rootScope.user_id,
                            starttime: +$scope.starttime.number,
                            deadline: +$scope.deadline.number,
                        },
                        url: '/tyo_Partytoroku'
                    }).success(function (data, status, heraders, config) {
                        $scope.results = data.data;
                        console.log(status);
                        console.log(data);
                        console.log(data.party_id);
                        //開始時刻と締め切り時刻が送れているか確認
                        console.log('$scope.starttime.number;' + $scope.starttime.number);
                        console.log('$scope.deadline.number;' + $scope.deadline.number);


                        if (data.party_id == "0") {
                            alert("飲み会を登録できません");
                            console.log('登録できません');
                            $location.path('/tyo_partytoroku');
                        } else {
                            console.log('登録完了');
                            alert("飲み会を登録しました。");
                            $location.path('/mypage');
                        }
                    }).error(function (data, status, heraders, config) {
                        console.log(status);
                        console.log(data);
                    });
                };


                $scope.tyonomiyametoku = function () {

                    //閉じたい
                    $rootScope.themenomitorokuflag = false;
                    $rootScope.modalInstance.close();
                };

            }]);
