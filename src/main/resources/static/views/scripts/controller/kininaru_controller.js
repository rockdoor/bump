/* global $routeScope */

angular.module('MyApp')
        .controller('KininaruSettingController', ['$scope', '$http', '$location', '$rootScope', '$route',
            function ($scope, $http, $location, $rootScope, $route) {
                //M_Likesの情報をそのまま持ってくる処理。ページの初期化
                $http({
                    url: '/kininarusetting'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log($scope.results);
                    console.log(status);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                //後の処理で使う変数の初期化
                $scope.accord1 = 0;
                $scope.accord2 = 0;
                $scope.value_no = [];
                $scope.value_name = [];
                $scope.css_cd = 0;

                //resuletの値(つまり大区分なのか中区分なのか小区分なのか)を判定して、それぞれにCSS(を判定する変数)を渡す。
                //画面に出力するかどうかはHTMLのng-showで解決するのため、JS側はCSSの準備のみ行う。
                $scope.doFilter = function (result) {
                    $scope.accord1 = result.like_first_cd;
                    $scope.accord2 = result.like_second_cd;
//                    $scope.accord1 = $scope.results.kininaruSettingWebOutDto[index].like_first_cd;
//                    $scope.accord2 = $scope.results.kininaruSettingWebOutDto[index].like_second_cd;
                    //function内で定義することで、HTMLのng-classに変数という形でCSSを渡す。
                    $scope.cssChange = function (result) {
                        if (result.like_second_cd == 0 && result.like_first_cd == $scope.accord1) {
                            return "accord3";//白
                        } else if (result.like_second_cd == 0) {
                            return "accord0";//白
                        } else if (result.like_no == 0 && result.like_first_cd == $scope.accord1) {
                            return "accord1";//薄オレンジ
                        } else {
                            return "accord2";//濃いオレンジ
                        }
                    };
                };

                //右にスワイプした際の処理。
                //スワイプされたものを判定して、格納先(value_no,value_name)にartifact_id,like_nameを格納(大区分は不可にしてある)
                //色付けをする際に、単純にresultの値で行うと、同div内でresultを複数使うと中身がリフレッシュされてしまい
                //⇒ng-repeat全体色づけの設定がいきわたってしまうため、idを采番してElementByIdを用いてng-repeatで生成される各div単位でClassを追加している。
                //そのため、functionの引数はあえてindexを用いている。
                $scope.doSwipeRight = function (index) {
                    console.log(index);
                    console.log($scope.results.kininaruSettingWebOutDto[index]);
                    //重複の選択を防ぐ処理。過去に一度右スワイプしているもの(つまりdouble_checkflag = false)はしても格納先には保存されないようにしてある。
                    $scope.double_checkflag = true;
                    if ($scope.results.kininaruSettingWebOutDto[index].like_second_cd != 0) {
                        for (var i = 0; i < $scope.value_no.length; i++) {
                            if ($scope.value_no[i] == $scope.results.kininaruSettingWebOutDto[index].artifact_id) {
                                $scope.double_checkflag = false;
                            }
                        }
                        if ($scope.double_checkflag == true) {
                            $scope.value_no.push($scope.results.kininaruSettingWebOutDto[index].artifact_id);
                            $scope.value_name.push($scope.results.kininaruSettingWebOutDto[index].like_name);
                            //采番したidに応じてClassを書き換えることで色付けを行う。
                            if ($scope.results.kininaruSettingWebOutDto[index].like_no == 0) {
                                var ele = document.getElementById('likes_' + index);
                                ele.setAttribute('class', 'checked_accord1');
                            } else {
                                var ele = document.getElementById('likes_' + index);
                                ele.setAttribute('class', 'checked_accord2');
                            }
//                            var ele = document.getElementById('likes_' + index);
//                            ele.setAttribute('class', 'checked');
//                            console.log($scope.value_no);
////                            console.log($scope.value_name);
                        }
                    }
                };

                //左にスワイプした際の処理。
                //スワイプされたものを判定して、格納先(value_no)からartifact_idを削除すると同時に、色を戻す。
                //色を戻す処理は、doRightSwipeと同様の考え方(色が区分によって違うため判定の上でClassを書き換えている)
                $scope.doSwipeLeft = function (index) {
                    $scope.double_checkflag = false;
                    for (var i = 0; i < $scope.value_no.length; i++) {
                        if ($scope.value_no[i] == $scope.results.kininaruSettingWebOutDto[index].artifact_id) {
                            $scope.delete_checkflag = true;
                        }
                    }
                    if ($scope.delete_checkflag == true) {
                        $scope.value_no.some(function (v, i) {
                            if (v == $scope.results.kininaruSettingWebOutDto[index].artifact_id)
                                $scope.value_no.splice(i, 1);
                        });
                        if ($scope.results.kininaruSettingWebOutDto[index].like_second_cd == 0) {
                            var ele = document.getElementById('likes_' + index);
                            ele.setAttribute('class', 'accord0');
                        } else if ($scope.results.kininaruSettingWebOutDto[index].like_no == 0) {
                            var ele = document.getElementById('likes_' + index);
                            ele.setAttribute('class', 'accord1');
                        } else {
                            var ele = document.getElementById('likes_' + index);
                            ele.setAttribute('class', 'accord2');
                        }
                    }
                };



                //右スワイプで選んでいるものをサーバー側に送る⇒t_uers_likesにインサート
                $scope.doCheckLikes = function () {
                    $http({
                        method: 'POST',
//                        data: $scope.value_no,
                        data: {user_id: Number($rootScope.user_id), artifact_list: $scope.value_no},
                        url: '/kininarutoroku'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log($scope.results);
                        console.log(status);
                        console.log(data);
                        alert('登録完了！登録一覧に移動します！');
                        $location.path('/likescfm');
//                        if (data.kininaruTorokuFlag == false) {
//                            alert('登録完了！登録一覧に移動します！');
//                            console.log("追加登録");
//                            $location.path('/likescfm');
//                        } else {
//                            console.log('登録失敗');
//                            alert('エラー');
//                            $location.path('/likescfm');
//                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };

                //気になることを追加する処理。
                $scope.add_like_name = {like_name: ""};
                $scope.doAddLikes = function (index) {
                    $http({
                        method: 'POST',
                        data: {add_like_name: $scope.add_like_name.like_name, add_like_first_cd: $scope.results.kininaruSettingWebOutDto[index].like_first_cd, add_like_second_cd: $scope.results.kininaruSettingWebOutDto[index].like_second_cd},
                        url: '/kininarutag'
                    }).success(function (data, status, headers, config) {
                        console.log(status);
                        console.log(data);
                        alert('タグの新規作成を完了しました！');
//                        $location.path("/likes");
                        $route.reload();
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    $http({
                        method: 'POST',
                        url: '/trendsetting'
                    }).success(function (data, status, headers, config) {
                        $scope.trendlist = data;
                        console.log(status);
                        console.log(data);
                        alert('気になるトレンドの取得に失敗しました');
//                        $location.path("/likes");
                        $route.reload();
                    }).error(function (data, status, headers, config) {
                        alert('気になるトレンドの取得に失敗しました');
                        console.log(status);
                    });
                };
            }])

        //参照用コントローラー
        .controller('KininaruSansyoController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/kininarusansyo'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
            }])

        .controller('KininaruDeleteController', ['$scope', '$http', '$location', '$rootScope', '$route',
            function ($scope, $http, $location, $rootScope) {
                $scope.delete_data = {delete: {}}; //追記2016/12/19

                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id
                    },
                    url: '/kininarusansyo'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
                $scope.dokininarudelete = function () {
                    $http({
                        method: 'POST',
                        data: {delete_map: $scope.delete_data.delete,
                            user_id: $rootScope.user_id
                        },
                        url: '/kininaruDelete'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.dflag == 0) {
                            alert('削除完了');
                        } else {
                            alert('気になる登録していません');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    $location.path('/likescfm');
                };
            }]);
