package maven.springboot.KininaruSansyo.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.KininaruSansyo.service.KininaruSansyoService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoInDto;
import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoWebInDto;
import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoWebOutDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KininaruSansyoController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruSansyoService kininarusansyoservice;
//登録ボタン押下後の処理⇒個人用のt_users_likesにuser_idと紐付けてインサートする。

    @RequestMapping(value = "/kininarusansyo", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruSansyoWebOutDto requestKininaruSansyo(@RequestBody KininaruSansyoWebInDto kswid) {
        int requser_id = kswid.getUser_id();

        KininaruSansyoInDto ksid = new KininaruSansyoInDto();
        ksid.setUser_id(requser_id);

        KininaruSansyoWebOutDto kswod = kininarusansyoservice.kininaruSansyo(ksid);

        //try {
          //  //確認
            //System.out.println(new ObjectMapper().writeValueAsString(kswod));
        //} catch (JsonProcessingException ex) {
          //  Logger.getLogger(KininaruSansyoController.class.getName()).log(Level.SEVERE, null, ex);
        //}
        return kswod;
    }
};