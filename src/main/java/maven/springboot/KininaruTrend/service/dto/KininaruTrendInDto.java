package maven.springboot.KininaruTrend.service.dto;

import java.util.Date;

public class KininaruTrendInDto {

    private Date currentDate;
    private Date lastweek;

    /**
     * @return the currentDate
     */
    public Date getCurrentDate() {
        return currentDate;
    }

    /**
     * @param currentDate the currentDate to set
     */
    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    /**
     * @return the lastweek
     */
    public Date getLastweek() {
        return lastweek;
    }

    /**
     * @param lastweek the lastweek to set
     */
    public void setLastweek(Date lastweek) {
        this.lastweek = lastweek;
    }
    

    
}
