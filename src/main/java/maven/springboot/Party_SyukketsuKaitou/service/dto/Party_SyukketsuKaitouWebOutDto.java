package maven.springboot.Party_SyukketsuKaitou.service.dto;

public class Party_SyukketsuKaitouWebOutDto {

    private boolean flag;

    /**
     * @return the flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
}
