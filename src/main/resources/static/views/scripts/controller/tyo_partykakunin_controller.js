/* global $routeScope */

angular.module('MyApp')
        .controller('Tyo_partyKakuninController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {
                //ヘッダーの通知件数を表示する為の固定処理
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data.count);

                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                var date = new Date();
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                var day = date.getDate();
                if (day < 10) {
                    day = "0" + day;
                }
                var today = "" + year + month + day;
                $rootScope.today = today;
                console.log(today);
                console.log($rootScope.today);

                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/tyo_partykakunin'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log($scope.results);
                    console.log(status);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
            }]);
