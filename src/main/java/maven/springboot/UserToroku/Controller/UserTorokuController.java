package maven.springboot.UserToroku.Controller;

import maven.springboot.UserToroku.service.UserTorokuService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.UserToroku.service.dto.UserTorokuInDto;
import maven.springboot.UserToroku.service.dto.UserTorokuOutDto;
import maven.springboot.UserToroku.service.dto.UserTorokuWebDto;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class UserTorokuController {

    @RequestMapping("/")
    public String main() {
        return "/views/Main.html";
    }

    /*★★★★AddressHanteiLogic.javaに渡して戻り値（OK、NG）に応じたJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private UserTorokuService usertorokuservice;

    @RequestMapping(value = "/userToroku", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public UserTorokuOutDto requestAddress(@RequestBody UserTorokuWebDto ut_webdto) {
        //RequestBodyで(mailの入った)JsonをWebDtoの形で受け取り、以下reqaddressに出力。
        String reqaddress = ut_webdto.getEmail();
        //InDtoに上記でうけとったメアドを入れる
        UserTorokuInDto usertorokuindto = new UserTorokuInDto();
        usertorokuindto.setEmail(reqaddress);

        //UserTorokuServiceにDtoを渡し、結果をもらう;
        UserTorokuOutDto ut_outdto = usertorokuservice.entryCfm(usertorokuindto);
        //確認
        System.out.println(ut_outdto.getFlag());

        return ut_outdto;
    }
    
    @RequestMapping(value = "/registry", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public UserTorokuOutDto requestResistry(@RequestBody UserTorokuWebDto ut_webdto) {
        //RequestBodyで(mailの入った)JsonをWebDtoの形で受け取り、以下reqaddressに出力。
        int requrl_id = ut_webdto.getUrl_id();
        String reqpassword = ut_webdto.getPassword();
        UserTorokuInDto usertorokuindto = new UserTorokuInDto();
        usertorokuindto.setUrl_id(requrl_id);
        usertorokuindto.setPassword(reqpassword);

        //UserTorokuServiceにDtoを渡し、結果をもらう;
        UserTorokuOutDto ut_outdto = usertorokuservice.registry(usertorokuindto);

        return ut_outdto;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public UserTorokuOutDto requestLogin(@RequestBody UserTorokuWebDto utwd) throws Exception {
        //RequestBodyで(mailの入った)JsonをWebDtoの形で受け取り、以下reqaddressに出力。
        String reqaddress = utwd.getEmail();
        String reqpassword = utwd.getPassword();
        //InDtoに上記でうけとったメアドを入れる
        UserTorokuInDto usertorokuindto = new UserTorokuInDto();
        usertorokuindto.setEmail(reqaddress);
        usertorokuindto.setPassword(reqpassword);

        System.out.println(reqaddress);

        //UserTorokuServiceにDtoを渡し、結果をもらう;
        UserTorokuOutDto utod = usertorokuservice.login(usertorokuindto);
        //確認
        System.out.println(utod.getFlag());
        return utod;
    }
    
    @RequestMapping(value = "/emailforentry", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public UserTorokuOutDto requestGetMail(@RequestBody UserTorokuWebDto ut_webdto) {
        //RequestBodyで(mailの入った)JsonをWebDtoの形で受け取り、以下reqaddressに出力。
        int requrl_id = ut_webdto.getUrl_id();
        UserTorokuInDto usertorokuindto = new UserTorokuInDto();
        usertorokuindto.setUrl_id(requrl_id);

        //UserTorokuServiceにDtoを渡し、結果をもらう;
        UserTorokuOutDto ut_outdto = usertorokuservice.getMail(usertorokuindto);

        return ut_outdto;
    }
    
    
    
}