/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Repository;

import maven.springboot.Entity.T_Tyo_PartiesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author t4-yoshioka
 */
@Repository  // DIする対象の目印
public interface T_Tyo_PartiesRepository extends JpaRepository<T_Tyo_PartiesEntity, Integer>{
    
//    @Query(value = "selest t from T_Tyo_PartiesEntity t where t.party_id = :party_id")
//    public List<T_Tyo_PartiesEntity> findByParty_id(@Param("party_id") int party_id);
    
}
