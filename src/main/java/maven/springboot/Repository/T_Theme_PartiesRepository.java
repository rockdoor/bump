/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Repository;

import maven.springboot.Entity.T_Theme_PartiesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author y-katakami
 */
public interface T_Theme_PartiesRepository extends JpaRepository<T_Theme_PartiesEntity, Integer> {    
 
     @Query(value = "select t from T_PartiesEntity t where t.party_id = :party_id")
     public List<T_Theme_PartiesEntity> findByParty_id(@Param("party_id") int party_id);
    
}
