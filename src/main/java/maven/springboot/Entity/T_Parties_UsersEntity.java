package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;
import java.io.Serializable;

@Entity
@Table(name = "t_parties_users")
@IdClass(T_Parties_UsersIdEntity.class)
public class T_Parties_UsersEntity {

    //t_partiesテーブルのparty_idと紐付けたい！！
    @Id
    private Integer party_id;

    //t_usersテーブルのuser_idと紐付けたい！！
    @Id
    private Integer user_id;
    
    //user_idに対応する名前
    @Column(name = "name")
    private String name;

    //飲み会名（party_name）の列を定義
    @Column(name = "party_name")
    private String party_name;
    
    //出席区分（attend_cd）の列を定義
    @Column(name = "attend_cd")
    private int attend_cd;
    
    //支払い金額（payment）の列を定義
    @Column(name = "payment")
    private int payment;
    
    //支払い区分（payment_cd）の列を定義
    @Column(name = "payment_cd")
    private int payment_cd;

    //支払い比重(規定値は5)
    @Column(name = "payment_rate")
    private int payment_rate;
    
    //既読フラグ
    @Column(name = "showed_flag")
    private boolean showed_flag;

   
    /**
     * @return the party_id
     */
    public Integer getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(Integer party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the party_name
     */
    public String getParty_name() {
        return party_name;
    }

    /**
     * @param party_name the party_name to set
     */
    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    /**
     * @return the attend_cd
     */
    public int getAttend_cd() {
        return attend_cd;
    }

    /**
     * @param attend_cd the attend_cd to set
     */
    public void setAttend_cd(int attend_cd) {
        this.attend_cd = attend_cd;
    }

    /**
     * @return the payment
     */
    public int getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(int payment) {
        this.payment = payment;
    }

    /**
     * @return the payment_cd
     */
    public int getPayment_cd() {
        return payment_cd;
    }

    /**
     * @param payment_cd the payment_cd to set
     */
    public void setPayment_cd(int payment_cd) {
        this.payment_cd = payment_cd;
    }

    /**
     * @return the payment_rate
     */
    public int getPayment_rate() {
        return payment_rate;
    }

    /**
     * @param payment_rate the payment_rate to set
     */
    public void setPayment_rate(int payment_rate) {
        this.payment_rate = payment_rate;
    }

    /**
     * @return the showed_flag
     */
    public boolean isShowed_flag() {
        return showed_flag;
    }

    /**
     * @param showed_flag the showed_flag to set
     */
    public void setShowed_flag(boolean showed_flag) {
        this.showed_flag = showed_flag;
    }

    
    
}
