package maven.springboot.Tyo_partyKakunin.service.dto;

public class Tyo_parties_users_Dto {

    private String name;
    private int attend_cd;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the attend_cd
     */
    public int getAttend_cd() {
        return attend_cd;
    }

    /**
     * @param attend_cd the attend_cd to set
     */
    public void setAttend_cd(int attend_cd) {
        this.attend_cd = attend_cd;
    }

}
