package maven.springboot.Kidokukinou.service.dto;

public class KidokukinouOutDto {

    //OutDto内には、数値のみ
    private boolean flag;

    /**
     * @return the flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

}
