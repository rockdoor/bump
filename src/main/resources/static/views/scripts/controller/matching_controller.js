/* global $routeScope */

angular.module('MyApp')
        .controller('MatchingController', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$uibModal',
            function ($scope, $http, $location, $rootScope, $routeParams, $uibModal) {

                //飲み会予定表示用関数---------------------------------------------------------
                $http({
                    method: 'POST',
                    data: {user_id: Number($rootScope.user_id)},
                    url: '/matching'
                }).success(function (data, status, headers, config) {
                    //二回押したら選択消すようフラグ
                    $scope.interact_f_flag = 0;
                    $scope.interact_d_flag = 0;

                    $scope.ddd = data;

                    //テーマ飲み
                    //マッチング可否ステータスを見て色つける
                    console.log(data);
                    if (data.t_ok_ng === 1) {
                        var ele = document.getElementById('batsu');
                        ele.setAttribute('class', 'selectedstatus');
                    } else if (data.t_ok_ng === 0) {
                        var ele = document.getElementById('maru');
                        ele.setAttribute('class', 'selectedstatus');
                    } else {
                        console.log("***テーマ飲みマッチング可否登録おかしいよー***");
                    }

                    //マッチング範囲を見て色つける
                    if (data.t_interact === 0) {
                        var ele = document.getElementById('all');
                        ele.setAttribute('class', 'selectedstatus');
                    } else if (data.t_interact === 1) {
                        var ele = document.getElementById('friend');
                        ele.setAttribute('class', 'selectedstatus');
                        $scope.interact_f_flag = 1;
                    } else if (data.t_interact === 2) {
                        var ele = document.getElementById('department');
                        ele.setAttribute('class', 'selectedstatus');
                        $scope.interact_d_flag = 1;
                    } else if (data.t_interact === 3) {
                        var ele = document.getElementById('friend');
                        ele.setAttribute('class', 'selectedstatus');
                        var ele = document.getElementById('department');
                        ele.setAttribute('class', 'selectedstatus');
                        $scope.interact_d_flag = 1;
                        $scope.interact_f_flag = 1;
                    } else {
                        console.log("***テーマ飲みマッチング範囲登録おかしいよー***");
                    }
                    
                    //飲み会エリアを表示
                    for (var i = 0; i < $scope.repoAreaArr.length; i++) {
                        if ($scope.repoAreaArr[i].value === $scope.ddd.t_area1)
                            $scope.t_area1 = $scope.repoAreaArr[i];

                    }
                    for (var i = 0; i < $scope.repoAreaArr.length; i++) {
                        if ($scope.repoAreaArr[i].value === $scope.ddd.t_area2)
                            $scope.t_area2 = $scope.repoAreaArr[i];
                    }
                    for (var i = 0; i < $scope.repoAreaArr.length; i++) {
                        if ($scope.repoAreaArr[i].value === $scope.ddd.t_area3)
                            $scope.t_area3 = $scope.repoAreaArr[i];
                    }
                    
                    //飲み会開始時刻を表示
                     for (var i = 0; i < $scope.repoNameArr2.length; i++) {
                        if ($scope.repoNameArr2[i].key === $scope.ddd.t_time)
                            $scope.t_time = $scope.repoNameArr2[i];
                    }                   
                }).error(function (data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                });

                //【テーマ飲み】マッチング可否
                $scope.domaru = function () {
                    $rootScope.t_ok_ng = 0;
                    var ele = document.getElementById('batsu');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('maru');
                    ele.setAttribute('class', 'selectedstatus');
                };
                $scope.dobatsu = function () {
                    $rootScope.t_ok_ng = 1;
                    var ele = document.getElementById('maru');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('batsu');
                    ele.setAttribute('class', 'selectedstatus');
                };

                //【テーマ飲み】マッチング範囲
                $scope.doT_interact_all = function () {
                    $rootScope.t_interact = 0;
                    var ele = document.getElementById('friend');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('department');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('all');
                    ele.setAttribute('class', 'selectedstatus');
                    $scope.interact_d_flag = 0;
                    $scope.interact_f_flag = 0;
                };
                $scope.doT_interact_friend = function () {
                    $rootScope.t_interact = 1;
                    if ($scope.interact_f_flag === 0) {
                        var ele = document.getElementById('all');
                        ele.removeAttribute("class");
                        var ele = document.getElementById('friend');
                        ele.setAttribute('class', 'selectedstatus');
                        $scope.interact_f_flag = 1;
                        if ($scope.interact_d_flag === 1)
                            $rootScope.t_interact = 3;
                    } else {
                        var ele = document.getElementById('friend');
                        ele.removeAttribute("class");
                        $scope.interact_f_flag = 0;
                        $rootScope.t_interact = 2;
                    }
                };
                $scope.doT_interact_department = function () {
                    $rootScope.t_interact = 2;
                    if ($scope.interact_d_flag === 0) {
                        var ele = document.getElementById('all');
                        ele.removeAttribute("class");
                        var ele = document.getElementById('department');
                        ele.setAttribute('class', 'selectedstatus');
                        $scope.interact_d_flag = 1;
                        if ($scope.interact_f_flag === 1)
                            $rootScope.t_interact = 3;
                    } else {
                        var ele = document.getElementById('department');
                        ele.removeAttribute("class");
                        $scope.interact_d_flag = 0;
                        $rootScope.t_interact = 1;
                    }
                };



                //開催エリア部分のプルダウンメニュー
                $scope.repoAreaArr = [
                    {value: 0, label: "希望無し"},
                    {value: 1, label: "社内開催"},
                    {value: 2, label: "木場駅周辺"},
                    {value: 3, label: "東京駅周辺"},
                    {value: 4, label: "新橋駅周辺"},
                    {value: 5, label: "品川駅周辺"},
                    {value: 6, label: "川崎駅周辺"},
                    {value: 7, label: "横浜駅周辺"}
                ];

                //開催時間
                $scope.repoNameArr2 = [
                    {key: 0, value: '希望無し '}, {key: 1730, value: '17:30以降 '}, {key: 1800, value: '18:00以降 '},
                    {key: 1830, value: '18:30以降 '}, {key: 1900, value: '19:00以降 '},
                    {key: 1930, value: '19:30以降 '}, {key: 2000, value: '20:00以降 '}
                ];
                $scope.t_time = $scope.repoNameArr2[0];

                $scope.doCheckMatching = function () {
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id, t_ok_ng: $rootScope.t_ok_ng,
                            t_interact: $rootScope.t_interact, t_time: $scope.t_time.key,
                            t_area1: $scope.t_area1.value, t_area2: $scope.t_area2.value, t_area3: $scope.t_area3.value},
                        url: '/MatchingToroku'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                        console.log(data);
                    });
                };
            }]);



