package maven.springboot.Logic;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class MailUtil {
    /**
     * メールを送信する
     */
    public static Boolean send() {
        /* 戻り値 */
        Boolean retValue = true;
        String toAddress = "k-anzai@nri.co.jp";
        String msgStr = "Hello";
        String subjectStr = "testmail";
        String fromAddress = "centos@ec2-52-199-75-141.ap-northeast-1.compute.amazonaws.com";
        String smtpHost = "ec2-52-199-75-141.ap-northeast-1.compute.amazonaws.com";
        String smtpPort = "25";
        boolean debug = true;

        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.smtp.port", smtpPort);
            if (debug) {
                props.put("mail.debug", true);
            }
            Session session = Session.getInstance(props, null);
            session.setDebug(debug);

            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(fromAddress));
            InternetAddress[] address = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subjectStr, "iso-2022-jp");

            msg.setSentDate(new Date());

            msg.setText(msgStr, "iso-2022-jp");

            Transport.send(msg);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            retValue = false;
        }
        return retValue;
    }

}
