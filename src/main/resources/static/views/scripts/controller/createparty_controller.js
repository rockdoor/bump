/* global $routeScope */

angular.module('MyApp')
        .controller('createpartyController', ['$scope', '$http', '$location', '$rootScope', '$uibModal',
            function ($scope, $http, $location, $rootScope, $uibModal) {

                //more,close表示用フラグ
                $scope.accord01 = false;
                //form用のフラグ
                $scope.hissu1 = true;
                $scope.hissu2 = true;
                $scope.hissu3 = true;
                $scope.hissu4 = true;
                //本日から3ヶ月を格納
                var datelist = [];
                //プルダウンメニュー(テーマのみ)--------------------------------------------------------------------------
                //テーマのプルダウンを表示する
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/theme_sansyo'
                }).success(function (data, status, headers, config) {
                    $scope.repoThemeNameArr = data;
                    console.log(status);
                    console.dir(data);
                    console.dir(data.m_LikesEntity_list);
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
                //今日から９０日くらい持ってくる
                var hiduke = new Date();
                // 曜日の表記
                var weekDayList = ["日", "月", "火", "水", "木", "金", "土"];
                for (var i = 0; i < 90; i++) {
                    //年・月・日・曜日を取得する
                    var year = hiduke.getFullYear();
                    var month = hiduke.getMonth() + 1;
                    var day = hiduke.getDate();
                    var weekDay = weekDayList[ hiduke.getDay() ];
                    if (month < 10 && day < 10) {
                        var todaykey = "" + year + "0" + month + "0" + day;
                        var today = "" + year + "/0" + month + "/0" + day + "(" + weekDay + ")";
                    } else if (month >= 10 && day < 10) {
                        var todaykey = "" + year + "" + month + "0" + day;
                        var today = "" + year + "/" + month + "/0" + day + "(" + weekDay + ")";
                    } else if (month < 10 && day >= 10) {
                        var todaykey = "" + year + "0" + month + "" + day;
                        var today = "" + year + "/0" + month + "/" + day + "(" + weekDay + ")";
                    } else {
                        var todaykey = "" + year + "" + month + "" + day;
                        var today = "" + year + "/" + month + "/" + day + "(" + weekDay + ")";
                    }
                    datelist.push({key: todaykey, value: today});
                    hiduke.setDate(hiduke.getDate() + 1);
                }

                $scope.repoNameArr1 = [
                    datelist[0], datelist[1], datelist[2], datelist[3], datelist[4], datelist[5], datelist[6], datelist[7], datelist[8], datelist[9],
                    datelist[10], datelist[11], datelist[12], datelist[13], datelist[14], datelist[15], datelist[16], datelist[17], datelist[18], datelist[19],
                    datelist[20], datelist[21], datelist[22], datelist[23], datelist[24], datelist[25], datelist[26], datelist[27], datelist[28], datelist[29],
                    datelist[30], datelist[31], datelist[32], datelist[33], datelist[34], datelist[35], datelist[36], datelist[37], datelist[38], datelist[39],
                    datelist[40], datelist[41], datelist[42], datelist[43], datelist[44], datelist[45], datelist[46], datelist[47], datelist[48], datelist[49],
                    datelist[50], datelist[51], datelist[52], datelist[53], datelist[54], datelist[55], datelist[56], datelist[57], datelist[58], datelist[59],
                    datelist[60], datelist[61], datelist[62], datelist[63], datelist[64], datelist[65], datelist[66], datelist[67], datelist[68], datelist[69],
                    datelist[70], datelist[71], datelist[72], datelist[73], datelist[74], datelist[75], datelist[76], datelist[77], datelist[78], datelist[79],
                    datelist[80], datelist[81], datelist[82], datelist[83], datelist[84], datelist[85], datelist[86], datelist[87], datelist[88], datelist[89]
                ];
//                $scope.selectedthemedate = null;
                $scope.repoNameArr2 = [
                    {key: 1730, value: '17:30'}, {key: 1745, value: '17:45'}, {key: 1800, value: '18:00'}, {key: 1815, value: '18:15'},
                    {key: 1830, value: '18:30'}, {key: 1845, value: '18:45'}, {key: 1900, value: '19:00'}, {key: 1915, value: '19:15'},
                    {key: 1930, value: '19:30'}, {key: 1945, value: '19:45'}, {key: 2000, value: '20:00'}, {key: 2015, value: '20:15'},
                    {key: 2030, value: '20:30'}, {key: 2045, value: '20:45'}, {key: 2100, value: '21:00'}, {key: 2115, value: '21:15'},
                    {key: 2130, value: '21:30'}, {key: 2145, value: '21:45'}, {key: 2200, value: '22:00'}, {key: 2215, value: '22:15'},
                    {key: 2230, value: '22:30'}, {key: 2245, value: '22:45'}, {key: 2300, value: '23:00'}, {key: 2315, value: '23:15'},
                    {key: 2330, value: '23:30'}, {key: 2345, value: '23:45'}, {key: 2345, value: '24:00'}
                ];
//                $scope.selectedthemetime = null;
                //詳細設定部分のプルダウンメニュー
                //開催エリア部分のプルダウンメニュー
                $scope.repoAreaArr = [
                    {value: 1, label: "社内開催"},
                    {value: 2, label: "木場駅周辺"},
                    {value: 3, label: "東京駅周辺"},
                    {value: 4, label: "新橋駅周辺"},
                    {value: 5, label: "品川駅周辺"},
                    {value: 6, label: "川崎駅周辺"},
                    {value: 7, label: "横浜駅周辺"}
                ];
//                $scope.themeArea;
                //開催ジャンルのプルダウンメニュー
                $scope.repoGenreArr = [
                    {value: 1, label: "和食"},
                    {value: 2, label: "フレンチ"},
                    {value: 3, label: "イタリアン"},
                    {value: 4, label: "バル"}
                ];
//                $scope.themeGenre;
                //開催上限人数のプルダウンメニュー
                $scope.repoKaisaimaxArr = [
                    {value: 5, label: "5人"},
                    {value: 10, label: "10人"},
                    {value: 15, label: "15人"},
                    {value: 20, label: "20人"},
                    {value: 25, label: "25人"},
                    {value: 30, label: "30人"},
                    {value: 35, label: "35人"},
                    {value: 40, label: "40人"},
                    {value: 45, label: "45人"},
                    {value: 50, label: "50人"}
                ];
//                $scope.themeKaisaimax;
                //募集上限人数のプルダウンメニュー
                $scope.repoBosyumaxArr = [
                    {value: 5, label: "5人"},
                    {value: 10, label: "10人"},
                    {value: 15, label: "15人"},
                    {value: 20, label: "20人"},
                    {value: 25, label: "25人"},
                    {value: 30, label: "30人"},
                    {value: 35, label: "35人"},
                    {value: 40, label: "40人"},
                    {value: 45, label: "45人"},
                    {value: 50, label: "50人"}
                ];
//                $scope.themeBosyumax;

                $scope.repodeadtimeArr = [
                    {key: 0900, value: '9:00'}, {key: 0930, value: '9:30'}, {key: 1000, value: '10:00'}, {key: 1030, value: '10:30'},
                    {key: 1100, value: '11:00'}, {key: 1130, value: '11:30'}, {key: 1200, value: '12:00'}, {key: 1230, value: '12:30'},
                    {key: 1300, value: '13:00'}, {key: 1330, value: '13:30'}, {key: 1400, value: '14:00'}, {key: 1430, value: '14:30'},
                    {key: 1500, value: '15:00'}, {key: 1530, value: '15:30'}, {key: 1600, value: '16:00'}, {key: 1630, value: '16:30'},
                    {key: 1700, value: '17:00'}, {key: 1730, value: '17:30'}, {key: 1800, value: '18:00'}, {key: 1830, value: '18:30'},
                    {key: 1900, value: '19:00'}, {key: 1930, value: '19:30'}, {key: 2000, value: '20:00'}, {key: 2030, value: '20:30'},
                    {key: 2100, value: '21:00'}
                ];
                //テーマのみ詳細設定----------------------------------------------------------------------------------------------
                $scope.dosyosaiadd = function () {
                    //出欠更新区分
                    $scope.accord01 = !$scope.accord01;
                    //募集締め切りのプルダウンメニュー
                    //今日から飲み会開催日までを持ってきたい
                    $scope.deaddatelist = [];
                    for (var i = 0; i < 90; i++) {
                        $scope.deaddatelist.push(datelist[i]);
                        if (datelist[i].key === $scope.selectedthemedate.key) {
                            break;
                        }
                    }

                };
                //ヘッダーの通知件数を表示する為の固定処理--------------------------------------------------------------------------
                $http({
                    method: 'POST',
                    data: {user_id: $rootScope.user_id},
                    url: '/myPageTsuchi'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
//                    console.log(status);
//                    console.log(data.count);
                    //件数毎に処理変更
                    if (data.count === 0) {
                        $rootScope.show = false;
                        $rootScope.tsuchicount = data.count;
                    } else if (data.count > 9) {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = "9+";
                    } else {
                        $rootScope.show = true;
                        $rootScope.tsuchicount = data.count;
                    }

                }).error(function (data, status, headers, config) {
//                    console.log(status);
                });
                $scope.buttonflag1 = true;
                $scope.buttonflag2 = false;
                $scope.buttonflag3 = false;
                //タブの切替---------------------------------------------------------
                $scope.do1show = function () {

                    $scope.buttonflag1 = true;
                    $scope.buttonflag2 = false;
                    $scope.buttonflag3 = false;
                    var ele = document.getElementById('button2');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button3');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button1');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button2');
                };
                //タブの切替
                $scope.do2show = function () {

                    $scope.buttonflag1 = false;
                    $scope.buttonflag2 = true;
                    $scope.buttonflag3 = false;
                    var ele = document.getElementById('button1');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button3');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button2');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button2');
                };
                //タブの切替
                $scope.do3show = function () {

                    $scope.buttonflag1 = false;
                    $scope.buttonflag2 = false;
                    $scope.buttonflag3 = true;
                    var ele = document.getElementById('button2');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button1');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button1');
                    var ele = document.getElementById('button3');
                    ele.removeAttribute("class");
                    ele.setAttribute('class', 'button2');
                };
                //createボタンを押したときの処理
                $scope.doTheme_partyToroku = function () {

//                    console.log($scope.hissu1);
//                    console.log($scope.hissu2);
//                    console.log($scope.hissu3);
                    $rootScope.modalInstance = $uibModal.open({
                        templateUrl: '/views/Themesyosaipop.html',
                        controller: 'themesyosaipopController',
                        backdrop: true,
//                        scope: $scope,
                        resolve: {
                            params: function () {
                                return {
                                    themename: $scope.party_name,
                                    theme: $scope.selectedthemename,
                                    themedate: $scope.selectedthemedate,
                                    themetime: $scope.selectedthemetime,
                                    themearea: $scope.themeArea,
                                    themeGenre: $scope.themeGenre,
                                    themeKaisaimax: $scope.themeKaisaimax,
                                    themeBosyumax: $scope.themeBosyumax,
                                    themedeaddate: $scope.themedeaddate,
                                    themedeadtime: $scope.themedeadtime
                                };
                            }
                        }
                    });
                    $rootScope.modalInstance.result.then(
                            function (result) {                                
//                                console.log(result);
                                if ($rootScope.themenomitorokuflag === true && $rootScope.syotaisum === 0) {
                                    alert("マッチング条件にあう人がいませんでした");
                                } else if ($rootScope.themenomitorokuflag === true) {
//                                    console.log("戻ってきたときのurl_id" + result);
                                    alert("登録完了しました！！飲み会ページに遷移します！");

                                    //飲み会ページに遷移させたい
                                    $location.path('/party/' + result);

                                } else if ($rootScope.themenomitorokuflag === false) {
                                    alert($rootScope.errormessage);
                                }
                            },
                            function (result) {
                            });
                };
                $scope.changetheme = function () {
                    $scope.hissu1 = false;
                    if ($scope.selectedthemename === null) {
                        $scope.hissu1 = true;
                    }
//                    console.log($scope.hissu1);
                };
                $scope.changedate = function () {
                    $scope.hissu2 = false;
                    if ($scope.selectedthemedate === null) {
                        $scope.hissu2 = true;
                    }
//                    console.log($scope.hissu2);
                };
                $scope.changetime = function () {
                    $scope.hissu3 = false;
                    if ($scope.selectedthemetime === null) {
                        $scope.hissu3 = true;
                    }
//                    console.log($scope.hissu3);
                };
                $scope.changename = function () {
                    $scope.hissu4 = false;
                    if ($scope.party_name === "") {
                        $scope.hissu4 = true;
                    }
//                    console.log($scope.hissu4);
                };
            }]);
