package maven.springboot.UserToroku.service.dto;


public class UserTorokuInDto {

	private String email;
        private String password;
        private int user_id;
        private int url_id;
    
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the url_id
     */
    public int getUrl_id() {
        return url_id;
    }

    /**
     * @param url_id the url_id to set
     */
    public void setUrl_id(int url_id) {
        this.url_id = url_id;
    }

}
											
