package maven.springboot.Logic;

import java.util.List;
import javax.annotation.Resource;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Logic.JavaMail;
import maven.springboot.Repository.T_UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ThemeMailsend {

    @Autowired
    private T_UsersRepository t_UsersRepository;

    public void Mailsend(int user_id, int owner_id, int url_id, String party_name, int date, int time, int deaddate, int deadtime) throws Exception {

        System.out.println(date);
        //開催日を○○○○年○○月○○日に
        StringBuilder hizuke = new StringBuilder();
        hizuke.append(date);
        hizuke.insert(4, "年");
        hizuke.insert(7, "月");
        hizuke.insert(10, "日");

        //開催日を○○時○○分に        
        StringBuilder jikan = new StringBuilder();
        jikan.append(time);
        jikan.insert(2, "時");
        jikan.insert(5, "分");

        //User_idをもとに部署idを取り出す為、検索をおこなう
        List<T_UsersEntity> list1 = t_UsersRepository.findByUser_id(user_id);

        //オーナーの名前もほしいっす。
        List<T_UsersEntity> list2 = t_UsersRepository.findByUser_id(owner_id);
        int count2 = list2.size();
        String ownername = "";
        if (count2 > 0) {
            ownername = list2.get(0).getName();
        } else {
            System.err.println("オーナーのUseridが登録されていません");
        }

        //ありえないが一応あるか確認
        int count = list1.size();

        //メール本文の改行
        String crlf = System.getProperty("line.separator");

        //メール送信クラスをインスタンス化しておく
        //Mailsend mailsend = new Mailsend();
        String subject = "飲み会招待通知";

        if (count > 0) {

            T_UsersEntity entity = list1.get(0);
            //メール送信に用いる値を定義
            String sendto = entity.getEmail();
            String message = entity.getName() + "さん。" + crlf + crlf
                    + "Bump!からのお知らせです。" + crlf
                    + ownername + "さんに飲み会に招待されました！！" + crlf + crlf
                    + "--------------------------------------------" + crlf
                    + "飲み会名：" + party_name + crlf
                    + "開催時間：" + hizuke + "    " + jikan + crlf
                    + "出欠締切：" + deaddate + "    " + deadtime + crlf
                    + "--------------------------------------------" + crlf + crlf
                    + "下記サイトにて出欠の回答を行ってください！！" + crlf
                    + "http://localhost:8080/#/party_top/" + url_id;

            //mail送信メソッドでメール送信(シェルなのでAWSのみ起動)
            //mailsend.send(message, sendto, subject);
            //ローカルでのテスト用
            JavaMail javamail = new JavaMail();
            //nriドメインだったら送れる設定になってる
            String from = "BUMP";

            System.out.println("送る相手：" + sendto);

            javamail.send(sendto, subject, message, from);

        } else {
            System.err.println("オーナーのUseridが登録されていません");

        }

    }
}
