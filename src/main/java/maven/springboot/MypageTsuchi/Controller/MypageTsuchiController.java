package maven.springboot.MypageTsuchi.Controller;

import maven.springboot.MypageTsuchi.service.MypageTsuchiService;
import maven.springboot.MypageTsuchi.service.dto.MypageTsuchiInDto;
import maven.springboot.MypageTsuchi.service.dto.MypageTsuchiOutDto;
import maven.springboot.MypageTsuchi.service.dto.MypageTsuchiWebDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class MypageTsuchiController {

    /*★★★★MypageTsuchiLogic.javaに渡して戻り値（飲み会通知内容）のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private MypageTsuchiService mypageTsuchiService;

    @RequestMapping(value = "/myPageTsuchi", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public MypageTsuchiOutDto requestAddress(@RequestBody MypageTsuchiWebDto mypageTsuchiWebDto) {
        //RequestBodyで(user_idの入った)JsonをWebDtoの形で受け取り、以下user_idに出力。
        int user_id = mypageTsuchiWebDto.getUser_id();
        //InDtoに上記でうけとったメアドを入れる
        MypageTsuchiInDto mypageTsuchiInDto = new MypageTsuchiInDto();
        mypageTsuchiInDto.setUser_id(user_id);

        //UserTorokuServiceにDtoを渡し、結果をもらう;
        MypageTsuchiOutDto mypageTsuchiOutDto = mypageTsuchiService.tsuchicheck(mypageTsuchiInDto);

        return mypageTsuchiOutDto;
    }

}