package maven.springboot.KininaruToroku.service.dto;

public class KininaruTagWebInDto {

    private Integer add_like_first_cd;
    private Integer add_like_second_cd;
    private String add_like_name;

    /**
     * @return the add_like_first_cd
     */
    public Integer getAdd_like_first_cd() {
        return add_like_first_cd;
    }

    /**
     * @param add_like_first_cd the add_like_first_cd to set
     */
    public void setAdd_like_first_cd(Integer add_like_first_cd) {
        this.add_like_first_cd = add_like_first_cd;
    }

    /**
     * @return the add_like_second_cd
     */
    public Integer getAdd_like_second_cd() {
        return add_like_second_cd;
    }

    /**
     * @param add_like_second_cd the add_like_second_cd to set
     */
    public void setAdd_like_second_cd(Integer add_like_second_cd) {
        this.add_like_second_cd = add_like_second_cd;
    }

    /**
     * @return the add_like_name
     */
    public String getAdd_like_name() {
        return add_like_name;
    }

    /**
     * @param add_like_name the add_like_name to set
     */
    public void setAdd_like_name(String add_like_name) {
        this.add_like_name = add_like_name;
    }
    
}