package maven.springboot.Entity;

import java.io.Serializable;


/**
 *
 * @author a-igarashi
 */
public class T_FriendsIdEntity  implements Serializable{
    
    private Integer user_id;
    
    private Integer friends_id;    
    
    //プライマリキークラスは，publicで引数のないコンストラクタを持たなければいけない。
//    public PresenceAbsenceAnswerEntityId(){
//    }
    
    //主キーの一意性を保証するためにequalsとhashCodeを必ず定義しなければならない
    @Override
    public int hashCode(){

        final int prime = 31;
            int result = 1;
            result = prime * result + ((friends_id == null) ? 0 : friends_id.hashCode());
            result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
            return result;
         
    }
    
    @Override
    public boolean equals(Object obj){

            T_FriendsIdEntity other = (T_FriendsIdEntity) obj;

            if  (friends_id == other.friends_id && user_id == other.user_id ){
                return true;
            }
            else{
                return false;
            }

    }
    
}
