package maven.springboot.JyohoSyosai.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.JyohoSyosai.service.JyohoSyosaiService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.JyohoSyosai.service.dto.JyohoSyosaiInDto;
import maven.springboot.JyohoSyosai.service.dto.JyohoSyosaiOutDto;
import maven.springboot.JyohoSyosai.service.dto.JyohoSyosaiWebDto;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class JyohoSyosaiController {

    /*★★★★mail_no(個人識別番号)に応じた各情報のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private JyohoSyosaiService jyohosyosaiservice;

    @RequestMapping(value = "/jyohoSyosai", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public JyohoSyosaiOutDto requestSyosai(@RequestBody JyohoSyosaiWebDto jswd) {
        int requser_id = jswd.getUser_id();

        //JyohoSyosaiInDTO.classにReqparamに入れる処理
        JyohoSyosaiInDto jyohosyosaiindto = new JyohoSyosaiInDto();
        jyohosyosaiindto.setUser_id(requser_id);

        //JyohoSyosaiServiceにDtoを渡し、結果をもらう;
        JyohoSyosaiOutDto jsod = jyohosyosaiservice.syosaiHyoji(jyohosyosaiindto);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(jsod));
            System.out.println("きてる２************************");
            System.out.println(requser_id);
            System.out.println("************************");
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JyohoSyosaiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsod;
    }

    @RequestMapping(value = "/kihonupdate", method = RequestMethod.POST)
    @ResponseBody//JSON形式
    public JyohoSyosaiOutDto requestKihonUpdate(@RequestBody JyohoSyosaiWebDto ktwd) {
        //jsonで受け取った値をJyohoSyosaiWebDtoに渡して、以下で各値に戻す。
        String reqname = ktwd.getName();
        int reqdepertment_id = ktwd.getDepartment_id();
        int reqjoinedyear = ktwd.getJoinedyear();
        int requser_id = ktwd.getUser_id();
        String reqemail = ktwd.getEmail();
        ///InDTOにニックネーム、部署、入社年を入れる処理
        JyohoSyosaiInDto jyohoSyosaiInDto = new JyohoSyosaiInDto();
        jyohoSyosaiInDto.setName(reqname);
        jyohoSyosaiInDto.setDepartment_id(reqdepertment_id);
        jyohoSyosaiInDto.setJoinedyear(reqjoinedyear);
        jyohoSyosaiInDto.setUser_id(requser_id);
        jyohoSyosaiInDto.setEmail(reqemail);

        //OutDtoに、true/falseとmail_no、を入れる処理
        JyohoSyosaiOutDto jyohoSyosaiOutDto = jyohosyosaiservice.kihonUpdate(jyohoSyosaiInDto);

        //serviceOutDtoのgetFlag()によって、true/false、を返す
        return jyohoSyosaiOutDto;

    }
}
