package maven.springboot.Party_Ownername.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Party_Ownername.service.dto.Party_OwnernameInDto;
import maven.springboot.Party_Ownername.service.dto.Party_OwnernameWebOutDto;
import maven.springboot.Repository.T_UsersRepository;

@Service  // DIする対象の目印
public class Party_OwnernameService {

    @Autowired
    private T_UsersRepository t_usersRepository;

    public Party_OwnernameWebOutDto selectowner(Party_OwnernameInDto party_OwnernameIndto) {

        Party_OwnernameWebOutDto party_OwnernameWebOutDto = new Party_OwnernameWebOutDto();

        int ownerid = party_OwnernameIndto.getOwner();

        System.out.println("オーナーのユーザID" + ownerid);
        
        List<T_UsersEntity> list = t_usersRepository.findByUser_id(ownerid);
        int count = list.size();

        if (count == 0) {
            System.out.println("★★★★★対象のオーナは存在しません★★★★");
        } else {
            party_OwnernameWebOutDto.setOwnername(list.get(0).getName());
        }

        return party_OwnernameWebOutDto;
    }
}
