package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.M_DepartmentsEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface M_DepartmentsRepository extends JpaRepository<M_DepartmentsEntity, Integer> {

        @Query(value = "select m from M_DepartmentsEntity m where m.department_name = :department_name")
        public List<M_DepartmentsEntity> findByDepartment_name(@Param("department_name")String department_name);

        @Query(value = "select m from M_DepartmentsEntity m where m.department_id = :department_id")
        public List<M_DepartmentsEntity> findByDepartment_id(@Param("department_id")int department_id);

}

