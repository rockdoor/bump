/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Tyo_partyKaitou.service.dto;

/**
 *
 * @author t4-yoshioka
 */
public class Tyo_partyKaitouInDto {
    
    private int user_id;
    private String party_name;
    private int attend_cd;
    
    

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the party_name
     */
    public String getParty_name() {
        return party_name;
    }

    /**
     * @param party_name the party_name to set
     */
    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    /**
     * @return the attend_cd
     */
    public int getAttend_cd() {
        return attend_cd;
    }

    /**
     * @param attend_cd the attend_cd to set
     */
    public void setAttend_cd(int attend_cd) {
        this.attend_cd = attend_cd;
    }
    
    
    
}
