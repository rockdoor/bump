package maven.springboot.KininaruToroku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

import maven.springboot.Entity.M_LikesEntity;
import maven.springboot.KininaruToroku.service.dto.KininaruSettingWebOutDto;
import maven.springboot.Repository.M_LikesRepository;

@Service  // DIする対象の目印
public class KininaruSettingService {

    @Autowired
    private M_LikesRepository m_likesRepository;

    public KininaruSettingWebOutDto kininaruSetting() {
        KininaruSettingWebOutDto ktwod = new KininaruSettingWebOutDto();
        List<M_LikesEntity> kininaru_List = m_likesRepository.findAllOrder();

        int count = kininaru_List.size();

        if (count == 0) {
            System.out.println("★★★★★m_likesのkininaru_Listがnullです⇒マスタを更新して下しさい★★★★");
        } else {
            ktwod.setKininaruSettingWebOutDto(kininaru_List);
        }
        return ktwod;
    }

}
