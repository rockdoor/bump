package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;
import maven.springboot.Entity.T_Parties_UsersEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_Parties_UsersRepository extends JpaRepository<T_Parties_UsersEntity, Integer> {

        @Query(value = "select t from T_Parties_UsersEntity t where t.user_id = :user_id")
        public List<T_Parties_UsersEntity> findByUser_id(@Param("user_id")int user_id);
        
        @Query(value = "select t from T_Parties_UsersEntity t where t.party_id = :party_id")
        public List<T_Parties_UsersEntity> findByParty_id(@Param("party_id")int party_id);

        @Query(value = "select t from T_Parties_UsersEntity t where t.user_id = :user_id and t.attend_cd = 2")
        public List<T_Parties_UsersEntity> findByUser_idAttend_cd(@Param("user_id")int user_id);
        
        @Query(value = "select t from T_Parties_UsersEntity t where t.user_id = :user_id and t.party_id = :party_id")
        public List<T_Parties_UsersEntity> findByUser_idAndParty_id(@Param("user_id")int user_id,@Param("party_id")int party_id);

        @Query(value = "select t from T_Parties_UsersEntity t where t.user_id = :user_id and t.party_id = :party_id")
        public List<T_Parties_UsersEntity> findByUser_idPartyid(@Param("user_id")int user_id, @Param("party_id")int party_id);
        
        //@Query(value = "select t from T_Parties_UsersEntity t natural left join T_UsersEntity u where user_id = :user_id")
        //public List<T_Parties_UsersEntity> findByUser_name(@Param("party_id")int party_id);
}
