/* global $routeScope */

angular.module('MyApp')
        .controller('UserTorokuController', ['$scope', '$http', '$location', '$rootScope',
            function ($scope, $http, $location, $rootScope) {

                $scope.doUserToroku = function () {
                    $http({
                        method: 'POST',
                        data: {email: $scope.email_cfm},
                        url: '/userToroku'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.flag == false) {
                            alert('すでに登録済みです！');
                            console.log("登録失敗");
                        } else {
                            $location.path('/tempentry');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
            }])
        .controller('EntryController', ['$scope', '$http', '$location', '$rootScope', '$routeParams',
            function ($scope, $http, $location, $rootScope, $routeParams) {
                $http({
                    method: 'POST',
                    data: {url_id: $routeParams.id
                    },
                    url: 'emailforentry'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data);
                });
                $scope.url_id = $routeParams.id;
                $scope.doEntry = function () {
                    $http({
                        method: 'POST',
                        data: {url_id: $scope.url_id,
                            password: $scope.password},
                        url: '/registry'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log($scope.results);
                        console.log(status);
                        console.log(data);
                        console.dir(data);
                        if ($scope.results.flag == false) {
                            alert('登録に失敗しました。');
                            console.log("登録失敗");
                        } else {
                            console.log('登録成功');
                            alert('登録完了しました！ログインページに遷移します！');
                            $location.path('/');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
            }])
        .controller('EntryController', ['$scope', '$http', '$location', '$rootScope', '$routeParams',
            function ($scope, $http, $location, $rootScope, $routeParams) {
                $http({
                    method: 'POST',
                    data: {url_id: $routeParams.id
                    },
                    url: 'emailforentry'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log(status);
                    console.log(data);
                });
                $scope.url_id = $routeParams.id;
                $scope.doEntry = function () {
                    $http({
                        method: 'POST',
                        data: {url_id: $scope.url_id,
                            password: $scope.password},
                        url: '/registry'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log($scope.results);
                        console.log(status);
                        console.log(data);
                        console.dir(data);
                        if ($scope.results.flag == false) {
                            alert('登録に失敗しました。');
                            console.log("登録失敗");
                        } else {
                            console.log('登録成功');
                            alert('登録完了しました！ログインページに遷移します！');
                            $location.path('/');
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
            }]);
