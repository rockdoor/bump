package maven.springboot.KininaruToroku.service.dto;

//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

public class KininaruTorokuWebInDto {

     private int user_id;
    private List<String> artifact_list = new ArrayList<>();

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the artifact_list
     */
    public List<String> getArtifact_list() {
        return artifact_list;
    }

    /**
     * @param artifact_list the artifact_list to set
     */
    public void setArtifact_list(List<String> artifact_list) {
        this.artifact_list = artifact_list;
    }
   
}