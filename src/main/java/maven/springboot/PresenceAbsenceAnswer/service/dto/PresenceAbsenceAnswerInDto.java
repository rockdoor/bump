/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.PresenceAbsenceAnswer.service.dto;

/**
 *
 * @author t4-yoshioka
 */
public class PresenceAbsenceAnswerInDto {
    
    //パーティid(party_id)、個人id（user_id）、出席区分（attend_cd）がＩｎＤｔｏに入っている
    private int party_id;
    private int user_id;
    private int attend_cd;

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the attend_cd
     */
    public int getAttend_cd() {
        return attend_cd;
    }

    /**
     * @param attend_cd the attend_cd to set
     */
    public void setAttend_cd(int attend_cd) {
        this.attend_cd = attend_cd;
    }
    
    
    
    
}
