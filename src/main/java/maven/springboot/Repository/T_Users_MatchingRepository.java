package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;
import maven.springboot.Entity.T_Users_MatchingEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_Users_MatchingRepository extends JpaRepository<T_Users_MatchingEntity, Integer> {

    @Query(value = "select t from T_Users_MatchingEntity t where t.user_id = :user_id")
    public List<T_Users_MatchingEntity> findByUser_id(@Param("user_id") int user_id);

    @Query(value = "select t from T_Users_MatchingEntity t where t.user_id = :user_id and t.t_ok_ng = :t_ok_ng")
    public List<T_Users_MatchingEntity> findByUser_idT_ok_ng(@Param("user_id") int user_id, @Param("t_ok_ng") int t_ok_ng);
}