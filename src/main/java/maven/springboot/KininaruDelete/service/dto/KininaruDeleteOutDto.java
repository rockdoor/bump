package maven.springboot.KininaruDelete.service.dto;

public class KininaruDeleteOutDto {

    private int dflag;
    /**
     * @return the dflag
     */
    public int getDflag() {
        return dflag;
    }

    /**
     * @param dflag the dflag to set
     */
    public void setDflag(int dflag) {
        this.dflag = dflag;
    }
    
    
}
