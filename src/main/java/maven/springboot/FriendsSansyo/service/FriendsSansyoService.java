package maven.springboot.FriendsSansyo.service;

import maven.springboot.FriendsSansyo.Controller.FriendsSansyoController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_FriendsEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Repository.T_FriendsRepository;
import maven.springboot.Repository.T_UsersRepository;

import java.util.*;
import maven.springboot.Logic.Mailpost;
import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoWebDto;
import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoInDto;
import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoOutDto;

@Service  // DIする対象の目印
public class FriendsSansyoService {

    //DI層へ接続
    @Autowired
    private T_FriendsRepository friends_repository;

    @Autowired
    private T_UsersRepository users_repository;
//
//    @Autowired
//    private Mailpost mailpost;

    public FriendsSansyoWebDto FriendsSansyo(FriendsSansyoInDto fsid) throws Exception {
        
        FriendsSansyoWebDto fwd = new FriendsSansyoWebDto();
        int eflag=0;
         
        List<T_FriendsEntity> friends_list = friends_repository.findByUser_id(fsid.getUser_id());
        int count = friends_list.size();
        
        if(count == 0){
            eflag=0; //エラー:対象のデータが一件もない
            System.out.println("対象のデータがありません");
            
        }else{
            eflag=1; //成功
           
            List<FriendsSansyoOutDto> fsod_friendslist = new ArrayList<>();
            List<FriendsSansyoOutDto> fsod_unfriendslist = new ArrayList<>();
            
            for (T_FriendsEntity t_FriendsEntity : friends_list) {
                
                FriendsSansyoOutDto fsod = new FriendsSansyoOutDto();
                
                    fsod.setFlag(t_FriendsEntity.getFlag()); //承認フラグを格納
                    List<T_UsersEntity> get_friends_info = users_repository.findByUser_id(t_FriendsEntity.getFriends_id());
                    fsod.setFriend_mail(get_friends_info.get(0).getEmail());//メールを格納
                    fsod.setFriend_name(get_friends_info.get(0).getName());//名前を格納
                    fsod_friendslist.add(fsod);            
                }                
            
            fwd.setFriends_list(fsod_friendslist);
            fwd.setEflag(eflag);
        }
        
        return fwd;
    }
}
