/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.FriendsToroku.service.dto;

/**
 *
 * @author y-katakami
 */
public class FriendsTorokuWebOutDto {
    private int error_flag;
   
    public int getFriendsTorokuFlag() {
        return error_flag;
    }
       
    public void setFriendsTorokuFlag(int error_flag) {
        this.error_flag = error_flag;
    }
    
    
    
}
