package maven.springboot.FriendsToroku.service;

import maven.springboot.FriendsToroku.Controller.FriendsTorokuController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Repository.T_FriendsRepository;
import maven.springboot.Repository.T_UsersRepository;
import java.util.*;
import maven.springboot.Entity.T_FriendsEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.FriendsToroku.service.dto.FriendsTorokuInDto;
import maven.springboot.FriendsToroku.service.dto.FriendsTorokuWebOutDto;


@Service
public class FriendsTorokuService {

    //DI層へ接続
    @Autowired
    private T_FriendsRepository t_friendsRepository;
    @Autowired
    private T_UsersRepository t_UsersRepository;

    public FriendsTorokuWebOutDto friendstoroku(FriendsTorokuInDto ftid) {

        FriendsTorokuWebOutDto ftwod = new FriendsTorokuWebOutDto();

        //error原因格納用変数
        int eflag = 0;
        int sflag = 0;
        int data_num = 0;
        int check_num = 0;
        int count = 0;
        
        //使用するEntityを宣言
        T_FriendsEntity t_friendsEntity = new T_FriendsEntity();
        T_UsersEntity t_UsersEntity = new T_UsersEntity();
        
        HashMap<String, String> map = ftid.getFriend_hash();
        int user_id = ftid.getUser_id();
        
        for (Map.Entry<String, String> entry : map.entrySet()) {
            
            if (entry.getValue().equals("true")) {
                //キー情報を元に個人情報(名前とアドレス)を検索
                List<T_UsersEntity> user_list = t_UsersRepository.findByUser_id(Integer.parseInt(entry.getKey()));
                count = user_list.size();
                //検索結果を取得した場合 
                if(count == 1){
                   //取得した名前、アドレス、キー情報(id)、判定フラグをセット(登録)
                   t_friendsEntity.setUser_id(user_id);
                   t_friendsEntity.setFriends_id(Integer.parseInt(entry.getKey()));
                   t_friendsEntity.setFlag(sflag);
                   t_friendsRepository.save(t_friendsEntity);
                   data_num = data_num + 1; 
                }
                check_num = check_num + 1;
            
            }
        }
        
        
        //登録データ数が0件であった場合、もしくは取得したキー情報から個人情報を検索できなかった場合
        if(data_num == 0 || data_num != check_num ){
            eflag = 1;
        }     
        ftwod.setFriendsTorokuFlag(eflag);
        
        return ftwod; //エラーフラグをリターン
    }

}
