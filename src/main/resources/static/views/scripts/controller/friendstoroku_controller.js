/* global $routeScope */

angular.module('MyApp')
        .controller('FriendsTorokuController', ['$scope', '$http', '$location', '$rootScope','$uibModal',
            function ($scope, $http, $location, $rootScope, $uibModal) {
                
                $scope.ftoroku = {result: {}}; //ハッシュマップ(連想配列型を宣言)
                $scope.show = false;
                
                //検索機能
                $scope.doFriendsKensaku = function () {
                    
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id,
                                mail: $scope.mail,
                                //jsonに数値型で入れたいためNumber型に変換する
                                department_id: Number($scope.department_id)
                        },
                        url: '/friendsKensaku'
                    }).success(function (data, status) {
                        
                        $rootScope.fresearch_results = data;
                        console.log(status);
                        console.log(data);   
                        $location.path('/friendstoroku');
                        
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    
                };
//                
//                $scope.friends = [
//                    {"id": "4", "name": "山田太郎", "address": "yamada1@nri.co.jp"},
//                    {"id": "5", "name": "山田二郎", "address": "yamada2@nri.co.jp"},
//                    {"id": "6", "name": "山田三郎", "address": "yamada3@nri.co.jp"}
//                ];
//              
                //登録機能
                $scope.doFriendsToroku = function () { 
                    
                    $http({
                        method: 'POST',
                        data: {
                            friend_hash: $scope.ftoroku.result, //Postするデータの内容
                            user_id: $rootScope.user_id
                        }, //←dto上で定義したデータ(JSONとしてサーバー側に送るデータ)
                        url: '/friendsToroku' //どのURLからきているのかをサーバー側に明示しているもの
                    }).success(function (data, status, headers, config) { //Postの送信の有無(成功かエラーか。dataはサーバー側からの返り値)
                        var uibModalInstance = $uibModal.open({
                        templateUrl: '/views/FriendsTorokuCheck.html',
                        controller: 'FriendsTorokuController',
                        backdrop: 'static',
                        scope: $scope
                        });
                        $scope.results = data;
                        console.log(status);
                        console.log(data.count);
                        console.log("登録完了");

                    }).error(function (data, status, headers, config) {
                        console.log(status);
                        
                    });
                };


            }]);

 