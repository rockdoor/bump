package maven.springboot.Tyo_partyKakunin.service.dto;

import java.util.List;

public class Tyo_partyKakuninOutDto {

    private int party_id;
    private String party_name;
    private int date;
    private List<Tyo_parties_users_Dto> tyo_parties_users_Dto;

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the party_name
     */
    public String getParty_name() {
        return party_name;
    }

    /**
     * @param party_name the party_name to set
     */
    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    /**
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(int date) {
        this.date = date;
    }

    /**
     * @return the tyo_parties_users_Dto
     */
    public List<Tyo_parties_users_Dto> getTyo_parties_users_Dto() {
        return tyo_parties_users_Dto;
    }

    /**
     * @param tyo_parties_users_Dto the tyo_parties_users_Dto to set
     */
    public void setTyo_parties_users_Dto(List<Tyo_parties_users_Dto> tyo_parties_users_Dto) {
        this.tyo_parties_users_Dto = tyo_parties_users_Dto;
    }

}