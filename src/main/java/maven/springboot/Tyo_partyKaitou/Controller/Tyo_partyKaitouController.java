/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Tyo_partyKaitou.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Tyo_partyKaitou.service.Tyo_partyKaitouService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouInDto;
import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouOutDto;
import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouWebDto;
import maven.springboot.Tyo_partyKaitou.service.dto.Tyo_partyKaitouupdateWebDto;
import org.springframework.web.bind.annotation.RequestBody;



/**
 *
 * @author t4-yoshioka
 */

@Controller
public class Tyo_partyKaitouController {
    
    /*★★★★mail_no(個人識別番号)に応じた各情報のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private Tyo_partyKaitouService tyo_partyKaitouservice;
    
    @RequestMapping(value = "/tyo_PartyKaitou", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Tyo_partyKaitouOutDto requestNomikai(@RequestBody Tyo_partyKaitouWebDto tkwd) {
        int requser_id = tkwd.getUser_id();
        
        System.out.println("************************");
        System.out.println(requser_id);
        System.out.println("************************");
        
        //Tyo_partyKaitouInDTO.classにReqparamに入れる処理
        Tyo_partyKaitouInDto tyo_partyKaitouInDto = new Tyo_partyKaitouInDto();
        tyo_partyKaitouInDto.setUser_id(requser_id);
        
//        //きてるよ
//        System.out.println("************************");
//        System.out.println(requser_id);
//        System.out.println("************************");
        
        //Tyo_partyKaitouServiceにDtoを渡し、結果をもらう;
        Tyo_partyKaitouOutDto tkod = tyo_partyKaitouservice.nomikaiHyoji(tyo_partyKaitouInDto);
        
        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(tkod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Tyo_partyKaitouController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("*******************");
        System.out.println("きてる１");
        System.out.println("*******************");
        
        
        return tkod;        
    }

    
    @RequestMapping(value = "/nomikaiupdate", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Tyo_partyKaitouupdateWebDto updateNomikai(@RequestBody Tyo_partyKaitouupdateWebDto webdto) {
        
        List<T_Parties_UsersEntity> list = webdto.getList();
        
        System.out.println("************************");
        for (T_Parties_UsersEntity t_Parties_UsersEntity : list) {
            System.out.println(t_Parties_UsersEntity.getAttend_cd());
        }
        System.out.println("************************");

        //サービス呼び出し
        tyo_partyKaitouservice.nomikaiUpdate(webdto);
        

        
        return webdto;        
    }

    
}
