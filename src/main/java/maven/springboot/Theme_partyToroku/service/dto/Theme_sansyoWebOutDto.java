package maven.springboot.Theme_partyToroku.service.dto;

import java.util.List;
import maven.springboot.Entity.M_LikesEntity;

public class Theme_sansyoWebOutDto {

    private List<M_LikesEntity> m_LikesEntity_list;

    /**
     * @return the m_LikesEntity_list
     */
    public List<M_LikesEntity> getM_LikesEntity_list() {
        return m_LikesEntity_list;
    }

    /**
     * @param m_LikesEntity_list the m_LikesEntity_list to set
     */
    public void setM_LikesEntity_list(List<M_LikesEntity> m_LikesEntity_list) {
        this.m_LikesEntity_list = m_LikesEntity_list;
    }
    
}
