package maven.springboot.Logic;

import java.util.ArrayList;
import java.util.List;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.M_DepartmentsEntity;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.M_DepartmentsRepository;
import static org.eclipse.jdt.internal.compiler.parser.Parser.name;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class rakurakuMail {

    @Autowired
    private T_UsersRepository t_UsersRepository;
    @Autowired
    private T_PartiesRepository t_PartiesRepository;
    @Autowired
    private T_Parties_UsersRepository t_Parties_UsersRepository;
    @Autowired
    private M_DepartmentsRepository m_DepartmentsRepository;

    public void Mailsend(int party_id) throws Exception {

        //メールを送りたい人のUser_idをもとにT_UsersEntityのリストを作成する
        //List<T_UsersEntity> list_user = t_UsersRepository.findByUser_id(user_id);
        //Party_idをもとにT_PartiesEntityのリストを作成する
        List<T_PartiesEntity> list_party = t_PartiesRepository.findByParty_id(party_id);

        //ありえないが一応あるか確認
        int count_party = list_party.size();
        //一応エラーメッセージを格納しておく
        String errormessage;

        if (count_party > 0) {
            //メール本文に記載する為の情報を格納しておく(個人名、飲み会名、飲み会日)
            //String name_user = list_user.get(0).getName();
            //String address_user = list_user.get(0).getEmail();
            String name_party = list_party.get(0).getParty_name();
            int date_party = list_party.get(0).getDate();
            //開催日を○○○○年○○月○○日に
            StringBuilder hizuke = new StringBuilder();
            hizuke.append(date_party);
            hizuke.insert(4, "年");
            hizuke.insert(7, "月");
            hizuke.insert(10, "日");

            List<String> nameList = new ArrayList();
            List<String> emailList = new ArrayList();
            List<T_UsersEntity> user = new ArrayList();
            String w_list = "";
            String crlf = System.getProperty("line.separator");
            int count1 = 0;

            //支払い者のuser_id取得
            int payuser_id = list_party.get(0).getPayer_id();
            //メールを送りたい人のUser_idをもとにT_UsersEntityのリストを作成する(pay_user_idのリスト作成) 
            List<T_UsersEntity> list_payuser = t_UsersRepository.findByUser_id(payuser_id);
            //支払い者の情報取得(名前、メールアドレス、部署ID)
            String mail_payer = list_payuser.get(0).getEmail();
            String name_payer = list_payuser.get(0).getName();
            int department_id = list_payuser.get(0).getDepartment_id();
            //支払い者の部署名取得
            List<M_DepartmentsEntity> list_department = m_DepartmentsRepository.findByDepartment_id(department_id);
            String payer_department=list_department.get(0).getDepartment_name();

            //出席者一覧を飲み会IDをもとに取得
            List<T_Parties_UsersEntity> list_kaikei = t_Parties_UsersRepository.findByParty_id(party_id);
            for (T_Parties_UsersEntity t_Parties_UsersEntity : list_kaikei) {
                if (t_Parties_UsersEntity.getAttend_cd() != 1) {
                    //参加者一人分の情報丸々
                    user = t_UsersRepository.findByUser_id(t_Parties_UsersEntity.getUser_id());
                    //参加者の名前、アドレス、支払い金額取得
                    for (T_UsersEntity t_UsersEntity : user) {
                        nameList.add(t_UsersEntity.getName());
                        emailList.add(t_UsersEntity.getEmail());
                        count1++;
                        w_list += t_UsersEntity.getName() + "さん   　" + t_Parties_UsersEntity.getPayment() + "円" + "\n";
                    }
                }

            }

            //int count1 = t_Parties_UsersRepository.findByParty_id(party_id).size();
            System.out.println("=============================");
            System.out.println(count1);
            System.out.println("=============================");
            if (count1 > 0) {
                for (int i = 0; i < count1; i++) {
                    String name = nameList.get(i);
                    String address = emailList.get(i);
                    //メール送信クラスをインスタンス化しておく
                    //Mailsend mailsend = new Mailsend();
                    String subject = "飲み会精算通知";

                    //検索されたuserのアドレスに通知メールを送信
                    //メール送信に用いる値を定義
                    String sendto = address;
                    String message = name + "さん" + crlf + crlf
                            + "Bump!からのお知らせです。" + crlf
                            + hizuke + "に参加していただいた" + crlf
                            + name_party + "の精算連絡です。" + crlf + crlf
                            + "---------------------------" + crlf
                            + w_list
                            + "---------------------------" + crlf
                            + "お支払いは、" + crlf
                            + payer_department + " " + name_payer + "(" + mail_payer + ")" + crlf
                            + "までお願いいたします。" + crlf + crlf
                            + "以上です。よろしくお願いいたします。";

                    //mail送信メソッドでメール送信(シェルなのでAWSのみ起動)
                    //mailsend.send(message, sendto, subject);
                    //ローカルでのテスト用
                    JavaMail javamail = new JavaMail();
                    //nriドメインだったら送れる設定になってる
                    String from = "a-nanasawa@nri.co.jp";
                    javamail.send(sendto, subject, message, from);
                }
            } else {
                System.out.println("参加者が存在しません");
            }
        } else {
            System.out.println("飲み会が存在しません。");
        }
    }

}
