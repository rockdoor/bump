package maven.springboot.KininaruSansyo.service;

import java.util.ArrayList;
import java.util.List;
import maven.springboot.Entity.M_LikesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import maven.springboot.Entity.T_Users_LikesEntity;
import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoInDto;
import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoOutDto;
import maven.springboot.KininaruSansyo.service.dto.KininaruSansyoWebOutDto;
import maven.springboot.Repository.M_LikesRepository;
import maven.springboot.Repository.T_Users_LikesRepository;

@Service  // DIする対象の目印
public class KininaruSansyoService {

    @Autowired
    private T_Users_LikesRepository t_Users_LikesRepository;

    @Autowired
    private M_LikesRepository m_LikesRepository;

    public KininaruSansyoWebOutDto kininaruSansyo(KininaruSansyoInDto ksid) {
        KininaruSansyoWebOutDto kswod = new KininaruSansyoWebOutDto();
        List<T_Users_LikesEntity> users_likes_list = t_Users_LikesRepository.findByUser_id(ksid.getUser_id());

        int count = users_likes_list.size();

        if (count == 0) {
            System.out.println("★★★★★t_users_likesにuser_idに紐づく気になることは存在しません。★★★★");
        } else {
            
            List<KininaruSansyoOutDto> ksod_list = new ArrayList<>();
            for (T_Users_LikesEntity t_Users_LikesEntity : users_likes_list) {
                KininaruSansyoOutDto ksod = new KininaruSansyoOutDto();
                M_LikesEntity get_like_name = m_LikesRepository.findByArtifact_id(t_Users_LikesEntity.getArtifact_id());
                ksod.setLike_name(get_like_name.getLike_name());
                ksod.setArtifact_id(get_like_name.getArtifact_id());
                ksod_list.add(ksod);
            }
            kswod.setKininaruSansyoWebOutDto(ksod_list);
        }
        return kswod;
    }
}
