/* global $routeScope */

angular.module('MyApp')
        .controller('TopController', ['$scope', '$http', '$location', '$rootScope', '$anchorScroll',
            function ($scope, $http, $location, $rootScope, $anchorScroll) {


                //header表示フラグをfalseに変更(初期値)
                $rootScope.header = false;
                $rootScope.footer = true;

                //Top画面でのみ表示
                //$rootScope.top_footer = true;

                //more,close表示用フラグ
                $scope.accord01 = false;
                $scope.accord02 = false;
                $scope.accord03 = false;
                $scope.accord04 = false;
                $scope.accord05 = false;

                $scope.login = false;

                //ページトップへ移動する機能
                $scope.jumpTo = function (id) {
                    $location.hash(id);
                    $anchorScroll();
                }

                $scope.doLogin = function () {
//                    $rootScope.email = $scope.email;
                    $http({
                        method: 'POST',
                        data: {email: $scope.user.mail,
                            password: $scope.user.password},
                        url: '/login'
                    }).success(function (data, status, headers, config) {
                        $scope.results = data;
                        console.log(status);
                        console.log(data);
                        if (data.flag == true) {
                            console.log('ログイン成功');
                            $rootScope.user_id = data.user_id

                            //header表示フラグをtrueに変更
                            $rootScope.header = true;
                            $rootScope.footer = false;

                            //Topで表示したフッダーを非表示にする
                            $rootScope.top_footer = false;


                            $location.path('/mypage');
                        } else {
                            alert("登録されていないか、パスワードが間違っています");
                        }
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
            }]);
