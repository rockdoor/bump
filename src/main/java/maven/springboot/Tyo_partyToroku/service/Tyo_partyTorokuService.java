package maven.springboot.Tyo_partyToroku.service;

import maven.springboot.Tyo_partyToroku.Controller.Tyo_partyTorokuController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Entity.T_Tyo_PartiesEntity;
import maven.springboot.Repository.T_Tyo_PartiesRepository;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_UsersRepository;

import java.util.*;
import maven.springboot.Logic.Mailpost;
import maven.springboot.Logic.Random8;

import maven.springboot.Tyo_partyToroku.service.dto.Tyo_partyTorokuInDto;
import maven.springboot.Tyo_partyToroku.service.dto.Tyo_partyTorokuOutDto;

@Service  // DIする対象の目印
public class Tyo_partyTorokuService {

    //DI層へ接続
    @Autowired
    private T_Tyo_PartiesRepository t_Tyo_PartiesRepository;
    
    @Autowired
    private T_PartiesRepository repository;

    @Autowired
    private T_Parties_UsersRepository u_repository;

    @Autowired
    private T_UsersRepository users_repository;

    @Autowired
    private Mailpost mailpost;

    @Autowired
    private Random8 random;

    public Tyo_partyTorokuOutDto PartyToroku(Tyo_partyTorokuInDto tyo_partyInDto) throws Exception {
        
        Tyo_partyTorokuOutDto tyo_partyOutDto = new Tyo_partyTorokuOutDto();

        int Party_id = 0;
        int Department_id;
        int User_id;

        //T_PartiesEntityをインスタンス化し、値を入れる
        T_PartiesEntity entity = new T_PartiesEntity();

        entity.setParty_cd(tyo_partyInDto.getParty_cd());
        entity.setParty_name(tyo_partyInDto.getParty_name());
        entity.setUser_id(tyo_partyInDto.getUser_id());
        entity.setDate(tyo_partyInDto.getDate());
        entity.setTime(tyo_partyInDto.getStarttime());
        entity.setPayer_id(tyo_partyInDto.getUser_id());

        //URL設定用のurl_idを発行
        int url_id = random.makeramdom();

        //url_idもテーブルに格納
        entity.setUrl_id(url_id);

        // insert文の発行
        repository.save(entity);

        //T_PartiesEntityをもう一度検索、更新行を取り出し、Party_idの取得
        List<T_PartiesEntity> list = repository.findByAddress(
                tyo_partyInDto.getUser_id(), tyo_partyInDto.getStarttime(), tyo_partyInDto.getDate());
        int i = list.size();
        Party_id = list.get(i - 1).getParty_id();

        //T_Tyo_PartiesEntityをインスタンス化し、値を入れる
        T_Tyo_PartiesEntity t_Tyo_PartiesEntity = new T_Tyo_PartiesEntity();

        t_Tyo_PartiesEntity.setParty_id(Party_id);
        t_Tyo_PartiesEntity.setDeadline(tyo_partyInDto.getDeadline());
        
        //insert文の発行
        t_Tyo_PartiesRepository.save(t_Tyo_PartiesEntity);

        tyo_partyOutDto.setParty_id(Party_id);

        //T_UsersEntityを検索、User_idで更新行を取り出し、Department_idを取得
        List<T_UsersEntity> users_list = users_repository.findByUser_id(tyo_partyInDto.getUser_id());
        Department_id = users_list.get(0).getDepartment_id();
        System.out.println("----------------------------------------");
        System.out.println("Department_id=" + Department_id);
        System.out.println("-------------------------------------------");
        
        System.out.println("deadline=" + tyo_partyInDto.getDeadline());

        //User_idを元に対象となる同じ部署の人にメールを送信する
        mailpost.Mailsend(tyo_partyInDto.getUser_id(), url_id, tyo_partyInDto.getParty_name(), tyo_partyInDto.getDate(), tyo_partyInDto.getStarttime(), tyo_partyInDto.getDeadline());

        //T_UsersEntityを検索、Department_idが一致する行を取り出し、User_id一覧を取得
        List<T_UsersEntity> users_list2 = users_repository.findByDepartment_id(Department_id);

        for (T_UsersEntity t_UsersEntity : users_list2) {
            User_id = t_UsersEntity.getUser_id();
            String name = t_UsersEntity.getName();

            //paymentrateの初期値
            int rate = 5;

            //showed_flagの初期値
            boolean showed_flag = false;

            //T_Parties_UsersEntityをインスタンス化し、値を入れる
            T_Parties_UsersEntity u_entity = new T_Parties_UsersEntity();
            u_entity.setParty_id(tyo_partyOutDto.getParty_id());
            u_entity.setUser_id(User_id);
            u_entity.setName(name);
            u_entity.setParty_name(tyo_partyInDto.getParty_name());
            u_entity.setPayment_rate(rate);
            u_entity.setShowed_flag(showed_flag);

            // insert文の発行
            u_repository.save(u_entity);
        }

        return tyo_partyOutDto;
    }
}
