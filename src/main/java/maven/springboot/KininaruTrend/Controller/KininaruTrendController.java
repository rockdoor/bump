package maven.springboot.KininaruTrend.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.KininaruTrend.service.KininaruTrendService;
import maven.springboot.KininaruTrend.service.dto.KininaruTrendInDto;
import maven.springboot.KininaruTrend.service.dto.KininaruTrendOutDto;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KininaruTrendController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private KininaruTrendService kininaruTrendService;
//気になること登録ページ生成用のJava⇒単純にlikesのマスタを出力するだけ。

    @RequestMapping(value = "/trendsetting")
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public KininaruTrendOutDto requestKininaruTrend()  throws JsonProcessingException  {
        System.out.println("～～～～～～～～～気になるトレンド到達～～～～～～～～～");
        KininaruTrendInDto trend_indto = new KininaruTrendInDto();
        //Date型で現在時刻をセット
        Date currentDate = new Date();
        //long型でミリ秒に変換
        long miliDate = currentDate.getTime();
        //一週間分のミリ秒を引いたlastweekを用意
        Date lastWeek = new Date(miliDate - (1000 * 60 * 60 * 24 * 7));
        
        trend_indto.setCurrentDate(currentDate);
        trend_indto.setLastweek(lastWeek);
        
        KininaruTrendOutDto trend_outdto = kininaruTrendService.kininaruTrend(trend_indto);
        
        return trend_outdto;

    }
}