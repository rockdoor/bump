package maven.springboot.KininaruTrend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import maven.springboot.Entity.T_TrendsEntity;

import maven.springboot.KininaruTrend.service.dto.KininaruTrendInDto;
import maven.springboot.KininaruTrend.service.dto.KininaruTrendOutDto;
import maven.springboot.Repository.T_TrendsRepository;

@Service  // DIする対象の目印
public class KininaruTrendService {

    @Autowired
    private T_TrendsRepository t_TrendsRepository;

    public KininaruTrendOutDto kininaruTrend(KininaruTrendInDto trend_indto) {

        // select文を発行し、要素数を代入
        List<Object> trend_list = t_TrendsRepository.getTrend(trend_indto.getCurrentDate(), trend_indto.getLastweek());
        KininaruTrendOutDto trend_outdto = new KininaruTrendOutDto();
        
        trend_outdto.setT_Trendslist(trend_list);
                
        return trend_outdto;
    }
}

