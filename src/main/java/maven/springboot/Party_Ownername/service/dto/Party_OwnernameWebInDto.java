package maven.springboot.Party_Ownername.service.dto;

public class Party_OwnernameWebInDto {

    private int owner;

    /**
     * @return the owner
     */
    public int getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(int owner) {
        this.owner = owner;
    }

    
}
