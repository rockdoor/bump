/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Entity;

/**
 *
 * @author y-katakami
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;


@Entity
@Table(name = "t_theme_paties")
public class T_Theme_PartiesEntity {

    @Id
    @GeneratedValue
    private Integer party_id;

    @Column(name = "artifact_id")
    private int artifact_id;

    @Column(name = "theme_area_id")
    private int theme_area_id;

    @Column(name = "theme_genre_id")
    private int theme_genre_id;

    @Column(name = "holding_max_person")
    private int holding_max_person;

    @Column(name = "recruit_max_person")
    private int recruit_max_person;

    @Column(name = "deaddate")
    private int deaddate;

    @Column(name = "deadtime")
    private int deadtime;

    /**
     * @return the party_id
     */
    public Integer getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(Integer party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the artifact_id
     */
    public int getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(int artifact_id) {
        this.artifact_id = artifact_id;
    }

    /**
     * @return the theme_area_id
     */
    public int getTheme_area_id() {
        return theme_area_id;
    }

    /**
     * @param theme_area_id the theme_area_id to set
     */
    public void setTheme_area_id(int theme_area_id) {
        this.theme_area_id = theme_area_id;
    }

    /**
     * @return the theme_genre_id
     */
    public int getTheme_genre_id() {
        return theme_genre_id;
    }

    /**
     * @param theme_genre_id the theme_genre_id to set
     */
    public void setTheme_genre_id(int theme_genre_id) {
        this.theme_genre_id = theme_genre_id;
    }

    /**
     * @return the holding_max_person
     */
    public int getHolding_max_person() {
        return holding_max_person;
    }

    /**
     * @param holding_max_person the holding_max_person to set
     */
    public void setHolding_max_person(int holding_max_person) {
        this.holding_max_person = holding_max_person;
    }

    /**
     * @return the recruit_max_person
     */
    public int getRecruit_max_person() {
        return recruit_max_person;
    }

    /**
     * @param recruit_max_person the recruit_max_person to set
     */
    public void setRecruit_max_person(int recruit_max_person) {
        this.recruit_max_person = recruit_max_person;
    }

    /**
     * @return the deaddate
     */
    public int getDeaddate() {
        return deaddate;
    }

    /**
     * @param deaddate the deaddate to set
     */
    public void setDeaddate(int deaddate) {
        this.deaddate = deaddate;
    }

    /**
     * @return the deadtime
     */
    public int getDeadtime() {
        return deadtime;
    }

    /**
     * @param deadtime the deadtime to set
     */
    public void setDeadtime(int deadtime) {
        this.deadtime = deadtime;
    }
    
    
}
