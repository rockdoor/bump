package maven.springboot.RakurakuSansyo.Service.dto;

//import maven.springboot.RakurakuSansyo.Service.dto.*;
public class RakurakuSansyoInDto {

    //InDto内には、party_idのみ
    private int party_id;
    private int user_id;
    private int payment;

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the user_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the payment
     */
    public int getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(int payment) {
        this.payment = payment;
    }
}
