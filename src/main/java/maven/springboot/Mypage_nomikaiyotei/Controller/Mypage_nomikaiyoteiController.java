package maven.springboot.Mypage_nomikaiyotei.Controller;

import maven.springboot.Mypage_nomikaiyotei.service.Mypage_nomikaiyoteiService;
import maven.springboot.Mypage_nomikaiyotei.service.dto.Mypage_nomikaiyoteiInDto;
import maven.springboot.Mypage_nomikaiyotei.service.dto.Mypage_nomikaiyoteiWebInDto;
import maven.springboot.Mypage_nomikaiyotei.service.dto.Mypage_nomikaiyoteiWebOutDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class Mypage_nomikaiyoteiController {
    
    @Autowired   // DIコンテナからインスタンスを取得する
    private Mypage_nomikaiyoteiService mypage_nomikaiyoteiinservice;

    @RequestMapping(value = "/mypage_nomikaiyotei", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Mypage_nomikaiyoteiWebOutDto mypage_nomikaiyotei(@RequestBody Mypage_nomikaiyoteiWebInDto mypage_nomikaiyoteiWebInDto) {
        int user_id = mypage_nomikaiyoteiWebInDto.getUser_id();

        //Tyo_partyKakuninInDTO.classにReqparamに入れる処理
        Mypage_nomikaiyoteiInDto mypage_nomikaiyoteiindto = new Mypage_nomikaiyoteiInDto();
        mypage_nomikaiyoteiindto.setUser_id(user_id);

        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
        Mypage_nomikaiyoteiWebOutDto mypage_nomikaiyoteiWebOutDto = mypage_nomikaiyoteiinservice.nomikaiyotei(mypage_nomikaiyoteiindto);

        return mypage_nomikaiyoteiWebOutDto;
    }
}
