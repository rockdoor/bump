package maven.springboot.RakurakuSeisan.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import maven.springboot.RakurakuSeisan.service.dto.RakurakuSeisanInDto;
import maven.springboot.RakurakuSeisan.service.dto.RakurakuSeisanOutDto;
import maven.springboot.RakurakuSeisan.service.dto.RakurakuSeisanWebDto;
import maven.springboot.RakurakuSeisan.service.RakurakuSeisanService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class RakurakuSeisanController {

    //サービスロジック層へ接続
    @Autowired
    private RakurakuSeisanService rakurakuseisanservice;

    //◆◆Mapping - 画面描写◆◆
    @RequestMapping(value = "/settingrakuraku", method = RequestMethod.POST)
    @ResponseBody//JSON形式で返す

    public RakurakuSeisanOutDto requestRakurakuSeisan(@RequestBody RakurakuSeisanWebDto rs_webdto) {
        int requser_id = rs_webdto.getUser_id();
        //RakurakuSeisanInDTO.classにReqparamに入れる処理
        RakurakuSeisanInDto rs_indto = new RakurakuSeisanInDto();
        rs_indto.setUser_id(requser_id);

        //RakurakuSeisanServiceにDtoを渡し、結果をもらう;
        RakurakuSeisanOutDto rs_outdto = rakurakuseisanservice.setting_rakuraku(rs_indto);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(rs_outdto));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuSeisanController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs_outdto;
    }

    //◆◆Mapping - 割り勘◆◆
    @RequestMapping(value = "/warikan", method = RequestMethod.POST)
    @ResponseBody//JSON形式で返す

    public RakurakuSeisanOutDto requestRakurakuWarikan(@RequestBody RakurakuSeisanWebDto rs_webdto) {
        int requser_id = rs_webdto.getUser_id();
        int reqparty_id = rs_webdto.getParty_id();
        int reqcount = rs_webdto.getCount();
        int reqpayment = rs_webdto.getPayment();
        List reqpaylist = rs_webdto.getPaylist();

        //RakurakuSeisanInDTO.classにReqparamに入れる処理
        RakurakuSeisanInDto rs_indto = new RakurakuSeisanInDto();
        rs_indto.setUser_id(requser_id);
        rs_indto.setParty_id(reqparty_id);
        rs_indto.setCount(reqcount);
        rs_indto.setPayment(reqpayment);
        rs_indto.setPaylist(reqpaylist);

        //RakurakuSeisanServiceにDtoを渡し、結果をもらう;
        RakurakuSeisanOutDto rs_outdto = rakurakuseisanservice.warikan_rakuraku(rs_indto);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(rs_outdto));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuSeisanController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs_outdto;
    }
    
//        //◆◆Mapping - 割り勘◆◆
//    @RequestMapping(value = "/bynyushayear", method = RequestMethod.POST)
//    @ResponseBody//JSON形式で返す
//
//    public RakurakuSeisanOutDto requestRakurakuByNyushaYear(@RequestBody RakurakuSeisanWebDto rs_webdto) {
//        int reqnenji1 = rs_webdto.getNenji1();
//        int reqparty_id = rs_webdto.getParty_id();
//        int reqcount = rs_webdto.getCount();
//        int reqpayment = rs_webdto.getPayment();
//        List reqpaylist = rs_webdto.getPaylist();
//
//        //RakurakuSeisanInDTO.classにReqparamに入れる処理
//        RakurakuSeisanInDto rs_indto = new RakurakuSeisanInDto();
//        rs_indto.setUser_id(requser_id);
//        rs_indto.setParty_id(reqparty_id);
//        rs_indto.setCount(reqcount);
//        rs_indto.setPayment(reqpayment);
//        rs_indto.setPaylist(reqpaylist);
//
//        //RakurakuSeisanServiceにDtoを渡し、結果をもらう;
//        RakurakuSeisanOutDto rs_outdto = rakurakuseisanservice.warikan_rakuraku(rs_indto);
//
//        try {
//            //確認
//            System.out.println(new ObjectMapper().writeValueAsString(rs_outdto));
//        } catch (JsonProcessingException ex) {
//            Logger.getLogger(RakurakuSeisanController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return rs_outdto;
//    }

//    //◆◆Mapping - 年次別◆◆
//    @RequestMapping(value = "/byage", method = RequestMethod.POST)
//    @ResponseBody//JSON形式で返す
//
//    public RakurakuSeisanOutDto requestRakurakuByage(@RequestBody RakurakuSeisanWebDto rs_webdto) {
//        int requser_id = rs_webdto.getUser_id();
//
//        //RakurakuSeisanInDTO.classにReqparamに入れる処理
//        RakurakuSeisanInDto rs_indto = new RakurakuSeisanInDto();
//        rs_indto.setUser_id(requser_id);
//
//        //RakurakuSeisanServiceにDtoを渡し、結果をもらう;
//        RakurakuSeisanOutDto rs_outdto = rakurakuseisanservice.byage_rakuraku(rs_indto);
//
//        try {
//            //確認
//            System.out.println(new ObjectMapper().writeValueAsString(rs_outdto));
//        } catch (JsonProcessingException ex) {
//            Logger.getLogger(RakurakuSeisanController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return rs_outdto;
//    }
//
//    
//        //◆◆Mapping - 個別◆◆
//    @RequestMapping(value = "/separate", method = RequestMethod.POST)
//    @ResponseBody//JSON形式で返す
//
//    public RakurakuSeisanOutDto requestRakurakuSeparate(@RequestBody RakurakuSeisanWebDto rs_webdto) {
//        int requser_id = rs_webdto.getUser_id();
//
//        //RakurakuSeisanInDTO.classにReqparamに入れる処理
//        RakurakuSeisanInDto rs_indto = new RakurakuSeisanInDto();
//        rs_indto.setParty_id(requser_id);
//
//        //RakurakuSeisanServiceにDtoを渡し、結果をもらう;
//        RakurakuSeisanOutDto rs_outdto = rakurakuseisanservice.byage_rakuraku(rs_indto);
//
//        try {
//            //確認
//            System.out.println(new ObjectMapper().writeValueAsString(rs_outdto));
//        } catch (JsonProcessingException ex) {
//            Logger.getLogger(RakurakuSeisanController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return rs_outdto;
//    }
//
//    
    
}
