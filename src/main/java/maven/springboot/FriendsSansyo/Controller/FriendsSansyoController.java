package maven.springboot.FriendsSansyo.Controller;

import maven.springboot.FriendsSansyo.service.FriendsSansyoService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoInDto;
import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoOutDto;
import maven.springboot.FriendsSansyo.service.dto.FriendsSansyoWebDto;

import org.springframework.web.bind.annotation.RequestBody;



@Controller
public class FriendsSansyoController {

    //サービスロジック層へ接続
    @Autowired   // DIコンテナからインスタンスを取得する
    private FriendsSansyoService friendsSansyoservice;
  
   
    @RequestMapping(value = "/friendsSansyo", method = RequestMethod.POST)  
    @ResponseBody  //戻り値をレスポンス(JSON)として返す。
      	public FriendsSansyoWebDto RequestSansyo(@RequestBody FriendsSansyoWebDto fswd) throws Exception{
            
            //RequestBodyでJsonをWebDtoの形で受け取り、以下requser_idに戻す。
             int requser_id = fswd.getUser_id();
            
                ///InDTOに上記でうけとったuser_idを入れる処理
                FriendsSansyoInDto fsid = new FriendsSansyoInDto();
                fsid.setUser_id(requser_id);
                  
	         //FriendsSansyoServiceにDtoを渡し、OutDtoに結果を入れる処理
	         fswd = friendsSansyoservice.FriendsSansyo(fsid);
 
                 //確認
           System.out.println("***********************************");
           System.out.println("成功:0 失敗:1");
           System.out.println("flag=" + fswd.getEflag());
           System.out.println("***********************************"); 

 	return fswd;
 }
}

