package maven.springboot.Party_Tyo_partyKakunin.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Party_Tyo_partyKakunin.service.Party_Tyo_partyKakuninService;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninInDto;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninWebInDto;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninWebOutDto;
import maven.springboot.RakurakuSansyo.Controller.RakurakuController;
import maven.springboot.RakurakuSansyo.Service.RakurakuSansyoService;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoInDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoWebDto;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class Party_Tyo_partyKakuninController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private Party_Tyo_partyKakuninService party_tyo_partykakuninservice;

    @RequestMapping(value = "/party_tyo_partykakunin", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Party_Tyo_partyKakuninWebOutDto requestTyoPartyKakunin(@RequestBody Party_Tyo_partyKakuninWebInDto tkwd) {
        int url_id = tkwd.getUrl_id();

        //InDTOにReqparamに入れる処理
        Party_Tyo_partyKakuninInDto party_tyo_partykakuninindto = new Party_Tyo_partyKakuninInDto();
        party_tyo_partykakuninindto.setUrl_id(url_id);

        Party_Tyo_partyKakuninWebOutDto party_tyo_partyKakuninWebOutDto = new Party_Tyo_partyKakuninWebOutDto();

        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
        party_tyo_partyKakuninWebOutDto = party_tyo_partykakuninservice.tyoPartyHyouji(party_tyo_partykakuninindto);

        return party_tyo_partyKakuninWebOutDto;
    }

    @RequestMapping(value = "/Tsuti", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public RakurakuSansyoWebDto requestTsuti(@RequestBody RakurakuSansyoInDto jswd) {
        int reqparty_id = jswd.getParty_id();
        System.out.println(reqparty_id);
        RakurakuSansyoInDto rakurakusansyoindto = new RakurakuSansyoInDto();
        rakurakusansyoindto.setParty_id(reqparty_id);
        //RakurkuaSansyoServiceにDtoを渡し、結果をもらう;
        RakurakuSansyoWebDto jsd = party_tyo_partykakuninservice.mailTsuti(rakurakusansyoindto);
        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(jsd));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsd;
    }

    @RequestMapping(value = "/seisanupdate", method = RequestMethod.POST)
    @ResponseBody//JSON形式
    public RakurakuSansyoWebDto requestSeisanUpdate(@RequestBody RakurakuSansyoInDto ktwd) {
        //jsonで受け取った値をJyohoSyosaiWebDtoに渡して、以下で各値に戻す。
        int reqpayment = ktwd.getPayment();
        int requser_id = ktwd.getUser_id();
        int reqparty_id = ktwd.getParty_id();

        ///InDTOにユーザIDと金額を入れる処理
        RakurakuSansyoInDto jyohoSyosaiInDto = new RakurakuSansyoInDto();
        jyohoSyosaiInDto.setPayment(reqpayment);
        jyohoSyosaiInDto.setUser_id(requser_id);
        jyohoSyosaiInDto.setParty_id(reqparty_id);

        RakurakuSansyoWebDto jyohoSyosaiOutDto = party_tyo_partykakuninservice.seisanUpdate(jyohoSyosaiInDto);

        return jyohoSyosaiOutDto;

    }

}

//    }
//    @RequestMapping(value = "/tyo_membaerkakunin", method = RequestMethod.POST)
//    @ResponseBody //戻り値をレスポンス(JSON)として返す。
//    public Tyo_partyKakuninOutDto requestTyoMember(@RequestBody Tyo_partyMemberWebInDto tmwd) {
//        int reqparty_id = tmwd.getParty_id();
//        //Tyo_partyKakuninInDTO.classにReqparamに入れる処理
//        Tyo_partyMemberInDto tyo_partymemberindto = new Tyo_partyMemberInDto();
//        tyo_partymemberindto.setParty_id(reqparty_id);
//
//        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
//        Tyo_partyMemberOutDto tmod = tyo_partykakuninservice.tyoMemberHyouji(tyo_partymemberindto);
//
//        try {
//            //確認
//            System.out.println(new ObjectMapper().writeValueAsString(tkod));
//        } catch (JsonProcessingException ex) {
//            Logger.getLogger(Tyo_partyKakuninController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return tkod;
//}
