package maven.springboot.KininaruToroku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import maven.springboot.Entity.M_LikesEntity;
import maven.springboot.KininaruToroku.service.dto.KininaruSettingWebOutDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTagInDto;
import maven.springboot.Repository.M_LikesRepository;

@Service  // DIする対象の目印
public class KininaruTagService {

    @Autowired
    private M_LikesRepository m_LikesRepository;

    @Autowired
    private KininaruSettingService kininarusettingservice;

    public KininaruSettingWebOutDto addTag(KininaruTagInDto tagid) {

        KininaruSettingWebOutDto kswod = new KininaruSettingWebOutDto();

        // select文を発行し、要素数を代入。同じ名前の項目は作れないようにする。
        int count = m_LikesRepository.findByLike_name(tagid.getAdd_like_name()).size();

        if (count == 0) {
            //Entityをインスタンス化し、値を入れる
            M_LikesEntity m_LikesEntity = new M_LikesEntity();
            m_LikesEntity.setLike_first_cd(tagid.getAdd_like_first_cd());
            m_LikesEntity.setLike_second_cd(tagid.getAdd_like_second_cd());
            m_LikesEntity.setLike_name(tagid.getAdd_like_name());

            //like_noを取得する。大区分＋中区分でマスタを回して取得した件数＋1＝like_no
            int add_like_no = m_LikesRepository.findByCd(tagid.getAdd_like_first_cd(), tagid.getAdd_like_second_cd()).size();
            m_LikesEntity.setLike_no(add_like_no);

            m_LikesRepository.save(m_LikesEntity);
        }
        //更新したマスタを出力。
        kswod = kininarusettingservice.kininaruSetting();
        return kswod;
    }
}
