package maven.springboot.JyohoSyosai.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_UsersEntity;
import java.util.*;

import maven.springboot.JyohoSyosai.service.dto.JyohoSyosaiInDto;
import maven.springboot.JyohoSyosai.service.dto.JyohoSyosaiOutDto;
import maven.springboot.Repository.M_DepartmentsRepository;

import maven.springboot.Entity.M_DepartmentsEntity;
import maven.springboot.Repository.T_UsersRepository;

@Service  // DIする対象の目印
public class JyohoSyosaiService {

    @Autowired
    private T_UsersRepository t_UsersRepository;

    @Autowired
    private M_DepartmentsRepository m_DepartmentsRepository;

    public JyohoSyosaiOutDto syosaiHyoji(JyohoSyosaiInDto jyohosyosaiindto) {

        JyohoSyosaiOutDto jyohosyosaioutdto = new JyohoSyosaiOutDto();

        // select文を発行し、要素数を代入
        int count = t_UsersRepository.findByUser_id(jyohosyosaiindto.getUser_id()).size();

        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
        } else {
            List<T_UsersEntity> syosaiList = t_UsersRepository.findByUser_id(jyohosyosaiindto.getUser_id());
            String email = syosaiList.get(0).getEmail();
            String name = syosaiList.get(0).getName();
            int joinedyear = syosaiList.get(0).getJoinedyear();

            // 部署idから部署名を取得
            int department_id = syosaiList.get(0).getDepartment_id();
            List<M_DepartmentsEntity> list = m_DepartmentsRepository.findByDepartment_id(department_id);

            String depertment_name = list.get(0).getDepartment_name();

            // DTOに値をセット
            jyohosyosaioutdto.setName(name);
            jyohosyosaioutdto.setDepartment_name(depertment_name);
            jyohosyosaioutdto.setEmail(email);
            jyohosyosaioutdto.setJoinedyear(joinedyear);
            jyohosyosaioutdto.setDepartment_id(department_id);

        }
        return jyohosyosaioutdto;
    }

    public JyohoSyosaiOutDto kihonUpdate(JyohoSyosaiInDto jyohoSyosaiInDto) {

        JyohoSyosaiOutDto jyohoSyosaiOutDto = new JyohoSyosaiOutDto();

        //user_idから検索し、nameを取り出す
        //基本情報がまだ登録されていなかったらnullが格納されている
        List<T_UsersEntity> userentity = t_UsersRepository.findByUser_id(jyohoSyosaiInDto.getUser_id());
        String name = userentity.get(0).getName();

        String user_id = Integer.toString(jyohoSyosaiInDto.getUser_id());
        int count2 = t_UsersRepository.findByName(user_id).size();

        if (name == null && count2 == 0) {
            System.out.println("★★★★★★該当レコードがゼロです★★★★★★");
        } else {

            //entityにupdateする値をセット
            T_UsersEntity entity = userentity.get(0);
            entity.setName(jyohoSyosaiInDto.getName());
            entity.setDepartment_id(jyohoSyosaiInDto.getDepartment_id());
            entity.setJoinedyear(jyohoSyosaiInDto.getJoinedyear());
            entity.setUser_id(jyohoSyosaiInDto.getUser_id());
            entity.setEmail(jyohoSyosaiInDto.getEmail());

            //Update文の発行
            this.t_UsersRepository.save(entity);
            
            int department_id = userentity.get(0).getDepartment_id();
            List<M_DepartmentsEntity> list = m_DepartmentsRepository.findByDepartment_id(department_id);
            String depertment_name = list.get(0).getDepartment_name();
            
            jyohoSyosaiOutDto.setName(jyohoSyosaiInDto.getName());
            jyohoSyosaiOutDto.setDepartment_name(depertment_name);
            jyohoSyosaiOutDto.setEmail(jyohoSyosaiInDto.getEmail());
            jyohoSyosaiOutDto.setJoinedyear(jyohoSyosaiInDto.getJoinedyear());
            jyohoSyosaiOutDto.setDepartment_id(jyohoSyosaiInDto.getDepartment_id());
            
        }
        return jyohoSyosaiOutDto;
    }

}
