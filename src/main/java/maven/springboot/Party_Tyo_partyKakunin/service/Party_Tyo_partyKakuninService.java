package maven.springboot.Party_Tyo_partyKakunin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninInDto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninOutDto;

import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninInDto;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninOutDto;
import maven.springboot.Party_Tyo_partyKakunin.service.dto.Party_Tyo_partyKakuninWebOutDto;
import maven.springboot.RakurakuSansyo.Service.RakurakuSansyoService;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoInDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoWebDto;
import maven.springboot.Repository.T_PartiesRepository;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_parties_users_Dto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninWebOutDto;
import maven.springboot.Logic.Mailpost;
import maven.springboot.Logic.rakurakuMail;

@Service  // DIする対象の目印
public class Party_Tyo_partyKakuninService {

    @Autowired
    private T_UsersRepository t_usersRepository;

    @Autowired
    private T_PartiesRepository t_partiesRepository;

    @Autowired
    private T_Parties_UsersRepository t_parties_usersRepository;
    
    @Autowired
    private rakurakuMail rakurakumail;

    public Party_Tyo_partyKakuninWebOutDto tyoPartyHyouji(Party_Tyo_partyKakuninInDto party_tyo_partykakuninindto) {

        Party_Tyo_partyKakuninWebOutDto party_tyo_partyKakuninWebOutDto = new Party_Tyo_partyKakuninWebOutDto();

        int url_id = party_tyo_partykakuninindto.getUrl_id();

        List<T_PartiesEntity> list = t_partiesRepository.findByUrl_id(url_id);
        party_tyo_partyKakuninWebOutDto.setT_PartiesEntitylist(list);

        //url_idに対応するparty_idを取り出し
        int party_id = list.get(0).getParty_id();

        int count = list.size();

        if (count == 0) {
            System.out.println("★★★★★対象の飲み会は登録されていません★★★★");
        } else {
            //party_idを元に、出席者の出欠状況の一覧を持ってくる
            List<T_Parties_UsersEntity> userlist = new ArrayList();

            //party_idに対応するリストをテーブルから持ってくる
            userlist = t_parties_usersRepository.findByParty_id(party_id);
            party_tyo_partyKakuninWebOutDto.setT_Parties_UsersEntitylist(userlist);
        }

        return party_tyo_partyKakuninWebOutDto;
    }
    public RakurakuSansyoWebDto mailTsuti(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したparty_idが登録されてるuser_idの持ち主の情報list
        List<T_Parties_UsersEntity> t_parties_users_List = t_parties_usersRepository.findByParty_id(rakurakusansyoindto.getParty_id());

        int count = t_parties_users_List.size();

        List<T_PartiesEntity> t_parties_List = t_partiesRepository.findByParty_id(rakurakusansyoindto.getParty_id());
        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
        } else {
            List<T_UsersEntity> t_users_List = new ArrayList();
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
                t_users_List.addAll(t_usersRepository.findByUser_id(t_Parties_UsersEntity.getUser_id()));
            }
            try {
                rakurakumail.Mailsend(rakurakusansyoindto.getParty_id());
            } catch (Exception ex) {
                Logger.getLogger(RakurakuSansyoService.class.getName()).log(Level.SEVERE, null, ex);
            }
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
            rakurakusansyowebdto.setT_Users_List(t_users_List);
            rakurakusansyowebdto.setT_Parties_List(t_parties_List);
        }
        return rakurakusansyowebdto;
    }

    public RakurakuSansyoWebDto seisanUpdate(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したparty_idが登録されてるuser_idの持ち主の情報list
        int count = t_parties_usersRepository.findByUser_id(rakurakusansyoindto.getUser_id()).size();
        List<T_Parties_UsersEntity> t_parties_users_List = t_parties_usersRepository.findByUser_idAndParty_id(rakurakusansyoindto.getUser_id(),rakurakusansyoindto.getParty_id());
        if (count == 0) {
            System.out.println("************************Listがnullです*****************************");
        } else {
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
               t_Parties_UsersEntity.setPayment(rakurakusansyoindto.getPayment());
                //Update文の発行
                this.t_parties_usersRepository.save(t_Parties_UsersEntity);
            }
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
        }
        return rakurakusansyowebdto;
    }
}
