package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "m_departments")
public class M_DepartmentsEntity {

        @Id
        @GeneratedValue
        private Integer department_id;

        //ニックネームの列を宣言
        @Column(name = "department_name")
        private String department_name;
        
    /**
     * @return the department_id
     */
    public Integer getDepartment_id() {
        return department_id;
    }

    /**
     * @param department_id the department_id to set
     */
    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    /**
     * @return the department_name
     */
    public String getDepartment_name() {
        return department_name;
    }

    /**
     * @param department_name the department_name to set
     */
    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
    
}
