package maven.springboot.Theme_partyToroku.Controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.RequestBody;

import maven.springboot.Theme_partyToroku.service.Theme_partyTorokuService;
import maven.springboot.Theme_partyToroku.service.dto.Theme_partyTorokuInDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_partyTorokuWebInDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_partyTorokuWebOutDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_sansyoWebInDto;
import maven.springboot.Theme_partyToroku.service.dto.Theme_sansyoWebOutDto;

@Controller
public class Theme_partyTorokuController {

    //サービスロジック層へ接続
    @Autowired   // DIコンテナからインスタンスを取得する
    private Theme_partyTorokuService theme_partytorokuservice;

    @RequestMapping(value = "/theme_sansyo", method = RequestMethod.POST)
    @ResponseBody
    public Theme_sansyoWebOutDto themesansyo(@RequestBody Theme_sansyoWebInDto theme_sansyoWebInDto) throws Exception {

        //フロント側に返すDTO
        Theme_sansyoWebOutDto theme_sansyoWebOutDto = new Theme_sansyoWebOutDto();
        theme_sansyoWebOutDto = theme_partytorokuservice.Themesansyo(theme_sansyoWebInDto);
        
        return theme_sansyoWebOutDto;
    }

    @RequestMapping(value = "/theme_Partytoroku", method = RequestMethod.POST)
    @ResponseBody
    public Theme_partyTorokuWebOutDto Requestparty(@RequestBody Theme_partyTorokuWebInDto theme_partyTorokuWebInDto) throws Exception {

        //フロント側から受け取ったWebInDtoの値をそのままServiceInDtoに格納する
        Theme_partyTorokuInDto theme_partyTorokuInDto = new Theme_partyTorokuInDto();

        //格納
        theme_partyTorokuInDto.setArtifact_id(theme_partyTorokuWebInDto.getArtifact_id());
        theme_partyTorokuInDto.setTheme_area_id(theme_partyTorokuWebInDto.getTheme_area_id());
        theme_partyTorokuInDto.setTheme_genre_id(theme_partyTorokuWebInDto.getTheme_genre_id());
        theme_partyTorokuInDto.setHolding_max_person(theme_partyTorokuWebInDto.getHolding_max_person());
        theme_partyTorokuInDto.setRecruit_max_person(theme_partyTorokuWebInDto.getRecruit_max_person());
        theme_partyTorokuInDto.setDeaddate(theme_partyTorokuWebInDto.getDeaddate());
        theme_partyTorokuInDto.setDeadtime(theme_partyTorokuWebInDto.getDeadtime());
        theme_partyTorokuInDto.setParty_name(theme_partyTorokuWebInDto.getParty_name());
        theme_partyTorokuInDto.setUser_id(theme_partyTorokuWebInDto.getUser_id());
        theme_partyTorokuInDto.setDate(theme_partyTorokuWebInDto.getDate());
        theme_partyTorokuInDto.setTime(theme_partyTorokuWebInDto.getTime());

        //フロント側に返す用のWebOutDto
        Theme_partyTorokuWebOutDto theme_partyTorokuOutDto = new Theme_partyTorokuWebOutDto();

        //Serviceを呼び出し、結果をWebOutDtoに格納
        theme_partyTorokuOutDto = theme_partytorokuservice.PartyToroku(theme_partyTorokuInDto);
        
        return theme_partyTorokuOutDto;
    }
}
