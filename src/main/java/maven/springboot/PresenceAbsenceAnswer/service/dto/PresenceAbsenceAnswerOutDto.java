/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.PresenceAbsenceAnswer.service.dto;

/**
 *
 * @author t4-yoshioka
 */
public class PresenceAbsenceAnswerOutDto {
    
    //OutDto内には、飲み会Id（party_id）、飲み会名（今はなし）、出欠Id（attend_cd）入っている
    private int party_id;
    private int attend_cd;
    
    

    /**
     * @return the attend_cd
     */
    public int getAttend_cd() {
        return attend_cd;
    }

    /**
     * @param attend_cd the attend_cd to set
     */
    public void setAttend_cd(int attend_cd) {
        this.attend_cd = attend_cd;
    }

    /**
     * @return the party_id
     */
    public int getParty_id() {
        return party_id;
    }

    /**
     * @param party_id the party_id to set
     */
    public void setParty_id(int party_id) {
        this.party_id = party_id;
    }
    
    
    
}
