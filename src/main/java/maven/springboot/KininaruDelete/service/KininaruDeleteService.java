package maven.springboot.KininaruDelete.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import maven.springboot.Entity.M_LikesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import maven.springboot.Entity.T_Users_LikesEntity;
import maven.springboot.KininaruDelete.service.dto.KininaruDeleteInDto;
import maven.springboot.KininaruDelete.service.dto.KininaruDeleteOutDto;
import maven.springboot.KininaruDelete.service.dto.KininaruDeleteWebOutDto;
import maven.springboot.Repository.T_Users_LikesRepository;


@Service  // DIする対象の目印
public class KininaruDeleteService {

    @Autowired
    private T_Users_LikesRepository t_Users_LikesRepository;

    public KininaruDeleteWebOutDto kininaruDelete(KininaruDeleteInDto kdid) {

        int dflag = 0; //エラーフラグ

        KininaruDeleteWebOutDto kdwod = new KininaruDeleteWebOutDto();

        //対象ユーザーのデータの存在チェック
        List<T_Users_LikesEntity> users_likes_list = t_Users_LikesRepository.findByUser_id(kdid.getUser_id());
        int count = users_likes_list.size();
        
        if (count == 0) {
            System.out.println("★対象ユーザーが気になる登録していません★");
            dflag = 1; //エラーフラグをオンにする
        } else {
            //ハッシュマップを宣言
            HashMap<String, String> map = kdid.getDelete_map();
            
//            //現在のやり方(拡張構文)
//            List<String> list = new ArrayList<>();
//            for (String string : list) {
//            }
//            //昔のやり方(無駄な処理が多い)
//            int n = list.size();
//            for (int i = 0; i < n; i++) {
//                list.get(i);//毎回参照するリストをゲットしてくる必要がある
//            }
//            //昔のやり方(イテレーターを使用する)
//            //※イテレーターは指定されたものを順々に参照していく箱のようなもの
//            Iterator<String> ite = list.iterator();//リストをイテレーターに入れる
//            while (ite.hasNext()) {//Nextを指定してやることで次々に指定をしてくれる
//                ite.next();
//            }
            
            T_Users_LikesEntity t_Users_LikesEntity = new T_Users_LikesEntity();

            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getValue().equals("true")) {
                    //チェックボックスにチェックの付いたものをリストに格納
                    t_Users_LikesEntity.setUser_id(kdid.getUser_id());
                    t_Users_LikesEntity.setArtifact_id(Integer.parseInt(entry.getKey()));
                    t_Users_LikesRepository.delete(t_Users_LikesEntity);        
                }
            }
            dflag = 0;
        }
        kdwod.setDflag(dflag);
        return kdwod;
    }
}
