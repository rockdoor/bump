package maven.springboot.Tyo_partyKakunin.service.dto;

import java.util.List;

public class Tyo_partyKakuninWebOutDto {

    private List<Tyo_partyKakuninOutDto> tyo_partyKakuninOutDto;

    /**
     * @return the tyo_partyKakuninOutDto
     */
    public List<Tyo_partyKakuninOutDto> getTyo_partyKakuninOutDto() {
        return tyo_partyKakuninOutDto;
    }

    /**
     * @param tyo_partyKakuninOutDto the tyo_partyKakuninOutDto to set
     */
    public void setTyo_partyKakuninOutDto(List<Tyo_partyKakuninOutDto> tyo_partyKakuninOutDto) {
        this.tyo_partyKakuninOutDto = tyo_partyKakuninOutDto;
    }

}
