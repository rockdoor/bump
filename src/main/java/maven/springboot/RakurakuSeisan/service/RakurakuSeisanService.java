package maven.springboot.RakurakuSeisan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

import maven.springboot.RakurakuSeisan.service.dto.RakurakuSeisanInDto;
import maven.springboot.RakurakuSeisan.service.dto.RakurakuSeisanOutDto;
import maven.springboot.Repository.T_PartiesRepository;

import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Entity.T_UsersEntity;

@Service  // DIする対象の目印
public class RakurakuSeisanService {

    @Autowired
    private T_Parties_UsersRepository t_Parties_UsersRepository;

    @Autowired
    private T_PartiesRepository t_PartiesRepository;

    @Autowired
    private T_UsersRepository t_UsersRepository;

    public RakurakuSeisanOutDto setting_rakuraku(RakurakuSeisanInDto rs_indto) {

        RakurakuSeisanOutDto rs_outdto = new RakurakuSeisanOutDto();

        //error原因格納用変数
        int eflag;
        // T_PartiesRepositoryでqueryを発行しuser_idがホストの飲み会情報を持ってくる。
        List<T_PartiesEntity> rs_party_list = t_PartiesRepository.findByUser_id(rs_indto.getUser_id());
        int cnt = rs_party_list.size();
        if (cnt == 0) {
            System.out.println("◆◆該当のパーティーは登録されていません◆◆");
            eflag = 1;
        } else {
            List<T_Parties_UsersEntity> t_parties_userslist;
            t_parties_userslist = new ArrayList<>();
            List<T_UsersEntity> t_userslist;
            t_userslist = new ArrayList<>();
            for (T_PartiesEntity t_PartiesEntity : rs_party_list) {
                t_parties_userslist.addAll(t_Parties_UsersRepository.findByParty_id(t_PartiesEntity.getParty_id()));
                for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_userslist) {
                    t_userslist.addAll(t_UsersRepository.findByUser_id(t_Parties_UsersEntity.getUser_id()));
                }
                rs_outdto.setT_userslist(t_userslist);
            }
            rs_outdto.setT_parties_userslist(t_parties_userslist);
            rs_outdto.setT_partieslist(rs_party_list);
        }
        return rs_outdto;
    }

    public RakurakuSeisanOutDto warikan_rakuraku(RakurakuSeisanInDto rs_indto) {
        RakurakuSeisanOutDto rs_outdto = new RakurakuSeisanOutDto();
        //エラーフラグをセット
        int eflag;
        int warikan_result;
        int payment = rs_indto.getPayment();
        int count = rs_indto.getCount();
        int party_id = rs_indto.getParty_id();
        List paylist = rs_indto.getPaylist();

        if (party_id == 0) {
            eflag = 1;
            rs_outdto.setEflag(eflag);
        } else {
            //計算してOutDtoに仕込む
            double keisan = Math.ceil(payment / count / 100.0);
            warikan_result = (int) keisan * 100;
            rs_outdto.setWarikan_result(warikan_result);
            rs_outdto.setParty_id(party_id);
            //Entityにwarikan_resultをセットしてアップデート
            List<T_Parties_UsersEntity> t_parties_userslist = t_Parties_UsersRepository.findByParty_id(party_id);
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_userslist) {
                Object idforcheck = t_Parties_UsersEntity.getUser_id();
                for (Object webpay : paylist) {
                    if (webpay == idforcheck) {
                        t_Parties_UsersEntity.setPayment(warikan_result);
                    }
                }
                //Update文の発行
                this.t_Parties_UsersRepository.save(t_Parties_UsersEntity);
            }
        }
        return rs_outdto;
    }

}
