package maven.springboot.RakurakuSansyo.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.RakurakuSansyo.Service.RakurakuSansyoService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoInDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoOutDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoWebDto;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class RakurakuController {

    /*★★★★mail_no(個人識別番号)に応じた各情報のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private RakurakuSansyoService rakurakusansyoservice;

    @RequestMapping(value = "/selectparty", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public RakurakuSansyoWebDto requestParty(@RequestBody RakurakuSansyoInDto jswd) {
        int requser_id = jswd.getUser_id();
        //JyohoSyosaiInDTO.classにReqparamに入れる処理
        RakurakuSansyoInDto rakurakusansyoindto = new RakurakuSansyoInDto();
        rakurakusansyoindto.setUser_id(requser_id);
        //RakurkuaSansyoServiceにDtoを渡し、結果をもらう;
        RakurakuSansyoWebDto jso = rakurakusansyoservice.Party_List(rakurakusansyoindto);
        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(jso));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jso;
    }

    @RequestMapping(value = "/Sansyo", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public RakurakuSansyoWebDto requestSyosai(@RequestBody RakurakuSansyoInDto jswd) {
        //int requser_id = jswd.getUser_id();
        int reqparty_id = jswd.getParty_id();
        //JyohoSyosaiInDTO.classにReqparamに入れる処理
        RakurakuSansyoInDto rakurakusansyoindto = new RakurakuSansyoInDto();
        //rakurakusansyoindto.setUser_id(requser_id);        
        rakurakusansyoindto.setParty_id(reqparty_id);
        System.out.println("----------------------");
        System.out.println(reqparty_id);
        System.out.println("----------------------");
        //RakurkuaSansyoServiceにDtoを渡し、結果をもらう;
        RakurakuSansyoWebDto jsod = rakurakusansyoservice.seisanHyoji(rakurakusansyoindto);
        System.out.println("----------------------");
        System.out.println(jsod);
        System.out.println("----------------------");
        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(jsod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsod;
    }

//    @RequestMapping(value = "/Tsuti", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public RakurakuSansyoWebDto requestTsuti(@RequestBody RakurakuSansyoInDto jswd) {
        int reqparty_id = jswd.getParty_id();
        System.out.println(reqparty_id);
        RakurakuSansyoInDto rakurakusansyoindto = new RakurakuSansyoInDto();
        rakurakusansyoindto.setParty_id(reqparty_id);
        //RakurkuaSansyoServiceにDtoを渡し、結果をもらう;
        RakurakuSansyoWebDto jsd = rakurakusansyoservice.mailTsuti(rakurakusansyoindto);
        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(jsd));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RakurakuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsd;
    }

//    @RequestMapping(value = "/seisanupdate", method = RequestMethod.POST)
    @ResponseBody//JSON形式
    public RakurakuSansyoWebDto requestSeisanUpdate(@RequestBody RakurakuSansyoInDto ktwd) {
        //jsonで受け取った値をJyohoSyosaiWebDtoに渡して、以下で各値に戻す。
        int reqpayment = ktwd.getPayment();
        int requser_id = ktwd.getUser_id();
        int reqparty_id = ktwd.getParty_id();

        ///InDTOにユーザIDと金額を入れる処理
        RakurakuSansyoInDto jyohoSyosaiInDto = new RakurakuSansyoInDto();
        jyohoSyosaiInDto.setPayment(reqpayment);
        jyohoSyosaiInDto.setUser_id(requser_id);
        jyohoSyosaiInDto.setParty_id(reqparty_id);

        RakurakuSansyoWebDto jyohoSyosaiOutDto = rakurakusansyoservice.seisanUpdate(jyohoSyosaiInDto);

        return jyohoSyosaiOutDto;

    }

}
