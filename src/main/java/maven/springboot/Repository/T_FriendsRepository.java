package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.T_FriendsEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_FriendsRepository extends JpaRepository<T_FriendsEntity, Integer> {

 @Query(value = "select t from T_FriendsEntity t where t.user_id = :user_id")
    public List<T_FriendsEntity> findByUser_id(@Param("user_id") int user_id);
    
 @Query(value = "select t from T_FriendsEntity t where t.user_id = :user_id and t.friends_id =:friends_id ")
    public List<T_FriendsEntity> findByMyfriend_id(@Param("user_id") int user_id, @Param("friends_id") int friends_id);   

 @Query(value = "select t from T_FriendsEntity t where t.user_id = :user_id and t.friends_id =:friends_id and t.flag = 1")
    public List<T_FriendsEntity> findTsunagari(@Param("user_id") int user_id, @Param("friends_id") int friends_id);   

}
