package maven.springboot.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.IdClass;

@Entity
@Table(name = "t_friends")
@IdClass(T_FriendsIdEntity.class)
public class T_FriendsEntity {

    //t_usersテーブルのuser_id（申請者）と紐付けたい！！
    @Id
    private Integer user_id;
    
    //t_usersテーブルのuser_id（友達）と紐付けたい！！
    @Id
    private Integer friends_id;

    @Column(name = "flag")
    private int flag;

    

    public int getUser_id() {
        return user_id;
    }
  
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
    public  int  getFriends_id() {
        return friends_id;
    }
  
    public void setFriends_id(int  friends_id) {
        this.friends_id = friends_id;
    }    
    
    public int getFlag() {
        return flag;
    }
  
    public void setFlag(int flag) {
        this.flag = flag;
    }
}