package maven.springboot.MatchingToroku.service.dto;

import maven.springboot.UserToroku.service.dto.*;

public class MatchingTorokuInDto {

    private int user_id;
    private int t_ok_ng;
    private int t_interact;
    private int t_area1;
    private int t_area2;
    private int t_area3;
    private int t_time;
    private int r_ok_ng;
    private int r_interact;
    private int r_area1;
    private int r_area2;
    private int r_area3;
    private int r_time;

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the t_ok_ng
     */
    public int getT_ok_ng() {
        return t_ok_ng;
    }

    /**
     * @param t_ok_ng the t_ok_ng to set
     */
    public void setT_ok_ng(int t_ok_ng) {
        this.t_ok_ng = t_ok_ng;
    }

    /**
     * @return the t_interact
     */
    public int getT_interact() {
        return t_interact;
    }

    /**
     * @param t_interact the t_interact to set
     */
    public void setT_interact(int t_interact) {
        this.t_interact = t_interact;
    }


    /**
     * @return the t_time
     */
    public int getT_time() {
        return t_time;
    }

    /**
     * @param t_time the t_time to set
     */
    public void setT_time(int t_time) {
        this.t_time = t_time;
    }

    /**
     * @return the r_ok_ng
     */
    public int getR_ok_ng() {
        return r_ok_ng;
    }

    /**
     * @param r_ok_ng the r_ok_ng to set
     */
    public void setR_ok_ng(int r_ok_ng) {
        this.r_ok_ng = r_ok_ng;
    }

    /**
     * @return the r_interact
     */
    public int getR_interact() {
        return r_interact;
    }

    /**
     * @param r_interact the r_interact to set
     */
    public void setR_interact(int r_interact) {
        this.r_interact = r_interact;
    }
    /**
     * @return the r_time
     */
    public int getR_time() {
        return r_time;
    }

    /**
     * @param r_time the r_time to set
     */
    public void setR_time(int r_time) {
        this.r_time = r_time;
    }

    /**
     * @return the t_area1
     */
    public int getT_area1() {
        return t_area1;
    }

    /**
     * @param t_area1 the t_area1 to set
     */
    public void setT_area1(int t_area1) {
        this.t_area1 = t_area1;
    }

    /**
     * @return the t_area2
     */
    public int getT_area2() {
        return t_area2;
    }

    /**
     * @param t_area2 the t_area2 to set
     */
    public void setT_area2(int t_area2) {
        this.t_area2 = t_area2;
    }

    /**
     * @return the t_area3
     */
    public int getT_area3() {
        return t_area3;
    }

    /**
     * @param t_area3 the t_area3 to set
     */
    public void setT_area3(int t_area3) {
        this.t_area3 = t_area3;
    }

    /**
     * @return the r_area1
     */
    public int getR_area1() {
        return r_area1;
    }

    /**
     * @param r_area1 the r_area1 to set
     */
    public void setR_area1(int r_area1) {
        this.r_area1 = r_area1;
    }

    /**
     * @return the r_area2
     */
    public int getR_area2() {
        return r_area2;
    }

    /**
     * @param r_area2 the r_area2 to set
     */
    public void setR_area2(int r_area2) {
        this.r_area2 = r_area2;
    }

    /**
     * @return the r_area3
     */
    public int getR_area3() {
        return r_area3;
    }

    /**
     * @param r_area3 the r_area3 to set
     */
    public void setR_area3(int r_area3) {
        this.r_area3 = r_area3;
    }
    
}
