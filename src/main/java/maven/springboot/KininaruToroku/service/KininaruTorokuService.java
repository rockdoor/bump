package maven.springboot.KininaruToroku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import static jdk.nashorn.internal.objects.NativeArray.pop;

import maven.springboot.Entity.T_Users_LikesEntity;
import maven.springboot.KininaruToroku.service.dto.KininaruTorokuInDto;
import maven.springboot.KininaruToroku.service.dto.KininaruTorokuWebOutDto;
import maven.springboot.Repository.M_LikesRepository;
import maven.springboot.Repository.T_TrendsRepository;
import maven.springboot.Repository.T_Users_LikesRepository;

@Service  // DIする対象の目印
public class KininaruTorokuService {

    @Autowired
    private T_Users_LikesRepository t_Users_LikesRepository;

    @Autowired
    private T_TrendsRepository t_TrendsRepository;

    @Autowired
    private M_LikesRepository m_LikesRepository;

    public KininaruTorokuWebOutDto kininaruToroku(KininaruTorokuInDto ktid) {

        KininaruTorokuWebOutDto ktwod = new KininaruTorokuWebOutDto();

        // select文を発行し、要素数を代入
//        int count = t_Users_LikesRepository.findByUser_id(ktid.getUser_id()).size();
        //user_id格納用変数
        int user_id;
        //Entityをインスタンス化し、値を入れる
        T_Users_LikesEntity t_Users_LikesEntity = new T_Users_LikesEntity();
        List<String> artifact_id_list = ktid.getArtifact_list();

        for (String artifact_id : artifact_id_list) {
            t_Users_LikesEntity.setUser_id(ktid.getUser_id());
            int reqid = Integer.parseInt(artifact_id);
            t_Users_LikesEntity.setArtifact_id(reqid);
            t_Users_LikesRepository.save(t_Users_LikesEntity);

        }
        // insert文の発行
        ktwod.setKininaruTorokuFlag(true);
        return ktwod;
    }

}
