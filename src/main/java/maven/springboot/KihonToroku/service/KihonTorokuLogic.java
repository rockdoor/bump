package maven.springboot.KihonToroku.service;

import maven.springboot.KihonToroku.Controller.KihonTorokuController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Repository.T_UsersRepository;
import java.util.*;
import maven.springboot.Entity.M_DepartmentsEntity;
import maven.springboot.Entity.T_UsersEntity;

import maven.springboot.KihonToroku.service.dto.KihonTorokuInDto;
import maven.springboot.KihonToroku.service.dto.KihonTorokuOutDto;

import maven.springboot.Repository.M_DepartmentsRepository;

@Service
public class KihonTorokuLogic {

    //DI層へ接続
    @Autowired
    private T_UsersRepository t_UsersRepository;

    //DI層へ接続
    @Autowired
    private M_DepartmentsRepository m_DepartmentsRepository;

    public KihonTorokuOutDto kihonTorokuHantei(KihonTorokuInDto kihonTorokuInDto) {

        KihonTorokuOutDto kihonTorokuOutDto = new KihonTorokuOutDto();

        //error原因格納用変数
        int eflag;

        //user_idから検索し、nameを取り出す
        //基本情報がまだ登録されていなかったらnullが格納されている
        List<T_UsersEntity> userentity = t_UsersRepository.findByUser_id(kihonTorokuInDto.getUser_id());
        String name = userentity.get(0).getName();

        //ニックネームが登録されてなかったら0が格納される

        //Listがint型を受け付けないため、Stringに変換
        //変更の必要有り。
        String user_id = Integer.toString(kihonTorokuInDto.getUser_id());
        int count2 = t_UsersRepository.findByName(user_id).size();

        if (name == null && count2 == 0) {
//            int department_id = ;
//            //部署マスタから部署idを取得してくる
//            List<M_DepartmentsEntity> list;
//            //Listがint型を受け付けないため、Stringに変換
//            //変更の必要有り。
//            String depertment_id = Integer.toString(kihonTorokuInDto.getDepartment_id());
//            list = m_DepartmentsRepository.findByDepartment_name(depertment_id);
//            //プルダウン名は部署マスタからそのまま持ってきているため、0件だった場合は考慮しない
//            department_id = list.get(0).getDepartment_id();
//
//            System.out.println(department_id);

            //entityにupdateする値をセット
            T_UsersEntity entity = userentity.get(0);
            entity.setName(kihonTorokuInDto.getName());
            entity.setDepartment_id(kihonTorokuInDto.getDepartment_id());
            entity.setJoinedyear(kihonTorokuInDto.getJoinedyear());
            entity.setBirth(kihonTorokuInDto.getBirth());
            
            //Update文の発行
            this.t_UsersRepository.save(entity);

            //Update文の発行
//            kihonTorokuRepository.setNameByUser_id(kihonTorokuInDto.getNickname(), kihonTorokuInDto.getMail_no());
//            kihonTorokuRepository.setDepartment_idByUser_id(department_id, kihonTorokuInDto.getMail_no());
//            kihonTorokuRepository.setJoinedyearByUser_id(kihonTorokuInDto.getNyushayear(), kihonTorokuInDto.getMail_no());
//            kihonTorokuRepository.setByUser_id(kihonTorokuInDto.getNickname(), department_id, kihonTorokuInDto.getNyushayear(),kihonTorokuInDto.getMail_no());
            kihonTorokuOutDto.setFlag(true);
            kihonTorokuOutDto.setUser_id(kihonTorokuInDto.getUser_id());

        } else {
            kihonTorokuOutDto.setFlag(false);

            //eflagが1だったらすでに基本情報を登録している
            if (name != null) {
                eflag = 1;
                kihonTorokuOutDto.setEflag(eflag);
            } //eflagが2だったらすでにニックネームが別の人に使われている
            else if (count2 > 0) {
                eflag = 2;
                kihonTorokuOutDto.setEflag(eflag);
            } else {
                eflag = 0;
                kihonTorokuOutDto.setEflag(eflag);
            }
        }
        return kihonTorokuOutDto;
    }

}
