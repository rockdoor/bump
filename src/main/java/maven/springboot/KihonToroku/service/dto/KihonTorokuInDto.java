package maven.springboot.KihonToroku.service.dto;


public class KihonTorokuInDto {

        //InDto内には、nickname,busho,nyushayearの３つ
        private String name;
        private int department_id;
        private int joinedyear;
        private int user_id;
        private int birth;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the department_id
     */
    public int getDepartment_id() {
        return department_id;
    }

    /**
     * @param department_id the department_id to set
     */
    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    /**
     * @return the joinedyear
     */
    public int getJoinedyear() {
        return joinedyear;
    }

    /**
     * @param joinedyear the joinedyear to set
     */
    public void setJoinedyear(int joinedyear) {
        this.joinedyear = joinedyear;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
        
    /**
     * @return the birth
     */
    public int getBirth() {
        return birth;
    }

    /**
     * @param birth the birth to set
     */
    public void setBirth(int birth) {
        this.birth = birth;
    }

    
}