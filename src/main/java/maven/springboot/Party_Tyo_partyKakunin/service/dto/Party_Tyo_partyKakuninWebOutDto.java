package maven.springboot.Party_Tyo_partyKakunin.service.dto;

import java.util.List;
import maven.springboot.Entity.T_PartiesEntity;
import maven.springboot.Entity.T_Parties_UsersEntity;

public class Party_Tyo_partyKakuninWebOutDto {

    private List<T_Parties_UsersEntity> t_Parties_UsersEntitylist;
    private List<T_PartiesEntity> t_PartiesEntitylist;

    /**
     * @return the t_Parties_UsersEntitylist
     */
    public List<T_Parties_UsersEntity> getT_Parties_UsersEntitylist() {
        return t_Parties_UsersEntitylist;
    }

    /**
     * @param t_Parties_UsersEntitylist the t_Parties_UsersEntitylist to set
     */
    public void setT_Parties_UsersEntitylist(List<T_Parties_UsersEntity> t_Parties_UsersEntitylist) {
        this.t_Parties_UsersEntitylist = t_Parties_UsersEntitylist;
    }

    /**
     * @return the t_PartiesEntitylist
     */
    public List<T_PartiesEntity> getT_PartiesEntitylist() {
        return t_PartiesEntitylist;
    }

    /**
     * @param t_PartiesEntitylist the t_PartiesEntitylist to set
     */
    public void setT_PartiesEntitylist(List<T_PartiesEntity> t_PartiesEntitylist) {
        this.t_PartiesEntitylist = t_PartiesEntitylist;
    }

    
}
