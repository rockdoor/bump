package maven.springboot.UserToroku.service;

import maven.springboot.Logic.Mailpost;
import maven.springboot.Logic.rakurakuMail;
import maven.springboot.UserToroku.Controller.UserTorokuController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Repository.T_UsersRepository;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Entity.T_PasswordsEntity;
import maven.springboot.Logic.EntryMail;
import maven.springboot.Logic.Random8;
import maven.springboot.Repository.T_PasswordsRepository;
import maven.springboot.Entity.T_Users_MatchingEntity;

import maven.springboot.UserToroku.service.dto.UserTorokuInDto;
import maven.springboot.UserToroku.service.dto.UserTorokuOutDto;
import maven.springboot.Repository.T_Users_MatchingRepository;

@Service  // DIする対象の目印
public class UserTorokuService {

    @Autowired
    private T_UsersRepository t_UsersRepository;

    @Autowired
    private Mailpost mailpost;

    @Autowired
    private EntryMail entryMail;

    @Autowired
    private T_PasswordsRepository t_PasswordsRepository;

    @Autowired
    private Random8 random;

    @Autowired
    private T_Users_MatchingRepository t_Users_MatchingRepository;

    public UserTorokuOutDto entryCfm(UserTorokuInDto ut_indto) {

        UserTorokuOutDto ut_outdto = new UserTorokuOutDto();

        // select文を発行し、要素数を代入
        int count = t_PasswordsRepository.findByEmail(ut_indto.getEmail()).size();
        //user_id格納用変数
        int user_id;

        // int count = 1 ;    
        if (count == 0) {
            //Entityをインスタンス化し、値を入れる
            T_PasswordsEntity t_passwordsentity = new T_PasswordsEntity();
            t_passwordsentity.setEmail(ut_indto.getEmail());

            //URL設定用のurl_idを発行
            int url_id = random.makeramdomforEntry();
            t_passwordsentity.setUrl_id(url_id);

            T_UsersEntity t_usersentity = new T_UsersEntity();
            t_usersentity.setEmail(ut_indto.getEmail());

            // insert文の発行
            t_PasswordsRepository.save(t_passwordsentity);
            t_UsersRepository.save(t_usersentity);

            // もう一度検索し、user_idを取り出して来る
            List<T_UsersEntity> list = t_UsersRepository.findByEmail(ut_indto.getEmail());
            //user_idの取得
            user_id = list.get(0).getUser_id();

            //マッチング基本情報にも登録
            T_Users_MatchingEntity t_Users_MatchingEntity = new T_Users_MatchingEntity();
            t_Users_MatchingEntity.setUser_id(user_id);
            t_Users_MatchingRepository.save(t_Users_MatchingEntity);

            try {
                entryMail.sendMail(ut_indto.getEmail());
            } catch (Exception ex) {
                Logger.getLogger(UserTorokuService.class.getName()).log(Level.SEVERE, null, ex);
            }
            ut_outdto.setEmail(ut_indto.getEmail());
            ut_outdto.setFlag(true);
            ut_outdto.setUser_id(user_id);
        } else {
            ut_outdto.setFlag(false);
        }

        return ut_outdto;

    }

    public UserTorokuOutDto registry(UserTorokuInDto ut_indto) {

        UserTorokuOutDto ut_outdto = new UserTorokuOutDto();

        // select文を発行し、要素数を代入
        List<T_PasswordsEntity> list = t_PasswordsRepository.findByUrl_id(ut_indto.getUrl_id());
        int count = list.size();

        if (count == 1) {
            //Entityをインスタンス化し、値を入れる
            T_PasswordsEntity t_passwordsentity = new T_PasswordsEntity();
            t_passwordsentity.setPassword(ut_indto.getPassword());
            t_passwordsentity.setEmail(list.get(0).getEmail());
            t_passwordsentity.setUrl_id(list.get(0).getUrl_id());

            // insert文の発行
            t_PasswordsRepository.save(t_passwordsentity);

            ut_outdto.setFlag(true);
        } else {
            ut_outdto.setFlag(false);
        }

        return ut_outdto;

    }

    public UserTorokuOutDto getMail(UserTorokuInDto ut_indto) {

        UserTorokuOutDto ut_outdto = new UserTorokuOutDto();

        // select文を発行し、要素数を代入
        List<T_PasswordsEntity> list = t_PasswordsRepository.findByUrl_id(ut_indto.getUrl_id());
        int count = list.size();

        if (count == 1) {
            //Entityをインスタンス化し、値を入れる
            ut_outdto.setEmail(list.get(0).getEmail());
            ut_outdto.setFlag(true);
        } else {
            ut_outdto.setFlag(false);
        }

        return ut_outdto;

    }

    public UserTorokuOutDto login(UserTorokuInDto ut_indto) throws Exception {

        UserTorokuOutDto ut_outdto = new UserTorokuOutDto();

        // 検索結果を取得する
        List<T_PasswordsEntity> account = t_PasswordsRepository.findByEmail(ut_indto.getEmail());

        // select文を発行し、要素数を代入
        int count = account.size();

        //user_id格納用変数
        int user_id;

        // 検索結果があったらtrue
        if (count > 0) {
            String dbpw = account.get(0).getPassword();
            String webpw = ut_indto.getPassword();

            if (dbpw == null ? webpw == null : dbpw.equals(webpw)) {

                List<T_UsersEntity> list = t_UsersRepository.findByEmail(ut_indto.getEmail());

                user_id = list.get(0).getUser_id();

                ut_outdto.setFlag(true);
                ut_outdto.setUser_id(user_id);

                System.out.println("user_id:" + user_id);

            } else {
                ut_outdto.setFlag(false);
            }

        } else {
            ut_outdto.setFlag(false);

        }

        return ut_outdto;

    }

}

//
//    public UserTorokuOutDto entryCfm(UserTorokuInDto ut_indto) {
//
//        UserTorokuOutDto ut_outdto = new UserTorokuOutDto();
//
//        // select文を発行し、要素数を代入
//        int count = t_PasswordsRepository.findByEmail(ut_indto.getEmail()).size();
//        //user_id格納用変数
//        int user_id;
//
//        // int count = 1 ;    
//        if (count == 0) {
//            //Entityをインスタンス化し、値を入れる
//            T_PasswordsEntity t_passwordsentity = new T_PasswordsEntity();
//            t_passwordsentity.setEmail(ut_indto.getEmail());
//            
//            //URL設定用のurl_idを発行
//            int url_id = random.makeramdomforEntry();
//            t_passwordsentity.setUrl_id(url_id);
//            
//            T_UsersEntity t_usersentity = new T_UsersEntity();
//            t_usersentity.setEmail(ut_indto.getEmail());
//
//            // insert文の発行
//            t_PasswordsRepository.save(t_passwordsentity);
//            t_UsersRepository.save(t_usersentity);
//
//            // もう一度検索し、user_idを取り出して来る
//            List<T_UsersEntity> list = t_UsersRepository.findByEmail(ut_indto.getEmail());
//            //user_idの取得
//            user_id = list.get(0).getUser_id();
//            
//            try {
//                entryMail.sendMail(ut_indto.getEmail());
//            } catch (Exception ex) {
//                Logger.getLogger(UserTorokuService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            ut_outdto.setFlag(true);
//            ut_outdto.setUser_id(user_id);
//        } else {
//            ut_outdto.setFlag(false);
//        }
//
//        return ut_outdto;
//
//    }
