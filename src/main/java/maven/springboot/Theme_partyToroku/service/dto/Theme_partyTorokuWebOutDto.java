package maven.springboot.Theme_partyToroku.service.dto;

public class Theme_partyTorokuWebOutDto {

    private boolean The_partyTorokuFlag;
    private String errormessage;
    private int syotaisum;
    private int url_id;
    
    /**
     * @return the The_partyTorokuFlag
     */
    public boolean isThe_partyTorokuFlag() {
        return The_partyTorokuFlag;
    }

    /**
     * @param The_partyTorokuFlag the The_partyTorokuFlag to set
     */
    public void setThe_partyTorokuFlag(boolean The_partyTorokuFlag) {
        this.The_partyTorokuFlag = The_partyTorokuFlag;
    }

    /**
     * @return the errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     * @param errormessage the errormessage to set
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     * @return the syotaisum
     */
    public int getSyotaisum() {
        return syotaisum;
    }

    /**
     * @param syotaisum the syotaisum to set
     */
    public void setSyotaisum(int syotaisum) {
        this.syotaisum = syotaisum;
    }

    /**
     * @return the url_id
     */
    public int getUrl_id() {
        return url_id;
    }

    /**
     * @param url_id the url_id to set
     */
    public void setUrl_id(int url_id) {
        this.url_id = url_id;
    }

    
    
}
