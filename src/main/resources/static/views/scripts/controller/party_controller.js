/* global $routeScope */

angular.module('MyApp')
        .controller('PartyController', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$uibModal',
            function ($scope, $http, $location, $rootScope, $routeParams, $uibModal) {

                $rootScope.url_id = $routeParams.id;
                $scope.header = false;
                $scope.tabflag1 = true;
                $scope.tabflag2 = false;
                $scope.tabflag3 = false;
                $scope.tabflag4 = false;
                $scope.payerflag = false;
                $scope.payerflag2 = true;
                $scope.rakuShowFlag = true;

                //ログイン者の名前を取り出す処理
                $http({
                    method: 'POST',
                    data: {owner: Number($scope.user_id)},
                    url: '/party_ownername'
                }).success(function (data, status, headers, config) {
                    console.log(status);
                    console.dir(data);
                    $scope.username = data.ownername;
                }).error(function (data, status, headers, config) {
                    console.log(status);
                });

                //飲み会予定表示用関数---------------------------------------------------------
                $http({
                    method: 'POST',
                    data: {url_id: Number($rootScope.url_id)},
                    url: '/party_tyo_partykakunin'
                }).success(function (data, status, headers, config) {
                    $scope.results = data;
                    console.log($scope.results);
                    console.log(status);
                    console.dir(data);
                    var l = Object.keys(data.t_Parties_UsersEntitylist).length;
                    var attend;
                    $scope.owner = $scope.results.t_PartiesEntitylist[0].user_id;
                    $rootScope.party_id = $scope.results.t_PartiesEntitylist[0].party_id;
                    //既読つけるよう関数---------------------------------------------------------
                    $http({
                        method: 'POST',
                        data: {user_id: $rootScope.user_id,
                            party_id: $scope.party_id},
                        url: '/kidokukinou'
                    }).success(function (data, status, headers, config) {
                        $scope.results3 = data;
                        console.log($scope.results3);
                        console.log(status);
                        console.dir(data);
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
//                    //飲み会の管理者かをみる
                    if ($scope.user_id === $scope.results.t_PartiesEntitylist[0].user_id) {
                        $scope.payerflag = true;
                        $scope.payerflag2 = false;
                    } else if ($scope.user_id === $scope.results.t_PartiesEntitylist[0].payer_id) {
                        $scope.payerflag = true;
                        $scope.payerflag2 = false;
                    } else {
                        $scope.payerflag = false;
                        $scope.payerflag2 = true;
                    }

                    //出欠ステータスをみて、○×を調整
                    var syukketuhantei;
                    for (k = 0; k < l; k++) {
                        if ($scope.results.t_Parties_UsersEntitylist[k].user_id === $rootScope.user_id) {
                            syukketuhantei = $scope.results.t_Parties_UsersEntitylist[k].attend_cd;
                        }
                    }
                    if (syukketuhantei === 1) {
                        var ele = document.getElementById('maru');
                        ele.removeAttribute("class");
                        var ele = document.getElementById('batsu');
                        ele.setAttribute('class', 'selectedstatus');
                    } else if (syukketuhantei === 2) {
                        var ele = document.getElementById('batsu');
                        ele.removeAttribute("class");
                        var ele = document.getElementById('maru');
                        ele.setAttribute('class', 'selectedstatus');
                    }

                    //飲み会区分に対応して、ヘッダー部の画像を変更
                    if ($scope.results.t_PartiesEntitylist[0].party_cd === 1) {
                        $rootScope.nomikai = "tyoinomi";
                    } else if ($scope.results.t_PartiesEntitylist[0].party_cd === 2) {
                        $rootScope.nomikai = "temanomi";
                    } else if ($scope.results.t_PartiesEntitylist[0].party_cd === 3) {
                        $rootScope.nomikai = "randomnomi";
                    } else {
                        $rootScope.nomikai = "nomi";
                    }

//                    //出席　未回答　欠席区別
//                    for (k = 0; k < l; k++) {
//                        if ($scope.results.t_Parties_UsersEntitylist[k].attend_cd === 0) {
//                            $scope.results.t_partiesEntity_list[k].party_cd = true;
//                        } else if($scope.results.t_Parties_UsersEntitylist[k].attend_cd === 2) {
//                            $scope.results.t_partiesEntity_list[k].party_cd = true;
//                        }else
//                             $scope.results.t_partiesEntity_list[k].party_cd = false;
//                    }


                    // 回答区分（0,1,2）を（未回答、欠席、出席）に変更
                    for (k = 0; k < l; k++) {
                        if ($scope.results.t_Parties_UsersEntitylist[k].attend_cd === 0) {
                            attend = "未回答";
                        } else if ($scope.results.t_Parties_UsersEntitylist[k].attend_cd === 1) {
                            attend = "欠席";
                        } else {
                            attend = "出席";
                        }
                        $scope.results.t_Parties_UsersEntitylist[k].party_id = attend;
                    }

                    //主催者の名前を取り出す処理
                    $http({
                        method: 'POST',
                        data: {owner: Number($scope.owner)},
                        url: '/party_ownername'
                    }).success(function (data, status, headers, config) {
                        console.log(status);
                        console.dir(data);
                        $scope.ownername = data.ownername;
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });

                    $scope.kaisaibi;
                    //年月日を表示できるように直す。
                    date = $scope.results.t_PartiesEntitylist[0].date;
                    date = date + "";
                    date = date.slice(0, 4) + '年' + date.slice(4, 6) + "月" + date.slice(6, date.length) + "日";
                    $scope.kaisaibi = date;
                    time = $scope.results.t_PartiesEntitylist[0].time;
                    time = time + "";
                    time = time.slice(0, 2) + '時' + time.slice(2, time.length) + '分';
                    $scope.kaisaijikan = time;



                }).error(function (data, status, headers, config) {
                    console.log(status);
                });
                //タブの切替---------------------------------------------------------
                $scope.dotab1show = function () {

                    $scope.tabflag1 = true;
                    $scope.tabflag2 = false;
                    $scope.tabflag3 = false;
                    $scope.tabflag4 = false;
                    var ele = document.getElementById('tab2');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab3');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab4');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab1');
                    ele.setAttribute('class', 'selected');
                };
                //タブの切替
                $scope.dotab2show = function () {

                    $scope.tabflag1 = false;
                    $scope.tabflag2 = true;
                    $scope.tabflag3 = false;
                    $scope.tabflag4 = false;
                    var ele = document.getElementById('tab1');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab3');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab4');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab2');
                    ele.setAttribute('class', 'selected');
                };
                //タブの切替
                $scope.dotab3show = function () {

                    $scope.tabflag1 = false;
                    $scope.tabflag2 = false;
                    $scope.tabflag3 = true;
                    $scope.tabflag4 = false;
                    var ele = document.getElementById('tab2');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab1');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab4');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab3');
                    ele.setAttribute('class', 'selected');
                };
                //タブの切替
                $scope.dotab4show = function () {

                    $scope.tabflag1 = false;
                    $scope.tabflag2 = false;
                    $scope.tabflag3 = false;
                    $scope.tabflag4 = true;
                    var ele = document.getElementById('tab2');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab3');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab1');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('tab4');
                    ele.setAttribute('class', 'selected');
                };
                //精算額の通知用関数---------------------------------------------------------
                $scope.doTsuti = function () {
                    $http({
                        method: 'POST',
                        data: {party_id: Number($scope.party_id)},
                        url: '/Tsuti'
                    }).success(function (data, status, headers, config) {
                        console.log(status);
                        alert('メールを送信しました!');
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                };
                /*精算額更新用関数*/
                $scope.doSeisanUpdate = function (idx) {
                    $http({
                        method: 'POST',
                        data: {user_id: $scope.results.t_Parties_UsersEntitylist[idx].user_id, payment: $scope.results.t_Parties_UsersEntitylist[idx].payment, party_id: $scope.party_id},
                        url: '/seisanupdate'
                    }).success(function (data, status, headers, config) {
                        $scope.Results = data;
                        $scope.i = $scope.i + 1;
                        console.log(status);
                        console.log(data);
                        alert('更新完了！');
                    }).error(function (data, status, headers, config) {
                        console.log(status);
                    });
                    $scope.updateflag = false;
                };
                //出欠回答に用いる処理
                //出席と回答
                $scope.dosyusseki = function () {
                    //出欠更新区分
                    $rootScope.marubatsu = 2;
                    $rootScope.modalInstance = $uibModal.open({
                        templateUrl: '/views/Syukkestupop.html',
                        controller: 'SyukkestupopController',
                        backdrop: true,
                        scope: $scope
                    });
                    $rootScope.modalInstance.result.then(
                            function (result) {
                                if ($rootScope.marubatsuflag === true) {
                                    window.alert("出欠区分を更新しました！！");
                                    var ele = document.getElementById('batsu');
                                    ele.removeAttribute("class");
                                    var ele = document.getElementById('maru');
                                    ele.setAttribute('class', 'selectedstatus');
                                } else {

                                }
                            },
                            function (result) {
                            });
                };
                //欠席と回答
                $scope.dokesseki = function () {
                    //出欠更新区分
                    $rootScope.marubatsu = 1;
                    $rootScope.modalInstance = $uibModal.open({
                        templateUrl: '/views/Syukkestupop.html',
                        controller: 'SyukkestupopController',
                        backdrop: true,
                        scope: $scope
                    });
                    $rootScope.modalInstance.result.then(
                            function (result) {
                                if ($rootScope.marubatsuflag === true) {
                                    window.alert("出欠区分を更新しました！！");
                                    var ele = document.getElementById('maru');
                                    ele.removeAttribute("class");
                                    var ele = document.getElementById('batsu');
                                    ele.setAttribute('class', 'selectedstatus');
                                } else {

                                }
                            },
                            function (result) {
                            });
                };
                $scope.showSansyo = function () {
                    //出欠更新区分
                    $scope.rakuShowFlag = true;
                    var ele = document.getElementById('rakuSeisan');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('rakuSansyo');
                    ele.setAttribute('class', 'selectedstatus');
                };
                //欠席と回答
                $scope.showSeisan = function () {
                    //出欠更新区分
                    $scope.rakuShowFlag = false;
                    var ele = document.getElementById('rakuSansyo');
                    ele.removeAttribute("class");
                    var ele = document.getElementById('rakuSeisan');
                    ele.setAttribute('class', 'selectedstatus');
                };
                
                
            }]);



