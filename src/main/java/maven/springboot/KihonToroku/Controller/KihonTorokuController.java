package maven.springboot.KihonToroku.Controller;

import maven.springboot.KihonToroku.service.KihonTorokuLogic;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.KihonToroku.service.dto.KihonTorokuInDto;
import maven.springboot.KihonToroku.service.dto.KihonTorokuOutDto;
import maven.springboot.KihonToroku.service.dto.KihonTorokuWebDto;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class KihonTorokuController {

//初期登録画面とMapping（コントローラー　→　JSP）
//        @RequestMapping("/kihon")
//        public String shokiTorokuIn(){
//                return "Kihon";
//        }
//サービスロジック層へ接続
    @Autowired
    private KihonTorokuLogic kihonTorokuLogic;

    //初期登録画面とMapping
    @RequestMapping(value = "/kihontoroku", method = RequestMethod.POST)
    @ResponseBody//JSON形式
    public KihonTorokuOutDto requestNickname(@RequestBody KihonTorokuWebDto ktwd) {
        //jsonで受け取った値をKihonTorokuWebDtoに渡して、以下で各値に戻す。
        String reqname = ktwd.getName();
        int reqdepertment_id = ktwd.getDepartment_id();
        int reqjoinedyear = ktwd.getJoinedyear();
        int requser_id = ktwd.getUser_id();
        int reqbirth = ktwd.getBirth();
        ///InDTOにニックネーム、部署、入社年を入れる処理
        KihonTorokuInDto kihonTorokuInDto = new KihonTorokuInDto();
        kihonTorokuInDto.setName(reqname);
        kihonTorokuInDto.setDepartment_id(reqdepertment_id);
        kihonTorokuInDto.setJoinedyear(reqjoinedyear);
        kihonTorokuInDto.setUser_id(requser_id);
        kihonTorokuInDto.setBirth(reqbirth);

        //OutDtoに、true/falseとmail_no、を入れる処理
        KihonTorokuOutDto kihonTorokuOutDto = kihonTorokuLogic.kihonTorokuHantei(kihonTorokuInDto);
        kihonTorokuOutDto.setUser_id(requser_id);

        //確認
        System.out.println("***********************************");
        System.out.println("flag=" + kihonTorokuOutDto.getFlag());
        System.out.println("mail_no=" + kihonTorokuOutDto.getUser_id());
        System.out.println("***********************************");

        //serviceOutDtoのgetFlag()によって、true/false、を返す
        return kihonTorokuOutDto;

    }

}
