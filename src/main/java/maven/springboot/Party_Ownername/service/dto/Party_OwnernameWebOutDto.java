package maven.springboot.Party_Ownername.service.dto;

public class Party_OwnernameWebOutDto {

    private String ownername;

    /**
     * @return the ownername
     */
    public String getOwnername() {
        return ownername;
    }

    /**
     * @param ownername the ownername to set
     */
    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

}
