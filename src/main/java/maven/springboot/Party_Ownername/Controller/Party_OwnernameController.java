package maven.springboot.Party_Ownername.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Party_Ownername.service.Party_OwnernameService;
import maven.springboot.Party_Ownername.service.dto.Party_OwnernameInDto;
import maven.springboot.Party_Ownername.service.dto.Party_OwnernameWebInDto;
import maven.springboot.Party_Ownername.service.dto.Party_OwnernameWebOutDto;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class Party_OwnernameController {

    @Autowired   // DIコンテナからインスタンスを取得する
    private Party_OwnernameService party_OwnernameService;

    @RequestMapping(value = "/party_ownername", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Party_OwnernameWebOutDto requestOwnername(@RequestBody Party_OwnernameWebInDto party_ownernamewebindto) {
        int owner = party_ownernamewebindto.getOwner();
        
        System.out.println(party_ownernamewebindto.getOwner());

        //InDTOにReqparamに入れる処理
        Party_OwnernameInDto party_Ownernameindto = new Party_OwnernameInDto();
        party_Ownernameindto.setOwner(owner);

        Party_OwnernameWebOutDto party_OwnernameWebOutDto = party_OwnernameService.selectowner(party_Ownernameindto);

        return party_OwnernameWebOutDto;
    }
}
