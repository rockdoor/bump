package maven.springboot.RakurakuSansyo.Service.dto;

import java.util.List;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Entity.T_PartiesEntity;

public class RakurakuSansyoWebDto {


    private List<T_Parties_UsersEntity> t_Parties_Users_List;
    private List<T_UsersEntity> t_Users_List;
    private List<T_PartiesEntity> t_Parties_List;

    /**
     * @return the t__Parties_Users_List
     */
    public List<T_Parties_UsersEntity> getT_Parties_Users_List() {
        return t_Parties_Users_List;
    }

    public void setT_Parties_Users_List(List<T_Parties_UsersEntity> t__Parties_Users_List) {
        this.t_Parties_Users_List = t__Parties_Users_List;
    }

    /**
     * @return the t_Users_List
     */
    public List<T_UsersEntity> getT_Users_List() {
        return t_Users_List;
    }

    /**
     * @param t_Users_List the t_Users_List to set
     */
    public void setT_Users_List(List<T_UsersEntity> t_Users_List) {
        this.t_Users_List = t_Users_List;
    }

    /**
     * @return the t_Parties_List
     */
    public List<T_PartiesEntity> getT_Parties_List() {
        return t_Parties_List;
    }

    /**
     * @param t_Parties_List the t_Parties_List to set
     */
    public void setT_Parties_List(List<T_PartiesEntity> t_Parties_List) {
        this.t_Parties_List = t_Parties_List;
    }

  

}
