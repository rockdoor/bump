package maven.springboot.Kidokukinou.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Repository.T_Parties_UsersRepository;

import java.util.*;
import maven.springboot.Kidokukinou.service.dto.KidokukinouInDto;
import maven.springboot.Kidokukinou.service.dto.KidokukinouOutDto;

@Service  // DIする対象の目印
public class KidokukinouService {

    //DI層へ接続
    @Autowired
    private T_Parties_UsersRepository t_Parties_Usersrepository;

    public KidokukinouOutDto KidokuToroku(KidokukinouInDto kidokukinouInDto){
        KidokukinouOutDto kidokukinouOutDto = new KidokukinouOutDto();
                
        //飲み会IDとUser_idを元にいったんその行を持ってくる
        List<T_Parties_UsersEntity> list = t_Parties_Usersrepository.findByUser_idPartyid(kidokukinouInDto.getUser_id(), kidokukinouInDto.getParty_id());
        int count = list.size();
        
        if(count == 0){
            System.out.println("対象の飲み会IDとユーザIDにひもづく飲み会データはありません");
            kidokukinouOutDto.setFlag(false);
        }
        else{
            T_Parties_UsersEntity t_Parties_UsersEntity = list.get(0);
            
            //showedflagにtrueをセット
            t_Parties_UsersEntity.setShowed_flag(true);
            t_Parties_Usersrepository.save(t_Parties_UsersEntity);

            //返り値にtrueをセット
            kidokukinouOutDto.setFlag(true);
        }

        return kidokukinouOutDto;
    }
}
