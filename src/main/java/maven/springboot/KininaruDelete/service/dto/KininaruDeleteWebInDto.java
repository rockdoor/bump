package maven.springboot.KininaruDelete.service.dto;

import java.util.HashMap;

public class KininaruDeleteWebInDto {
    
    private int user_id;
    private HashMap<String, String> delete_map;
    
    //ユーザーID
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
    //artfact_id
    /**
     * @return the delete_map
     */
    public HashMap<String, String> getDelete_map() {
        return delete_map;
    }

    /**
     * @param delete_map the delete_map to set
     */
    public void setDelete_map(HashMap<String, String> delete_map) {
        this.delete_map = delete_map;
    }


}
