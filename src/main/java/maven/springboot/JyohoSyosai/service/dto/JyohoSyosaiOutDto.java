package maven.springboot.JyohoSyosai.service.dto;

import maven.springboot.UserToroku.service.dto.*;


public class JyohoSyosaiOutDto {
        private String email;
        private String name;
        private int department_id;
        private String department_name;
        private int joinedyear;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the department_id
     */
    public int getDepartment_id() {
        return department_id;
    }

    /**
     * @param department_id the department_id to set
     */
    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    /**
     * @return the department_name
     */
    public String getDepartment_name() {
        return department_name;
    }

    /**
     * @param department_name the department_name to set
     */
    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    /**
     * @return the joinedyear
     */
    public int getJoinedyear() {
        return joinedyear;
    }

    /**
     * @param joinedyear the joinedyear to set
     */
    public void setJoinedyear(int joinedyear) {
        this.joinedyear = joinedyear;
    }

    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
        
}
