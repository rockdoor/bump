package maven.springboot.Theme_partyToroku.service.dto;

public class Theme_partyTorokuInDto {

    private String party_name; //パーティ名
    private int user_id; //オーナの個人ID
    private int date; //開催日
    private int time; //開催時間

    private int artifact_id; //テーマID
    private int theme_area_id; //開催エリアID
    private int theme_genre_id; //開催店ジャンルID
    private int holding_max_person; //開催上限人数ID
    private int recruit_max_person; //開催下限人数ID
    private int deaddate; //募集締め切り
    private int deadtime; //募集締め切り

    /**
     * @return the party_name
     */
    public String getParty_name() {
        return party_name;
    }

    /**
     * @param party_name the party_name to set
     */
    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(int date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public int getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the artifact_id
     */
    public int getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(int artifact_id) {
        this.artifact_id = artifact_id;
    }

    /**
     * @return the theme_area_id
     */
    public int getTheme_area_id() {
        return theme_area_id;
    }

    /**
     * @param theme_area_id the theme_area_id to set
     */
    public void setTheme_area_id(int theme_area_id) {
        this.theme_area_id = theme_area_id;
    }

    /**
     * @return the theme_genre_id
     */
    public int getTheme_genre_id() {
        return theme_genre_id;
    }

    /**
     * @param theme_genre_id the theme_genre_id to set
     */
    public void setTheme_genre_id(int theme_genre_id) {
        this.theme_genre_id = theme_genre_id;
    }

    /**
     * @return the holding_max_person
     */
    public int getHolding_max_person() {
        return holding_max_person;
    }

    /**
     * @param holding_max_person the holding_max_person to set
     */
    public void setHolding_max_person(int holding_max_person) {
        this.holding_max_person = holding_max_person;
    }

    /**
     * @return the recruit_max_person
     */
    public int getRecruit_max_person() {
        return recruit_max_person;
    }

    /**
     * @param recruit_max_person the recruit_max_person to set
     */
    public void setRecruit_max_person(int recruit_max_person) {
        this.recruit_max_person = recruit_max_person;
    }

    /**
     * @return the deaddate
     */
    public int getDeaddate() {
        return deaddate;
    }

    /**
     * @param deaddate the deaddate to set
     */
    public void setDeaddate(int deaddate) {
        this.deaddate = deaddate;
    }

    /**
     * @return the deadtime
     */
    public int getDeadtime() {
        return deadtime;
    }

    /**
     * @param deadtime the deadtime to set
     */
    public void setDeadtime(int deadtime) {
        this.deadtime = deadtime;
    }


}
