package maven.springboot.KininaruTrend.service.dto;

import java.util.List;
import maven.springboot.Entity.T_TrendsEntity;

public class KininaruTrendOutDto {

    private List<Object> t_Trendslist;

    /**
     * @return the t_Trendslist
     */
    public List<Object> getT_Trendslist() {
        return t_Trendslist;
    }

    /**
     * @param t_Trendslist the t_Trendslist to set
     */
    public void setT_Trendslist(List<Object> t_Trendslist) {
        this.t_Trendslist = t_Trendslist;
    }

    
    
    
}
