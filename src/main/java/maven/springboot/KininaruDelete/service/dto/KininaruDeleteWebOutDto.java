package maven.springboot.KininaruDelete.service.dto;

//import java.util.List;


public class KininaruDeleteWebOutDto {

    private int dflag; 

    /**
     * @return the dflag
     */
    public int getDflag() {
        return dflag;
    }

    /**
     * @param dflag the dflag to set
     */
    public void setDflag(int dflag) {
        this.dflag = dflag;
    }
    
}
