package maven.springboot.Kidokukinou.Controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.RequestBody;

import maven.springboot.Kidokukinou.service.KidokukinouService;
import maven.springboot.Kidokukinou.service.dto.KidokukinouInDto;
import maven.springboot.Kidokukinou.service.dto.KidokukinouOutDto;
import maven.springboot.Kidokukinou.service.dto.KidokukinouWebDto;

@Controller
public class KidokukinouController {

    //サービスロジック層へ接続
    @Autowired   // DIコンテナからインスタンスを取得する
    private KidokukinouService kidokukinouservice;

    @RequestMapping(value = "/kidokukinou", method = RequestMethod.POST)
    @ResponseBody
    public KidokukinouOutDto Requestparty(@RequestBody KidokukinouWebDto kidokukinouWebDto){

        ///InDTOにパーティー区分、パーティーの名前、開催日、開催時間を入れる処理
        KidokukinouInDto kidokukinouInDto = new KidokukinouInDto();
        kidokukinouInDto.setParty_id(kidokukinouWebDto.getParty_id());
        kidokukinouInDto.setUser_id(kidokukinouWebDto.getUser_id());

        KidokukinouOutDto kidokukinouOutDto = new KidokukinouOutDto();
        
        //既読に更新する処理をサービスクラスで行う
        kidokukinouOutDto = kidokukinouservice.KidokuToroku(kidokukinouInDto);

        return kidokukinouOutDto;
    }
}
