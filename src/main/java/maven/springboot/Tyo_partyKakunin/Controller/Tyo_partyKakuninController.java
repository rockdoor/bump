package maven.springboot.Tyo_partyKakunin.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Tyo_partyKakunin.service.Tyo_partyKakuninService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninInDto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninOutDto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninWebInDto;
import maven.springboot.Tyo_partyKakunin.service.dto.Tyo_partyKakuninWebOutDto;

import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class Tyo_partyKakuninController {
    
    /*★★★★mail_no(個人識別番号)に応じた各情報のJSONを返す。*/
    @Autowired   // DIコンテナからインスタンスを取得する
    private Tyo_partyKakuninService tyo_partykakuninservice;

    @RequestMapping(value = "/tyo_partykakunin", method = RequestMethod.POST)
    @ResponseBody //戻り値をレスポンス(JSON)として返す。
    public Tyo_partyKakuninWebOutDto requestTyoPartyKakunin(@RequestBody Tyo_partyKakuninWebInDto tkwd) {
        int requser_id = tkwd.getUser_id();
        //Tyo_partyKakuninInDTO.classにReqparamに入れる処理
        Tyo_partyKakuninInDto tyo_partykakuninindto = new Tyo_partyKakuninInDto();
        tyo_partykakuninindto.setUser_id(requser_id);

        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
        Tyo_partyKakuninWebOutDto tkwod = tyo_partykakuninservice.tyoPartyHyouji(tyo_partykakuninindto);

        try {
            //確認
            System.out.println(new ObjectMapper().writeValueAsString(tkwod));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Tyo_partyKakuninController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tkwod;
    }
}

//    }
//    @RequestMapping(value = "/tyo_membaerkakunin", method = RequestMethod.POST)
//    @ResponseBody //戻り値をレスポンス(JSON)として返す。
//    public Tyo_partyKakuninOutDto requestTyoMember(@RequestBody Tyo_partyMemberWebInDto tmwd) {
//        int reqparty_id = tmwd.getParty_id();
//        //Tyo_partyKakuninInDTO.classにReqparamに入れる処理
//        Tyo_partyMemberInDto tyo_partymemberindto = new Tyo_partyMemberInDto();
//        tyo_partymemberindto.setParty_id(reqparty_id);
//
//        //Tyo_partyKakuninServiceにDtoを渡し、結果をもらう;
//        Tyo_partyMemberOutDto tmod = tyo_partykakuninservice.tyoMemberHyouji(tyo_partymemberindto);
//
//        try {
//            //確認
//            System.out.println(new ObjectMapper().writeValueAsString(tkod));
//        } catch (JsonProcessingException ex) {
//            Logger.getLogger(Tyo_partyKakuninController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return tkod;
//}
