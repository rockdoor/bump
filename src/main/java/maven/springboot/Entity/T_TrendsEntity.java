package maven.springboot.Entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "t_trends")
public class T_TrendsEntity {

        @Id
        @GeneratedValue
        private Integer trend_id;

        @Column(name = "artifact_id")
        private int artifact_id;

        @Column(name = "like_name")
        private String like_name;

        @Column(name = "like_date")
        private Date like_date;

    /**
     * @return the trend_id
     */
    public Integer getTrend_id() {
        return trend_id;
    }

    /**
     * @param trend_id the trend_id to set
     */
    public void setTrend_id(Integer trend_id) {
        this.trend_id = trend_id;
    }

    /**
     * @return the artifact_id
     */
    public int getArtifact_id() {
        return artifact_id;
    }

    /**
     * @param artifact_id the artifact_id to set
     */
    public void setArtifact_id(int artifact_id) {
        this.artifact_id = artifact_id;
    }

    /**
     * @return the like_name
     */
    public String getLike_name() {
        return like_name;
    }

    /**
     * @param like_name the like_name to set
     */
    public void setLike_name(String like_name) {
        this.like_name = like_name;
    }

    /**
     * @return the like_date
     */
    public Date getLike_date() {
        return like_date;
    }

    /**
     * @param like_date the like_date to set
     */
    public void setLike_date(Date like_date) {
        this.like_date = like_date;
    }

        
}