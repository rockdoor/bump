/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maven.springboot.Tyo_partyKaitou.service.dto;

import maven.springboot.Entity.T_Parties_UsersEntity;
import java.util.*;

/**
 *
 * @author t4-yoshioka
 */
public class Tyo_partyKaitouOutDto {
//    private String party_name;
//    private int attend_cd;
//   
//    /**
//     * @return the party_name
//     */
//    public String getParty_name() {
//        return party_name;
//    }
//
//    /**
//     * @param party_name the party_name to set
//     */
//    public void setParty_name(String party_name) {
//        this.party_name = party_name;
//    }
//
//    /**
//     * @return the attend_cd
//     */
//    public int getAttend_cd() {
//        return attend_cd;
//    }
//
//    /**
//     * @param attend_cd the attend_cd to set
//     */
//    public void setAttend_cd(int attend_cd) {
//        this.attend_cd = attend_cd;
//    }
//    
    
    //LIST型変数を追加します
    private List<T_Parties_UsersEntity> list;

    /**
     * @return the list
     */
    public List<T_Parties_UsersEntity> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<T_Parties_UsersEntity> list) {
        this.list = list;
    }
    
    
    
    
}
