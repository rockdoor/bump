package maven.springboot.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import maven.springboot.Entity.T_PartiesEntity;
import java.util.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository  // DIする対象の目印
public interface T_PartiesRepository extends JpaRepository<T_PartiesEntity, Integer> {

    @Query(value = "select t from T_PartiesEntity t where t.user_id = :user_id")
    public List<T_PartiesEntity> findByUser_id(@Param("user_id") int user_id);

    @Query(value = "select t from T_PartiesEntity t where t.party_id = :party_id")
    public List<T_PartiesEntity> findByParty_id(@Param("party_id") int party_id);

    @Query(value = "select t from T_PartiesEntity t where t.user_id = :user_id AND t.time = :time  AND t.date = :date ")
    public List<T_PartiesEntity> findByAddress(@Param("user_id") int user_id,
            @Param("time") int time,
            @Param("date") int date);

    @Query(value = "select t from T_PartiesEntity t where t.user_id = :user_id AND t.party_cd = 1")
    public List<T_PartiesEntity> findByUserid_Partycd(@Param("user_id") int user_id);

    @Query(value = "select t from T_PartiesEntity t where t.url_id = :url_id")
    public List<T_PartiesEntity> findByUrl_id(@Param("url_id") int url_id);

    @Query(value = "select t from T_PartiesEntity t where t.party_id = :party_id and t.date >= :date order by t.date, t.time")
    public List<T_PartiesEntity> findByParty_idDate(@Param("party_id") int party_id,
            @Param("date") int date);

}
