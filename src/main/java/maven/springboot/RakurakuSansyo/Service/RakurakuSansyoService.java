package maven.springboot.RakurakuSansyo.Service;

//import maven.springboot.RakurakuSansyo.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import maven.springboot.Entity.T_Parties_UsersEntity;
import maven.springboot.Entity.T_UsersEntity;
import maven.springboot.Entity.T_PartiesEntity;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import maven.springboot.Logic.Mailpost;
import maven.springboot.Logic.rakurakuMail;

import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoInDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoOutDto;
import maven.springboot.RakurakuSansyo.Service.dto.RakurakuSansyoWebDto;

import maven.springboot.Repository.T_Parties_UsersRepository;
import maven.springboot.Repository.T_UsersRepository;
import maven.springboot.Repository.T_PartiesRepository;

@Service  // DIする対象の目印
public class RakurakuSansyoService {

    @Autowired
    private T_Parties_UsersRepository t_Parties_UsersRepository;
    @Autowired
    private T_UsersRepository t_UsersRepository;
    @Autowired
    private Mailpost mailpost;
    @Autowired
    private rakurakuMail rakurakumail;
    @Autowired
    private T_PartiesRepository t_PartiesRepository;

    public RakurakuSansyoWebDto Party_List(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したuser_idが登録されてるpartyの情報list
        int count = t_Parties_UsersRepository.findByUser_id(rakurakusansyoindto.getUser_id()).size();
        List<T_Parties_UsersEntity> t_parties_users_List = t_Parties_UsersRepository.findByUser_id(rakurakusansyoindto.getUser_id());
        if (count == 0) {
            System.out.println("☆☆☆☆☆☆☆☆☆☆☆☆Listがnullです☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
        } else {
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
            System.out.println("☆☆☆☆☆☆☆☆☆");
        }
        return rakurakusansyowebdto;
    }

    public RakurakuSansyoWebDto seisanHyoji(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したparty_idが登録されてるuser_idの持ち主の情報list
        int count = t_Parties_UsersRepository.findByParty_id(rakurakusansyoindto.getParty_id()).size();
        List<T_Parties_UsersEntity> t_parties_users_List = t_Parties_UsersRepository.findByParty_id(rakurakusansyoindto.getParty_id());
        List<T_PartiesEntity> t_parties_List = t_PartiesRepository.findByParty_id(rakurakusansyoindto.getParty_id());
        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
        } else {
            List<T_UsersEntity> t_users_List = new ArrayList();
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
                t_users_List.addAll(t_UsersRepository.findByUser_id(t_Parties_UsersEntity.getUser_id()));
            }
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
            rakurakusansyowebdto.setT_Users_List(t_users_List);
            rakurakusansyowebdto.setT_Parties_List(t_parties_List);
            System.out.println("★★★★★★★★★");
        }
        return rakurakusansyowebdto;
    }

    public RakurakuSansyoWebDto mailTsuti(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したparty_idが登録されてるuser_idの持ち主の情報list
        List<T_Parties_UsersEntity> t_parties_users_List = t_Parties_UsersRepository.findByParty_id(rakurakusansyoindto.getParty_id());

        int count = t_parties_users_List.size();

        List<T_PartiesEntity> t_parties_List = t_PartiesRepository.findByParty_id(rakurakusansyoindto.getParty_id());
        if (count == 0) {
            System.out.println("★★★★★★★★★★★★★★★★★Listがnullです★★★★★★★★★★★★★★★★★");
        } else {
            List<T_UsersEntity> t_users_List = new ArrayList();
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
                t_users_List.addAll(t_UsersRepository.findByUser_id(t_Parties_UsersEntity.getUser_id()));
            }
            try {
                rakurakumail.Mailsend(rakurakusansyoindto.getParty_id());
            } catch (Exception ex) {
                Logger.getLogger(RakurakuSansyoService.class.getName()).log(Level.SEVERE, null, ex);
            }
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
            rakurakusansyowebdto.setT_Users_List(t_users_List);
            rakurakusansyowebdto.setT_Parties_List(t_parties_List);
        }
        return rakurakusansyowebdto;
    }

    public RakurakuSansyoWebDto seisanUpdate(RakurakuSansyoInDto rakurakusansyoindto) {
        RakurakuSansyoWebDto rakurakusansyowebdto = new RakurakuSansyoWebDto();
        // select文を発行し、要素数を代入 指定したparty_idが登録されてるuser_idの持ち主の情報list
        int count = t_Parties_UsersRepository.findByUser_id(rakurakusansyoindto.getUser_id()).size();
        List<T_Parties_UsersEntity> t_parties_users_List = t_Parties_UsersRepository.findByParty_id(rakurakusansyoindto.getParty_id());
        if (count == 0) {
            System.out.println("************************Listがnullです*****************************");
        } else {
            for (T_Parties_UsersEntity t_Parties_UsersEntity : t_parties_users_List) {
               t_Parties_UsersEntity.setPayment(rakurakusansyoindto.getPayment());
               t_Parties_UsersEntity.setUser_id(rakurakusansyoindto.getUser_id());
               t_Parties_UsersEntity.setParty_id(rakurakusansyoindto.getParty_id());
                //Update文の発行
                this.t_Parties_UsersRepository.save(t_Parties_UsersEntity);
            }
            rakurakusansyowebdto.setT_Parties_Users_List(t_parties_users_List);
        }
        return rakurakusansyowebdto;
    }
}
