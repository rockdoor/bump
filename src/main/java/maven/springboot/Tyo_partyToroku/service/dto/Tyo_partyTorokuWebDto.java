package maven.springboot.Tyo_partyToroku.service.dto;

public class Tyo_partyTorokuWebDto {

    //パーティー区分、パーティーの名前、個人id、開催日、開催時間、締め切り時間
    private int party_cd;
    private String party_name;
    private int user_id;
    private int date;
    private int starttime;
    private int deadline;

    //setter
    public void setParty_cd(int party_cd) {
        this.party_cd = party_cd;
    }

    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setStarttime(int starttime) {
        this.starttime = starttime;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

    //getter
    public Integer getParty_cd() {
        return this.party_cd;
    }

    public String getParty_name() {
        return this.party_name;
    }

    public Integer getUser_id() {
        return this.user_id;
    }

    public Integer getDate() {
        return this.date;
    }

    public Integer getStarttime() {
        return this.starttime;
    }

    public int getDeadline() {
        return deadline;
    }

}
